// ==UserScript==
// @name			cmMBLRCC
// @version			3.3.7
// @namespace		http://www.croquemonster.com
// @description		Aide de jeu pour la MBL (stats, surveillance et refresh)
// @include			/http://www\.croquemonster\.com/mbl/[0-9]+/
// @copyright		Pascal Vander-Swalmen aka squalbee from famous RCC! :)
// @grant			GM_getValue
// @grant			GM_setValue
// @grant			GM_xmlhttpRequest
// @license			LGPL http://www.gnu.org/licenses/lgpl.html
// ==/UserScript==

// TODO BUGS
// Problème d'horaire si le script est lancé avant 10h15.
// urgent : surveillance stoppée après un KO qui était surveillé en connexion. À la mise à jour à la main, ça s'est déverouillé.
// + voir si les timers sont OK pour la surveillance

// TODO EVOS
// 0 - Remettre un suivi qui fonctionne (sans charger le fichier du match)
// 1 - Ajouter une demande de sonnerie à 1 minute avant l'ajout de PA
// 2 - Fusionner avec le magnéto

// récupération de l'heure locale
var local_time = new Date();

// récupération de la zone d'affichage
function get_print_area() {
	return document.getElementById('content');
}
// récupération du numéro de match
function get_match_ID() {
	return parseInt(location.href.substr(33), 10);
}
// test des erreurs
function check_errors() {
	// si la zone d'affichage n'existe pas, alors on stoppe
	if (!print_area) return true;

	// si on n'est pas sur une page de match, alors on stoppe
	if (!(match_ID > 0)) return true;
}

var print_area = get_print_area();
var match_ID = get_match_ID();

// constantes
var SOUND_LOCATION = 'http://squal.conrad.free.fr/RCC_the_winners/sons/';
var NO = -1;
var BLUE = 0;
var RED = 1;
var MATCH = 2;
var HISTO = 3;
var CREW = 0;
var KO = 1;
var PLAYER = 2;
var OPPONENT = 3;
var FRIEND = 4;
var TEST = 10;
var SPY = 5;
var FULLPA = 24;
var INITPA = 10;
var ATTAQUE_PERCENT = 0.75;
var DEFENSE_PERCENT = 0.55;
var PG_XB = 0;
var PG_XE = 34;
var PG_YB = 0;
var PG_YE = 16;
var BLUE_SIDE = PG_XB;
var RED_SIDE = PG_XE;
var FREQUENCE_SPY = 10000;
var ONLINE = '<img src="/gfx/icons/online.gif">';
var NOTIF_OPPONENT = '/gfx/icons/sadism.gif';
var NOTIF_FRIEND = '/gfx/icons/power.gif';
var NOTIF_PASS = '/gfx/icons/greediness.gif';
var NOTIF_FIGHT = '/gfx/icons/fight.gif';
var NOTIF_PA = '/gfx/icons/reputation.gif';
var NOTIF_PA_SOON = '/gfx/forum/time.gif';
var NOTIF_CONNEXION = '/gfx/icons/online.gif';
var NOTIF_DECONNEXION = '/gfx/icons/offline.gif';
var NOTIF_INFO = '/gfx/forum/icon_exclaim.gif';
var INTERVAL = 1;
var TIMEOUT = 2;
var INVINCIBLE = 200;
var SIDE = new Array();
SIDE[BLUE] = 1;
SIDE[RED] = -1;
var TIP_TITLE = 'Gloires et hontes de ';
var PV_KILL_INIT_MIN = 1000;
var PV_KILL_INIT_MAX = 0;

// variables globales
var my_ID = 0;
var my_team = NO;
var syndicate = new Array();
var old_player_size = new Array();
old_player_size[BLUE] = 0;
old_player_size[RED] = 0;
var old_KO_size = new Array();
old_KO_size[BLUE] = 0;
old_KO_size[RED] = 0;
var old_crew_size = new Array();
old_crew_size[BLUE] = 0;
old_crew_size[RED] = 0;
var script_main_div;
var stats_div;
var check_div;
var histo_div;
var help_div;
var mbl_flash = document.getElementById('mbl');
var fuseau = new Object();
fuseau.diff = 0;
fuseau.wait = false;
fuseau.timer = 0;
fuseau.timer_type = 0;
fuseau.save = 0;
var nb_timers = 0
var main_button = 0;
var main_cancel = 0;
var updating_button = 0;
var read_histo_button = 0;
var check_match_button = new Object();
var timer_match_alert;
var checkbox = new Object();
var sound_game_loaded = false;
var sound_spy_loaded = false;
var cpt_evenement = 0;
var friend_sound = 0;
var passe_sound = 0;
var PA_sound = 0;
var fight_sound = 0;
var opponent_sound = 0;
var connexion_sound = 0;
var deconnexion_sound = 0;
var radio_test_opponent_move = 0;
var radio_test_friend_move = 0;
var radio_test_opponent_passe = 0;
var radio_test_friend_passe = 0;
var radio_test_opponent_fight = 0;
var radio_test_friend_fight = 0;
var radio_test_PA = 0;
var radio_test_connexion = 0;
var radio_test_deconnexion = 0;
var help_on;
var help_off;
var chosen = new Chosen_Guy(0, false, 0, 0, 0, 0, 0);
var first_time = true;
var CM = new Object();
var baseH = 0;
var baseM = 0;
var match = new Object();
match.references = new Array();
match.data = 0;
match.read = 0;
match.minute_add = 0;
match.minute_start = 0;
match.minute_gap = 0;
match.minute_pause = true;
match.playing = true;
var data_OK = false;
var histo = new Object();
histo.cpt_check = 0;
histo.cpt_actions = 0;
histo.lock = false;
histo.actions = new Array();
histo.references = new Array();
histo.refresh = 0;
histo.data = 0;
histo.size = 0;
histo.last_action = 0;
histo.last_read = 0;
histo.read = 0;
histo.nb_monsters = 0;
histo.time = 0;
histo.monster_infos = new Object();
histo.monsterids = new Array();
var hack_for_fast_check = false;
var params = new Object();
var jetlag = 0;
var script_online = false;

// gestion des événements
var ActBoxAppear = 0; // 2 champs
var ActResetMonster = 1; // 3 champs
var ActMove = 2; // 2 champs
var ActDistributeActions = 3; // 1 champ
var ActEnters = 4; // 1 champ
var ActBoxOpened = 5; // 4 champs
var ActFightBall = 6; // 3 champs
var ActGetBall = 7; // 2 champs
var ActPass = 8; // 3 champs
var ActGoal = 9; // 1 champ
var ActBadGoal = 10; // 1 champ
var ActFight = 11; // 6 champs
var ActProjected = 12; // 2 champs
var ActDropBall = 13; // 1 champ
var ActMonsterDie = 14; // 1 champ
var ActEnd = 15; // 0 champ
var actions = new Array();

// calcul du décalage horaire
compute_jetlag();

// ajout des boutons du script
if (!check_errors()) {
	button_area = get_button_area();
	add_func_button(button_area, 'refresh_match', '"F5" Singe', refresh_match, 'Force la mise à jour du terrain de jeu');
	add_text(button_area, 'span', 'space', '', '&nbsp;');
	main_button = add_func_button(button_area, 'start_script', 'Stats Singe', show_script, 'Calcule les statistiques du match avec ce script');
	main_cancel = add_func_cancel_button(button_area, 'stop_script', 'Cacher le script', hide_script , 'Retour à un affichage normal');
	add_text(button_area, 'span', 'space', '', '&nbsp;');
}

// refresh de la mort
function refresh_match () {
	var save = mbl_flash.innerHTML;
	mbl_flash.innerHTML = '';
	mbl_flash.innerHTML = save;
// TODO : test de focus pour le jeu
	document.getElementById('mapSwf').focus();
}

// récupération d'une page autre que la page en cours
function getDOC(url, callback) {
	GM_xmlhttpRequest({
		method: 'GET',
		url: url,
		headers: {
			"User-agent": "Mozilla/4.0 (compatible) Greasemonkey",
			"Content-Type": "text/plain; charset=utf-8",
			"Pragma": "no-cache",
			"Refresh": "1",
		},
		onload: function(responseDetails) {
			if (!responseDetails.responseText || responseDetails.responseText == "") {
				print_error('aucune donnée reçue de : ' + url);
				return;
			}
			var doc;			
			if (!responseDetails.responseXML) doc = new DOMParser().parseFromString(responseDetails.responseText, "text/xml");
			else doc = responseDetails.responseXML;
			callback(doc); // ici je rappelle la fonction qui va extraire l'information
		},
		onerror: function(responseDetails) {
			print_error('erreur pour charger : ' + ur);
		}
	});
}
function change_visibility (type) {
	var x;
	switch (type) {
		case 'stats': x = stats_div; break;
		case 'check': x = check_div; break;
		case 'histo': x = histo_div; break;
		case 'help': x = help_div; break;
		default: return;
	}
	if (x.style.display == 'none') x.style.display = 'block';
	else x.style.display = 'none';
}
function detect_fuseau(doc) {
	return (doc.documentElement.textContent == "");
}

function colored(color, str) {
	return '<span style="color:' + syndicate[color].color_text + ';">' + str + '</span>';
}

// fonctions pour montrer/cacher le script
function hide_script() {
	main_button.style.display = 'inline';
	main_cancel.style.display = 'none';
	script_main_div.style.display = 'none';
}
function show_script() {
	main_button.style.display = 'none';
	main_cancel.style.display = 'inline';
	// mise en page si premier appel
	if (first_time) {
		init();
		first_time = false;
	}
	script_main_div.style.display = 'block';
	update();
	wait_for_data(function () { read_histo(); });
}

// calcul de la différence entre l'heure du monster pod et l'heure réelle
function compute_jetlag() {
	var time = document.getElementById('serverTime');
	if (time == null) return;
	script_online = true;
	time = time.innerHTML;
	CM.h = parseInt(time.substr(0, 2), 10);
	CM.m = parseInt(time.substr(3, 2), 10);
	CM.s = parseInt(time.substr(6, 2), 10);
	jetlag = CM.h - local_time.getHours();
	fuseau.diff = (local_time.getMinutes() * 60 + local_time.getSeconds()) - (CM.m * 60 + CM.s);
}

// calcul de l'heure CM
function compute_time() {
	var time = new Date();
	baseH = time.getHours() + jetlag;
	baseM = time.getMinutes();
}

// donne l'heure correspondante à l'attente d'un nombre de PA
function echeance(wait) {
	if (wait > 0) {
		wait = Math.ceil(wait / match.minute_add) - 1;
		var M = baseM;
		var adjust = M % (2 * match.minute_gap);
		if (adjust >= 0 && adjust < match.minute_start) adjust = match.minute_start - adjust;
		else {
			if (adjust >= (match.minute_start + match.minute_gap) && adjust < (2 * match.minute_gap)) adjust = (2 * match.minute_gap) + match.minute_start - adjust;
			else adjust = match.minute_start + match.minute_gap - adjust;
		}
		M = M + adjust + wait * match.minute_gap;
		var H = baseH + Math.floor(M / 60);
		if (match.minute_pause && H >= 22) H = H - 12;
		if (H >= 24) H = H % 24;
		M = M % 60;
		if (M < 10) M = '0' + M;
		return H + 'h' + M;
	}
	else {
		return 'now!';
	}
}

// affichage des buts
function print_goal(color, i, goal) {
	var color_text = syndicate[color].color_text;
	if (goal <= 0) {
		if (goal <= -2) return '<span style="color:' + color_text + '">But</span><span style="font-size:10px;">(' + (-goal) + ')</span>';
		else return '<span style="color:' + color_text + '">But!</span><span style="font-size:10px;">(' + (-goal) + ')</span>';
	}
	else {
		if (goal == syndicate[color].min_goal) return '<span title="' + echeance(goal) + '"><b>' + goal + '</b><span style="font-size:8px;"> PA</span></span>';
		else return '<span title="' + echeance(goal) + '">' + goal + '<span style="font-size:8px;"> PA</span></span>';
	}
}

// récupération de mon ID CM
function get_my_ID() {
	my_level = document.getElementById('level');
	user_link = my_level.getElementsByTagName('a');
	user_href = user_link[0].getAttribute('href');
	return user_href.substring(user_href.lastIndexOf("/") + 1);
}

// trouver si je suis rouge ou bleu
function get_my_team () {
	if (my_team == NO && script_online) {
		if (isplayer(BLUE, my_ID) != NO || isKO(BLUE, my_ID) != NO|| iscrew(BLUE, my_ID) != NO) return BLUE;
		if (isplayer(RED, my_ID) != NO || isKO(RED, my_ID) != NO || iscrew(RED, my_ID) != NO) return RED;
	}
	return NO;
}

// récupération de la zone des boutons
function get_button_area() {
	listp = document.getElementById('content').getElementsByTagName('p');
	return listp[listp.length-1];
}

// fonctions d'ajout de bouton
function add_cancel_button(here, id, text, details) {
	var a = document.createElement('a');
	a.setAttribute('id', GM_info.script.name + '_button_' + id);
	a.setAttribute('class', 'cancelButton');
	a.style.display = 'none';
	a.title = details;
	a.innerHTML = text;
	here.appendChild(a);
	return a;
}
function add_button(here, id, text, details) {
	var a = document.createElement('a');
	a.setAttribute('id', GM_info.script.name + '_button_' + id);
	a.setAttribute('class', 'okButton');
	a.title = details;
	a.innerHTML = text;
	here.appendChild(a);
	return a;
}
function add_link_button(here, id, text, href, details) {
	var a = add_button(here, id, text, details);
	a.href = href;
	a.target = "_blank";
	return a;
}
function add_func_button(here, id, text, func, details) {
	var a = add_button(here, id, text, details);
	a.addEventListener('click', func, false);
	return a;
}
function add_func_cancel_button(here, id, text, func, details) {
	var a = add_cancel_button(here, id, text, details);
	a.addEventListener('click', func, false);
	return a;
}
function add_func_link(here, id, text, func, details) {
	if (here.firstChild) here.removeChild(here.firstChild);
	var a = document.createElement('a');
	a.setAttribute('id', GM_info.script.name + '_link_' + id);
	a.title = details;
	a.innerHTML = text;
	a.addEventListener('click', func, false);
	here.appendChild(a);
	return a;
}

// ajout des div et span pour la mise en page
function add(here, type, id) {
	var x = document.createElement(type);
	if (id != '') x.setAttribute('id', GM_info.script.name + '_' + type + '_' + id);
	here.appendChild(x);
	return x;
}
function add_text(here, type, id, style, text) {
	var x = add(here, type, id);
	x.setAttribute('style', style);
	x.innerHTML = text;
	return x;
}
function add_input_checkbox(here, name, checked) {
	var x = document.createElement('input');
	x.setAttribute('type', 'checkbox');
	x.setAttribute('id', GM_info.script.name + '_input_checkbox.' + name);
	x.setAttribute('name', name);
	x.checked = checked;
	here.appendChild(x);
	return x;
}
function add_input_radio(here, name, id, checked) {
	var x = document.createElement('input');
	x.setAttribute('type', 'radio');
	x.setAttribute('name', name);
	x.setAttribute('id', GM_info.script.name + '_input_radio_' + id);
	x.checked = checked;
	here.appendChild(x);
	return x;
}
function add_title (type, text) {
	var x = add_text(script_main_div, 'h2', type + '_title', 'margin:0px;padding:0px;border-width:1px 0px;border-style:dashed;', '');
	add_func_button(x, 'change_' + type + '_visibility', '&nbsp;&nbsp;<img src="/gfx/forum/control.gif">', function () { change_visibility(type); }, 'Affiche ou cache cette zone');
	add_text(x, 'span', type + 'title_label', '', '&nbsp;' + text + '&nbsp;');
	return x;
}
function add_main (type) {
	return add_text(script_main_div, 'div', type + '_main', 'display:block;clear:both;overflow:visible;', '');
}
function add_footer (here) {
	add_text(here, 'span', 'footer', 'display:block;clear:both;width:100%;', '&nbsp;');
}
function add_tip(element, title, content) {
	element.setAttribute('onmouseout', 'Tip.hide();');
	var temp = 'null';
	if (title != '') temp = '\'' + title + '\'';
	element.setAttribute('onmouseover', 'Tip.show(' + temp + ',\'' + content + '\',null,event);');
}
function remove_tip(element) {
	element.removeAttribute('onmouseout');
	element.removeAttribute('onmouseover');
}

// fonctions de création des zones de réception des statistiques
function add_opponent(color, index) {
	var id = 'tr_player' + color + index;
	var my_tr = add(syndicate[color].player_body, 'tr', id);
	var odd = index % 2;
	if (odd) my_tr.setAttribute('class', 'false');
	else my_tr.setAttribute('class', 'true');
	// status
	id = 'player_status_' + color + index;
	var my_td = add(my_tr, 'td', id);
	my_td.setAttribute('style', 'width:20px;');
	syndicate[color].player[index].status_zone = add_text(my_td, 'div', id, 'height:20px;', syndicate[color].player[index].status);
	// name
	syndicate[color].player[index].name_zone = add(my_tr, 'td', 'player_name_' + color + index);
	// owner
	id = 'player_owner_' + color + index;
	my_td = add(my_tr, 'td', id);
	my_td.setAttribute('style', 'width:15px;');
	syndicate[color].player[index].owner_zone = add_text(my_td, 'div', id, 'height:15px;', '');
	// PA
	syndicate[color].player[index].PA_zone = add(my_tr, 'td', 'player_PA_' + color + index);
	// combat
	syndicate[color].player[index].combat_zone = add(my_tr, 'td', 'player_combat_' + color + index);
	// life
	syndicate[color].player[index].life_zone = add(my_tr, 'td', 'player_life_' + color + index);
	// but
	syndicate[color].player[index].but_zone = add(my_tr, 'td', 'player_but_' + color + index);
	// attaque
	syndicate[color].player[index].attaque_zone = add(my_tr, 'td', 'player_attaque_' + color + index);
	// defense
	syndicate[color].player[index].defense_zone = add(my_tr, 'td', 'player_defense_' + color + index);
}
function add_friend(color, index) {
	var id = 'tr_player' + color + index;
	var my_tr = add(syndicate[color].player_body, 'tr', id);
	var odd = index % 2;
	if (odd) my_tr.setAttribute('class', 'false');
	else my_tr.setAttribute('class', 'true');
	// status
	id = 'player_status_' + color + index;
	var my_td = add(my_tr, 'td', id);
	my_td.setAttribute('style', 'width:20px;');
	syndicate[color].player[index].status_zone = add_text(my_td, 'div', id, 'height:20px;', syndicate[color].player[index].status);
	// name
	syndicate[color].player[index].name_zone = add(my_tr, 'td', 'player_name_' + color + index);
	// owner
	id = 'player_owner_' + color + index;
	my_td = add(my_tr, 'td', id);
	my_td.setAttribute('style', 'width:15px;');
	syndicate[color].player[index].owner_zone = add_text(my_td, 'div', id, 'height:15px;', '');
	// PA
	syndicate[color].player[index].PA_zone = add(my_tr, 'td', 'player_PA_' + color + index);
	// combat
	syndicate[color].player[index].combat_zone = add(my_tr, 'td', 'player_combat_' + color + index);
	// life
	syndicate[color].player[index].life_zone = add(my_tr, 'td', 'player_life_' + color + index);
	// but
	syndicate[color].player[index].but_zone = add(my_tr, 'td', 'player_but_' + color + index);
	// passe
	syndicate[color].player[index].passe_zone = add(my_tr, 'td', 'player_passe_' + color + index);
	// reception
	syndicate[color].player[index].reception_zone = add(my_tr, 'td', 'player_reception_' + color + index);
}
function add_player(color, index) {
	var id = 'tr_player' + color + index;
	var my_tr = add(syndicate[color].player_body, 'tr', id);
	var odd = index % 2;
	if (odd) my_tr.setAttribute('class', 'false');
	else my_tr.setAttribute('class', 'true');
	// status
	id = 'player_status_' + color + index;
	var my_td = add(my_tr, 'td', id);
	my_td.setAttribute('style', 'width:20px;');
	syndicate[color].player[index].status_zone = add_text(my_td, 'div', id, 'height:20px;', syndicate[color].player[index].status);
	// name
	syndicate[color].player[index].name_zone = add(my_tr, 'td', 'player_name_' + color + index);
	// owner
	id = 'player_owner_' + color + index;
	my_td = add(my_tr, 'td', id);
	my_td.setAttribute('style', 'width:15px;');
	syndicate[color].player[index].owner_zone = add_text(my_td, 'div', id, 'height:15px;', '');
	// PA
	syndicate[color].player[index].PA_zone = add(my_tr, 'td', 'player_PA_' + color + index);
	// combat
	syndicate[color].player[index].combat_zone = add(my_tr, 'td', 'player_combat_' + color + index);
	// life
	syndicate[color].player[index].life_zone = add(my_tr, 'td', 'player_life_' + color + index);
	// but
	syndicate[color].player[index].but_zone = add(my_tr, 'td', 'player_but_' + color + index);
}
function add_KO(color, index) {
	var id = 'tr_KO' + color + index;
	var my_tr = add(syndicate[color].KO_body, 'tr', id);
	var odd = index % 2;
	if (odd) my_tr.setAttribute('class', 'false');
	else my_tr.setAttribute('class', 'true');
	// status
	id = 'KO_status_' + color + index;
	var my_td = add(my_tr, 'td', id);
	my_td.setAttribute('style', 'width:60px;');
	syndicate[color].KO[index].status_zone = add_text(my_td, 'div', id, 'height:20px;', '');
	// name
	syndicate[color].KO[index].name_zone = add(my_tr, 'td', 'KO_name_' + color + index);
	syndicate[color].KO[index].name_zone.setAttribute('style', 'text-align:left;');
	// combat
	syndicate[color].KO[index].combat_zone = add(my_tr, 'td', 'KO_combat_' + color + index);
	// life
	syndicate[color].KO[index].life_zone = add(my_tr, 'td', 'KO_life_' + color + index);
}
function add_agency(color, index) {
	var id = 'tr_crew' + color + index;
	var my_tr = add(syndicate[color].crew_body, 'tr', id);
	var odd = index % 2;
	if (odd) my_tr.setAttribute('class', 'false');
	else my_tr.setAttribute('class', 'true');
	// status
	id = 'crew_status_' + color + index;
	var my_td = add(my_tr, 'td', id);
	my_td.setAttribute('style', 'width:60px;');
	syndicate[color].crew[index].status_zone = add_text(my_td, 'div', id, 'height:20px;', '');
	// name
	syndicate[color].crew[index].name_zone = add(my_tr, 'td', 'crew_name_' + color + index);
	syndicate[color].crew[index].name_zone.setAttribute('style', 'text-align:left;');
	syndicate[color].crew[index].name_zone.innerHTML = syndicate[color].crew[index].name;
}

function remove_table(color, type) {
	var test = false;
	var div;

	switch (type) {
	case PLAYER:
		if (syndicate[color].player_body != 0) test = true;
		syndicate[color].player_body = 0;
		div = syndicate[color].player_div;
		break;
	case KO:
		if (syndicate[color].KO_body != 0) test = true;
		syndicate[color].KO_body = 0;
		div = syndicate[color].KO_div;
		break;
	case SPY:
		if (syndicate[color].check_body != 0) test = true;
		syndicate[color].check_body = 0;
		div = syndicate[color].check_div;
		break;
	default: // case CREW
		if (syndicate[color].crew_body != 0) test = true;
		syndicate[color].crew_body = 0;
		div = syndicate[color].crew_div;
	}
	if (test) div.removeChild(div.firstChild);
}

function add_table(here, color, type) {
	var t = document.createElement('table');
	t.setAttribute('id', GM_info.script.name + '_table_' + color + type);
	t.setAttribute('style', 'width:100%;text-align:left;');
	var c = t.createCaption();
	var t_head = add(t, 'thead', 'head_' + color + type);
	var my_tr = add(t_head, 'tr', '');
	var my_th;
	switch (type) {
	case KO:
		c.innerHTML = 'KO (' + syndicate[color].KO_size + ')';
		my_th = add(my_tr, 'th', '');
		my_th.setAttribute('colspan', '2');
		my_th.innerHTML = 'Agence';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = '<img src="/gfx/icons/fight.gif">';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = '<img src="/gfx/icons/endurance.gif">';
		syndicate[color].KO_body = add(t, 'tbody', 'body_' + color + type);
		syndicate[color].KO_body.setAttribute('style', 'text-align:center;');
		for (i = 0 ; i < syndicate[color].KO_size ; i++) add_KO(color, i);
		break;
	case PLAYER:
		c.innerHTML = 'Joueurs (' + syndicate[color].player_size + ')';
		my_th = add(my_tr, 'th', '');
		my_th.setAttribute('colspan', '3');
		my_th.innerHTML = 'Agence';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'PA';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = '<img src="/gfx/icons/fight.gif">';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = '<img src="/gfx/icons/endurance.gif">';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'but';
		syndicate[color].player_body = add(t, 'tbody', 'body_' + color + type);
		syndicate[color].player_body.setAttribute('style', 'text-align:center;');
		for (i = 0 ; i < syndicate[color].player_size ; i++) add_player(color, i);
		break;
	case OPPONENT:
		c.innerHTML = 'Joueurs (' + syndicate[color].player_size + ')';
		my_th = add(my_tr, 'th', '');
		my_th.setAttribute('colspan', '3');
		my_th.innerHTML = 'Agence';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'PA';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = '<img src="/gfx/icons/fight.gif">';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = '<img src="/gfx/icons/endurance.gif">';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'but';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'attaque';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'défense';
		syndicate[color].player_body = add(t, 'tbody', 'body_' + color + type);
		syndicate[color].player_body.setAttribute('style', 'text-align:center;');
		for (i = 0 ; i < syndicate[color].player_size ; i++) add_opponent(color, i);
		break;
	case FRIEND:
		c.innerHTML = 'Joueurs (' + syndicate[color].player_size + ')';
		my_th = add(my_tr, 'th', '');
		my_th.setAttribute('colspan', '3');
		my_th.innerHTML = 'Agence';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'PA';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = '<img src="/gfx/icons/fight.gif">';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = '<img src="/gfx/icons/endurance.gif">';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'but';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'passe';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'réception';
		syndicate[color].player_body = add(t, 'tbody', 'body_' + color + type);
		syndicate[color].player_body.setAttribute('style', 'text-align:center;');
		for (i = 0 ; i < syndicate[color].player_size ; i++) add_friend(color, i);
		break;
	default: // case CREW
		c.innerHTML = 'Remplaçants (' + syndicate[color].crew_size + ')';
		my_th = add(my_tr, 'th', '');
		my_th.setAttribute('colspan', '2');
		my_th.innerHTML = 'Agence';
		syndicate[color].crew_body = add(t, 'tbody', 'body_' + color + type);
		syndicate[color].crew_body.setAttribute('style', 'text-align:center;');
		for (i = 0 ; i < syndicate[color].crew_size ; i++) add_agency(color, i);
	}

	here.appendChild(t);
	return t;
}

function parse_status (status) {
	var pattern = /o[n,f]*line/m;
	var i = status.length;
	var status_found = '';
	var status_read = '';
	do {
		i--;
		status_read = status[i].innerHTML;
		status_found = status_read.match(pattern);
		if (status_found == null) status_found = '';
	} while (status_found == '' && i > 0);
	if (i == 0) status_found = 'pause';
	status_read = '<img src="/gfx/icons/' + status_found + '.gif">';
	return status_read;
}
function get_status (color, type, index) {
	switch (type) {
	case KO:
		syndicate[color].KO[index].status_zone.innerHTML = '';
		id = syndicate[color].KO[index].id;
		break;
	case CREW:
		syndicate[color].crew[index].status_zone.innerHTML = '';
		id = syndicate[color].crew[index].id;
		break;
	default: // PLAYER, OPPONENT, FRIEND
		syndicate[color].player[index].status_zone.innerHTML = '';
		id = syndicate[color].player[index].id;
	}
	getDOC(GM_info.script.namespace + '/user/' + id, function (guy) {
		if (detect_fuseau(guy)) return;
		var status = guy.getElementsByClassName('aBar')[0].getElementsByTagName('span');
		var status_read = parse_status(status);
		switch (type) {
			case KO:
				syndicate[color].KO[index].status = status_read;
				syndicate[color].KO[index].status_zone.innerHTML = syndicate[color].KO[index].status;
				break;
			case CREW:
				syndicate[color].crew[index].status = status_read;
				syndicate[color].crew[index].status_zone.innerHTML = syndicate[color].crew[index].status;
				break;
			default: // PLAYER, OPPONENT, FRIEND
				syndicate[color].player[index].status = status_read;
				syndicate[color].player[index].status_zone.innerHTML = syndicate[color].player[index].status;
		}
	});
}

function fill_status(color, type) {
	switch (type) {
	case KO:
		for ( i = 0 ; i < syndicate[color].KO_size ; i++ )
			get_status(color, type, i);
		break;
	case CREW:
		for ( i = 0 ; i < syndicate[color].crew_size ; i++ )
			get_status(color, type, i);
		break;
	default: // PLAYER, OPPONENT, FRIEND
		for ( i = 0 ; i < syndicate[color].player_size ; i++ )
			get_status(color, type, i);
	}
}

// fonctions de mises à jour dans les tableaux de statistiques
function fill_stats_one_player(color, index) {
	// nom et lien pour choisir
	var b = add_func_link(	syndicate[color].player[index].name_zone,
					'player_name' + color + index,
					syndicate[color].player[index].name,
					function() {
						chosen.id = syndicate[color].player[index].id;
						chosen.newguy = true;
						update();
					},
					'');
	b.setAttribute('style', 'cursor:pointer');
	if (syndicate[color].player[index].tip) add_tip(b, TIP_TITLE + '<span style="color:' + syndicate[color].color_text + '">' + syndicate[color].player[index].name + '</span>', build_tip(syndicate[color].player[index].mid, syndicate[color].player[index].combat));
	// owner
	if (syndicate[color].player[index].owner) syndicate[color].player[index].owner_zone.innerHTML = '<img src="/gfx/icons/greediness.gif">';
	else syndicate[color].player[index].owner_zone.innerHTML = '';
	// PA
	if (syndicate[color].player[index].PA == FULLPA) syndicate[color].player[index].PA_zone.innerHTML = '<b>' + syndicate[color].player[index].PA + '</b><span style="font-size:8px;"> PA</span>';
	else syndicate[color].player[index].PA_zone.innerHTML = syndicate[color].player[index].PA + '<span style="font-size:8px;"> PA</span>';
	// combat
	syndicate[color].player[index].combat_zone.innerHTML = syndicate[color].player[index].combat;
	// life
	syndicate[color].player[index].life_zone.innerHTML = '<span title="vie max = ' + syndicate[color].player[index].lifemax + '">' + syndicate[color].player[index].life + '</span>';
	// but
	syndicate[color].player[index].but_zone.innerHTML = print_goal(color, index, syndicate[color].player[index].but);
}
function fill_stats_one_friend(color, index) {
	// nom et lien pour choisir
	var b;
	if (chosen.id == syndicate[color].player[index].id) {
		b = add_func_link(	syndicate[color].player[index].name_zone,
						'player_name' + color + index,
						'<span style="color:' + chosen.friend_color + '"><b>' + syndicate[color].player[index].name + '</b></span>',
						function() {
							chosen.id = syndicate[color].player[index].id;
							chosen.newguy = false;
							update();
						}, '');
	}
	else {
		b = add_func_link(	syndicate[color].player[index].name_zone,
						'player_name' + color + index,
						syndicate[color].player[index].name,
						function() {
							chosen.id = syndicate[color].player[index].id;
							chosen.newguy = true;
							update();
						},
						'');
	}
	b.setAttribute('style', 'cursor:pointer');
	if (syndicate[color].player[index].tip) add_tip(b, TIP_TITLE + '<span style="color:' + syndicate[color].color_text + '">' + syndicate[color].player[index].name + '</span>', build_tip(syndicate[color].player[index].mid, syndicate[color].player[index].combat));
	// owner
	if (syndicate[color].player[index].owner) syndicate[color].player[index].owner_zone.innerHTML = '<img src="/gfx/icons/greediness.gif">';
	else syndicate[color].player[index].owner_zone.innerHTML = '';
	// PA
	if (syndicate[color].player[index].PA == FULLPA) syndicate[color].player[index].PA_zone.innerHTML = '<b>' + syndicate[color].player[index].PA + '</b><span style="font-size:8px;"> PA</span>';
	else syndicate[color].player[index].PA_zone.innerHTML = syndicate[color].player[index].PA + '<span style="font-size:8px;"> PA</span>';
	// combat
	syndicate[color].player[index].combat_zone.innerHTML = syndicate[color].player[index].combat;
	// life
	syndicate[color].player[index].life_zone.innerHTML = '<span title="vie max = ' + syndicate[color].player[index].lifemax + '">' + syndicate[color].player[index].life + '</span>';
	// but
	syndicate[color].player[index].but_zone.innerHTML = print_goal(color, index, syndicate[color].player[index].but);
	// passe et reception mises à jour dans la fonction compute_pass
}
function fill_stats_one_opponent(color, index) {
	// nom et lien pour choisir
	var b = add_func_link(	syndicate[color].player[index].name_zone,
					'player_name' + color + index,
					syndicate[color].player[index].name,
					function() {
						chosen.id = syndicate[color].player[index].id;
						chosen.newguy = true;
						update();
					},
					'');
	b.setAttribute('style', 'cursor:pointer');
	if (syndicate[color].player[index].tip) add_tip(b, TIP_TITLE + '<span style="color:' + syndicate[color].color_text + '">' + syndicate[color].player[index].name + '</span>', build_tip(syndicate[color].player[index].mid, syndicate[color].player[index].combat));
	// owner
	if (syndicate[color].player[index].owner) syndicate[color].player[index].owner_zone.innerHTML = '<img src="/gfx/icons/greediness.gif">';
	else syndicate[color].player[index].owner_zone.innerHTML = '';
	// PA
	if (syndicate[color].player[index].PA == FULLPA) syndicate[color].player[index].PA_zone.innerHTML = '<b>' + syndicate[color].player[index].PA + '</b><span style="font-size:8px;"> PA</span>';
	else syndicate[color].player[index].PA_zone.innerHTML = syndicate[color].player[index].PA + '<span style="font-size:8px;"> PA</span>';
	// combat
	syndicate[color].player[index].combat_zone.innerHTML = syndicate[color].player[index].combat;
	// life
	syndicate[color].player[index].life_zone.innerHTML = '<span title="vie max = ' + syndicate[color].player[index].lifemax + '">' + syndicate[color].player[index].life + '</span>';
	// but
	syndicate[color].player[index].but_zone.innerHTML = print_goal(color, index, syndicate[color].player[index].but);
	// attaque et défense mises à jour dans la fonction compute_fight
}
function fill_stats_one_KO(color, index) {
	// nom
	syndicate[color].KO[index].name_zone.innerHTML = syndicate[color].KO[index].name;
	if (syndicate[color].KO[index].tip) add_tip(syndicate[color].KO[index].name_zone, TIP_TITLE + '<span style="color:' + syndicate[color].color_text + '">' + syndicate[color].KO[index].name + '</span>', build_tip(syndicate[color].KO[index].mid, syndicate[color].KO[index].combat));
	// combat
	syndicate[color].KO[index].combat_zone.innerHTML = syndicate[color].KO[index].combat;
	// life
	syndicate[color].KO[index].life_zone.innerHTML = syndicate[color].KO[index].lifemax;
}
function fill_stats_player(color) {
	var i;
	switch (syndicate[color].type) {
	case FRIEND:
		for ( i = 0 ; i < syndicate[color].player_size ; i++ )
			fill_stats_one_friend(color, i);
		break;
	case OPPONENT:
		for ( i = 0 ; i < syndicate[color].player_size ; i++ )
			fill_stats_one_opponent(color, i);
		break;
	default: //case PLAYER
		for ( i = 0 ; i < syndicate[color].player_size ; i++ )
			fill_stats_one_player(color, i);
	}

	// Résumé des joueurs
	if (syndicate[color].player_size > 0) {
		var PA = 0;
		var PC = 0;
		var PV = 0;
		var avance = 0;
		for ( i = 0 ; i < syndicate[color].player_size ; i++ ) {
			PA += syndicate[color].player[i].PA;
			PC += syndicate[color].player[i].combat;
			PV += syndicate[color].player[i].life;
			if (color == BLUE) avance += syndicate[color].player[i].posX;
			else avance += RED_SIDE - syndicate[color].player[i].posX;
		}
		add_tip(syndicate[color].status_button, syndicate[color].player_size + ' joueurs sur le terrain', 'Cliquez pour connaître les présences des joueurs de <span style="color:' + syndicate[color].color_text + '">' + syndicate[color].name + '</span><br><br><strong>Joueur moyen</strong><br>' + (Math.round(PA / syndicate[color].player_size * 100)/100) + ' PA / <img src=\\\'/gfx/icons/fight.gif\\\' width=\\\'13\\\'> ' + (Math.round(PC / syndicate[color].player_size * 100)/100) + ' / <img src=\\\'/gfx/icons/endurance.gif\\\' width=\\\'13\\\'> ' + (Math.round(PV / syndicate[color].player_size * 100)/100) + '<br>Avance sur le terrain : ' + (Math.round(avance / syndicate[color].player_size * 100)/100) + '<br>Avance + PA : ' + (Math.round((avance+PA) / syndicate[color].player_size * 100)/100) + '<br><br><strong>Statistiques cumulées</strong><br>' + PA + ' PA / <img src=\\\'/gfx/icons/fight.gif\\\' width=\\\'13\\\'> ' + PC + ' / <img src=\\\'/gfx/icons/endurance.gif\\\' width=\\\'13\\\'> ' + PV + '<br>Avance sur le terrain : ' + avance + '<br>Avance + PA : ' + (avance+PA));
	}
	else {
		add_tip(syndicate[color].status_button, 'Aucun joueur sur le terrain', 'Cliquez pour connaître les présences des joueurs de <span style="color:' + syndicate[color].color_text + '">' + syndicate[color].name + '</span>');
	}
}
function fill_stats_KO(color) {
	for ( var i = 0 ; i < syndicate[color].KO_size ; i++ )
		fill_stats_one_KO(color, i);
}

// fonctions de lecture du fichier de données MBL
function int_size(x) {
	if (x == 0) return 1;
	return Math.floor(Math.log(x) / Math.log(10)) + 1;
}
function M_get_int() {
	var ref;
	ref = parseInt(match.data.substr(match.read), 10);
	match.read = match.read + int_size(ref);
	return ref;
}
function M_get_integer_field() {
	var integer;
	if (match.data[match.read] == 'i') {
		match.read++;
		integer = parseInt(match.data.substr(match.read), 10);
		match.read = match.read + int_size(integer);
	}
	else {
		// si match.read est sur 'n' ou 'z'
		integer = 0;
		match.read++;
	}
	return integer;
}
function M_get_minute () {
	return parseInt(match.data.substr(match.read+15, 2), 10);
}
function M_get_date () {
	match.read++;
	var Y = parseInt(match.data.substr(match.read, 4), 10);
	match.read+=5;
	var M = parseInt(match.data.substr(match.read, 2), 10)-1;
	match.read+=3;
	var D = parseInt(match.data.substr(match.read, 2), 10);
	match.read+=3;
	var H = parseInt(match.data.substr(match.read, 2), 10);
	match.read+=3;
	var Min = parseInt(match.data.substr(match.read, 2), 10);
	match.read+=3;
	var S = parseInt(match.data.substr(match.read, 2), 10);
	match.read+=2;
	var date = new Date(Y, M, D, H, Min, S, 0);
	return date;
}
function M_skip_string() {
	var size = M_get_int();
	match.read++;
	match.references.push(match.data.substr(match.read, size));
	match.read = match.read + size;
}
function M_get_string() {
	var size = M_get_int();
	match.read++;
	string = match.data.substr(match.read, size);
	match.read = match.read + size;
	match.references.push(string);
	return string;
}
function player_ready(team_id, id, monster_id, name, ball, PA, posX, posY, combat, life, lifemax) {
	// couleur
	var team_color;
	if (syndicate[BLUE].id == team_id) team_color = BLUE;
	else team_color = RED;

	// mise à jour ou ajout
	if (life == 0) {
		// chosen guy KO
		if (id == chosen.id) {
			chosen.id = 0;
			chosen.newguy = true;
		}
		// KO
		if (isKO(team_color, id) == NO) {
			syndicate[team_color].KO[syndicate[team_color].KO_size++] = new Ko(id, monster_id, '', name, combat, lifemax);
			// le cas échéant, retirer de player
			var player_index = isplayer(team_color, id);
			if (player_index != NO) {
				if (syndicate[team_color].player[player_index].cpt_spy > 0) stop_one_spy(team_color, player_index);
				syndicate[team_color].KO[syndicate[team_color].KO_size-1].status = syndicate[team_color].player[player_index].status;
				if (player_index != syndicate[team_color].player_size - 1) copy_last_player(player_index, team_color);
				syndicate[team_color].player.pop();
				syndicate[team_color].player_size--;
			}
			// le cas échéant, retirer de crew
			var crew_index = iscrew(team_color, id);
			if (crew_index != NO) {
				syndicate[team_color].KO[syndicate[team_color].KO_size-1].status = syndicate[team_color].crew[crew_index].status;
				if (crew_index != syndicate[team_color].crew_size - 1) copy_last(crew_index, team_color);
				syndicate[team_color].crew.pop();
				syndicate[team_color].crew_size--;
			}
		}
	}
	else {
		// PLAYER
		table_id = isplayer(team_color, id);
		if (table_id != NO) {
			syndicate[team_color].player[table_id].owner = ball;
			syndicate[team_color].player[table_id].PA = PA;
			syndicate[team_color].player[table_id].posX = posX;
			syndicate[team_color].player[table_id].posY = posY;
			syndicate[team_color].player[table_id].life = life;
		}
		else {
			syndicate[team_color].player[syndicate[team_color].player_size++] = new Player(id, monster_id, '', name, ball, PA, posX, posY, combat, life, lifemax);
			// le cas échéant, retirer de crew
			var crew_index = iscrew(team_color, id);
			if (crew_index != NO) {
				syndicate[team_color].player[syndicate[team_color].player_size-1].status = syndicate[team_color].crew[crew_index].status;
				if (crew_index != syndicate[team_color].crew_size - 1) copy_last_crew(crew_index, team_color);
				syndicate[team_color].crew.pop();
				syndicate[team_color].crew_size--;
			}
		}
	}
}

// lecture intelligente du fichier du match (instantané)
function M_parse_player() {
	var dummy = 0;
	var nb_local_objects = 0;
	var type = '';
	var title = '';
	var reading = true;
	while (reading) {
		type = match.data[match.read++];
		switch (type) {
			case 'o': nb_local_objects++; break;
			case 'g': nb_local_objects--; if (nb_local_objects == 0) reading = false; break;
			case 'l': case 'h': case 'w': case 'n': case 'z': case 't': case 'f': break;
			case ':': case 'i': dummy = M_get_int(); break;
			default:
				if (type == 'y') title = M_get_string();
				else title = match.references[M_get_int()]; // type == 'R'
				switch (title) {
					case "life": var life = M_get_integer_field(); break;
					case "teamId": var team_id = M_get_integer_field(); break;
					case "ballId": var ball = M_get_integer_field(); break;
					case "actions": var PA = M_get_integer_field(); break;
					case "x": var posX = M_get_integer_field(); break;
					case "y": var posY = M_get_integer_field(); break;
					case "id": var monster_id = M_get_integer_field(); break;
					case "lifeMax": var lifemax = M_get_integer_field(); break;
					case "ownerId": var agency_id = M_get_integer_field(); break;
					case "owner":
						type = match.data[match.read++];
						if (type == 'y') var name = M_get_string();
						else var name = match.references[M_get_int()]; // type == 'R'
						break;
					case "fight": var combat = M_get_integer_field(); break;
					case "caracs": break;
					default:
						type = match.data[match.read];
						if (type == 'y') {
							match.read++;
							M_skip_string();
						}
						else {
							if (type == 'R') {
								match.read++;
								dummy = M_get_int();
							}
							else dummy = M_get_integer_field();
						}
				}
		}
	}
	player_ready(team_id, agency_id, monster_id, name, (ball > 0), PA, posX, posY, combat, life, lifemax);
}
function M_parse_monsters() {
	var dummy = 0;
	var type = '';
	var reading = true;
	while (reading) {
		type = match.data[match.read++];
		switch (type) {
			case 'l': case 'w': case 'n': case 'z': break;
			case ':': case 'i': dummy = M_get_int(); break;
			case 'o': match.read--;
			M_parse_player(); break;
			case 'h': reading = false; break;
			default:
				if (type == 'y') {
					match.read++;
					M_skip_string();
				}
				else {
					if (type == 'R') {
						match.read++;
						dummy = M_get_int();
					}
					else dummy = M_get_integer_field();
				}
		}
	}
}
function parse_match() {
	match.references.splice(0, match.references.length);
	match.read = 0;
	var dummy = 0;
	var type = '';
	var title = '';
	var nb_fields_ok = 0;
	var reading = true;
	while (reading) {
		type = match.data[match.read++];
		switch (type) {
			case 'o': case 'g': case 'l': case 'h': case 'w': case 'n': case 'z': break;
			case ':': case 'i': dummy = M_get_int(); break;
			default:
				if (type == 'y') title = M_get_string();
				else title = match.references[M_get_int()]; // type == 'R'
				switch (title) {
					case "monsters":
						M_parse_monsters();
						nb_fields_ok++;
						if (nb_fields_ok == 4) reading = false;
						break;
					case "edate":
						var end_day = M_get_date();
						nb_fields_ok++;
						if (nb_fields_ok == 4) reading = false;
						break;
					case "sdate":
						match.minute_start = M_get_minute();
						var start_day = M_get_date();
						nb_fields_ok++;
						if (nb_fields_ok == 4) reading = false;
						break;
					case "mbl.MatchStatus":
						var status = M_get_string();
						if (status == "Ended") match.playing = false;
						nb_fields_ok++;
						if (nb_fields_ok == 4) reading = false;
						break;
					default:
						type = match.data[match.read];
						if (type == 'y') {
							match.read++;
							M_skip_string();
						}
						else {
							if (type == 'R') {
								match.read++;
								dummy = M_get_int();
							}
							else dummy = M_get_integer_field();
						}
				}
		}
	}
	var game_length = (end_day.getTime() - start_day.getTime()) / 3600000;
	// 60 heures => 3 jours (avec pause la nuit), 2 PA toutes les 30 minutes
	if (game_length == 60) {
		match.minute_add = 2;
		match.minute_gap = 30;
		match.minute_pause = true;
	}
	else {
		// 72 heures => 3 jours complets, 2 PA toutes les 60 minutes
		if (game_length == 72) {
			match.minute_add = 2;
			match.minute_gap = 60;
			match.minute_pause = false;
		}
		else {
			// 3 heures, 4 PA toutes les 5 minutes
			match.minute_add = 4;
			match.minute_gap = 5;
			match.minute_pause = false;
			match.minute_start = match.minute_start % 10;
			if (match.minute_start >= match.minute_gap) match.minute_start = match.minute_start - match.minute_gap;
		}
	}
	if (match.minute_pause && (baseH > 22 || baseH < 10)) {
		baseH = 10;
		baseM = 0;
	}
	// tri des tableaux
	syndicate[BLUE].player.sort(sortfunc);
	syndicate[BLUE].KO.sort(sortfunc);
	syndicate[RED].player.sort(sortfunc);
	syndicate[RED].KO.sort(sortfunc);
}

// calcule les statistiques de tous les joueurs
function compute_stats() {
	minmin = 40;
	player_found = false;
	for (k = BLUE ; k <= RED ; k++ ) {
		// init pour calcul de distance de but
		min_goal = 34;
		if (k == BLUE) {
			var goal_X = PG_XE;
			var op = -1;
		}
		else {
			var goal_X = PG_XB;
			var op = 1;
		}
		for (i = 0 ; i < syndicate[k].player_size ; i++ ) {
			if (syndicate[k].player[i].id == chosen.id) {
				chosen.name_div.innerHTML = '<span style="color:' + chosen.friend_color +  '">' + syndicate[k].player[i].name + '</span>';
				chosen.posX = syndicate[k].player[i].posX;
				chosen.posY = syndicate[k].player[i].posY;
				chosen.PA = syndicate[k].player[i].PA;
				chosen.combat = syndicate[k].player[i].combat;
				chosen.att_max = syndicate[k].player[i].att_max;
				chosen.att_min = syndicate[k].player[i].att_min;
				chosen.att_nb = syndicate[k].player[i].att_nb;
				chosen.def_max = syndicate[k].player[i].def_max;
				chosen.def_min = syndicate[k].player[i].def_min;
				chosen.def_nb = syndicate[k].player[i].def_nb;
				chosen.life = syndicate[k].player[i].life;
				chosen.owner = syndicate[k].player[i].owner;
				player_found = true;
			}
			var DB = goal_X + op * syndicate[k].player[i].posX;
			var goal = DB - syndicate[k].player[i].PA;
			if (goal < min_goal) min_goal = goal;
			syndicate[k].player[i].but = goal;
		}
		if (min_goal < minmin) minmin = min_goal;
		syndicate[k].goal_span.innerHTML = echeance(min_goal);
		syndicate[k].min_goal = minmin;
	}
	if (player_found) {
		var max_move = minmin - (FULLPA - chosen.PA);
		if (max_move > chosen.PA) max_move = chosen.PA;
		if (max_move < 0) max_move = 0;
		chosen.max_move_div.innerHTML = max_move + (max_move % 2);
		chosen.full_PA_div.innerHTML = echeance(FULLPA - chosen.PA);
	}
}

// calcule de la distance entre 2 joueurs
function distance (posX1, posY1, posX2, posY2) {
	var dX;
	if (posX1 > posX2) dX = posX1 - posX2;
	else dX = posX2 - posX1;
	var dY;
	if (posY1 > posY2) dY = posY1 - posY2;
	else dY = posY2 - posY1;
	return dX + dY;
}

// calcule les stats de contact par rapport au joueur choisi
function compute_fight(i) {
	// nom de l'adversaire
	var opponent_name = '<span style="color:' + chosen.opponent_color + '">' + syndicate[chosen.opponent].player[i].name + '</span>';
	// distance
	var dist = distance(chosen.posX, chosen.posY, syndicate[chosen.opponent].player[i].posX, syndicate[chosen.opponent].player[i].posY) - 1;
	// zone attaque
	// affichage
	var att_txt = '';
	var def_txt = '';
	var tip_txt = 'Informations';
	var lack_PA = 0;
	var attaque = 0;
	var defense = 0;
	// PA
	var contact = chosen.PA - dist;
	var attaque_KO = Math.ceil(syndicate[chosen.opponent].player[i].life / (ATTAQUE_PERCENT * chosen.combat));
	var defense_KO = Math.ceil(chosen.life / (DEFENSE_PERCENT * syndicate[chosen.opponent].player[i].combat));
	var PA_KO = Math.min(attaque_KO, defense_KO);
	// chances de pousser un adversaire
	var att_100 = Math.max(0, chosen.att_max - Math.max(chosen.att_min, syndicate[chosen.opponent].player[i].def_max) + 1);
	var att_0 = Math.max(0, Math.min(chosen.att_max, syndicate[chosen.opponent].player[i].def_min) - chosen.att_min + 1);
	var ZC_min = Math.max(chosen.att_min, syndicate[chosen.opponent].player[i].def_min);
	var ZC_max = Math.min(chosen.att_max, syndicate[chosen.opponent].player[i].def_max);
	var ZC_nb = Math.max(0, ZC_max - ZC_min + 1);
	var def_0 = Math.max(0, ZC_min - syndicate[chosen.opponent].player[i].def_min + 1);
	var ZC_att = ZC_nb / syndicate[chosen.opponent].player[i].def_nb * ((ZC_nb - 1) / 2  + def_0);
	var att_chances = Math.round((ZC_att + att_100) / (att_0 + ZC_nb + att_100) * 10000) / 100;
	var def_chances = Math.round((100 - att_chances) * 100) / 100;
	tip_txt += '<br>Chances de réussir l\\\'attaque : <strong>' + att_chances + '%</strong>';

	if (contact > 0) {
		// aucun KO
		if (contact < PA_KO) {
			attaque = Math.round(contact * ATTAQUE_PERCENT * chosen.combat);
			defense = Math.round(contact * DEFENSE_PERCENT * syndicate[chosen.opponent].player[i].combat);
			if (attaque_KO < defense_KO) {
				lack_PA = attaque_KO - contact;
				tip_txt += '<br>' + opponent_name + ' sera <strong>KO</strong> dans ' + lack_PA + ' PA (à&nbsp;' + echeance(lack_PA) + ')';
			}
			else {
				lack_PA = defense_KO - contact;
				tip_txt += '<br>' + chosen.name_div.innerHTML + ' sera <strong>KO</strong> dans ' + lack_PA + ' PA (à&nbsp;' + echeance(lack_PA) + ')';
			}
			att_txt = syndicate[chosen.opponent].player[i].life - attaque;
			def_txt = chosen.life - defense;
			tip_txt += '<br>Distance : ' + dist + ' PA';
			tip_txt += '<br>Frappe : ' + contact + ' PA';
			tip_txt += '<br>Déplacement estimé : ' + Math.round(contact*att_chances/100) + ' cases';
			tip_txt += '<br>PV restants pour ' + opponent_name + ' : ' + att_txt;
			tip_txt += '<br>PV restants pour ' + chosen.name_div.innerHTML + ' : ' + def_txt;
		}
		else {
			// si defense_KO < attaque_KO, alors chosen KO avant adversaire
			if (defense_KO < attaque_KO) {
				attaque = Math.round(defense_KO * ATTAQUE_PERCENT * chosen.combat);
				defense = Math.round(defense_KO * DEFENSE_PERCENT * syndicate[chosen.opponent].player[i].combat);
				att_txt = syndicate[chosen.opponent].player[i].life - attaque;
				def_txt = '<strong>KO</strong>';
				tip_txt += '<br>Distance : ' + dist + ' PA';
				tip_txt += '<br>Frappe : ' + defense_KO + ' PA';
				tip_txt += '<br>Déplacement estimé : ' + Math.round(defense_KO*att_chances/100) + ' cases';
				tip_txt += '<br>PV restants pour ' + opponent_name + ' : ' + att_txt;
				tip_txt += '<br>' + chosen.name_div.innerHTML + ' sera KO';
			}
			else {
				attaque = Math.round(attaque_KO * ATTAQUE_PERCENT * chosen.combat);
				defense = Math.round((attaque_KO-1) * DEFENSE_PERCENT * syndicate[chosen.opponent].player[i].combat);
				// ici, adversaire KO, mais en même temps
				if (defense >= chosen.life) {
					att_txt = '<strong>KO</strong>';
					def_txt = '<strong>KO</strong>';
					tip_txt += '<br>Distance : ' + dist + ' PA';
					tip_txt += '<br>Frappe : ' + defense_KO + ' PA';
					tip_txt += '<br>Déplacement estimé : ' + Math.round(defense_KO*att_chances/100) + ' cases';
					tip_txt += '<br>' + chosen.name_div.innerHTML + ' sera KO';
					tip_txt += '<br>' + opponent_name + ' sera KO';
				}
				// seul adversaire KO
				else {
					att_txt = '<strong>KO</strong><span style="font-size:10px;">(' + (chosen.PA - attaque_KO - dist) + ')</span>';
					def_txt = chosen.life - defense;
					tip_txt += '<br>Distance : ' + dist + ' PA';
					tip_txt += '<br>Frappe : ' + attaque_KO + ' PA';
					tip_txt += '<br>Déplacement estimé : ' + Math.round(attaque_KO*att_chances/100) + ' cases';
					tip_txt += '<br>' + opponent_name + ' sera KO';
					tip_txt += '<br>PV restants pour ' + chosen.name_div.innerHTML + ' : ' + def_txt;
					tip_txt += '<br>PA restants pour ' + chosen.name_div.innerHTML + ' : ' + (chosen.PA - attaque_KO - dist);
				}
			}
		}
		// affichage si baston
		syndicate[chosen.opponent].player[i].attaque_zone.innerHTML = '<span style="color:' + chosen.opponent_color + '">' + att_txt + '</span>|<span style="color:' + chosen.friend_color + '">' + def_txt + '</span>';
	}
	else {
		if (attaque_KO < defense_KO) {
			lack_PA = attaque_KO + (-contact);
			tip_txt += '<br>' + opponent_name + ' sera <strong>KO</strong> dans ' + lack_PA + ' PA (à&nbsp;' + echeance(lack_PA) + ')';
		}
		else {
			lack_PA = defense_KO + (-contact);
			tip_txt += '<br>' + chosen.name_div.innerHTML + ' sera <strong>KO</strong> dans ' + lack_PA + ' PA (à&nbsp;' + echeance(lack_PA) + ')';
		}
		// affichage si pas de contact
		syndicate[chosen.opponent].player[i].attaque_zone.innerHTML = (-contact + 1) + '<span style="font-size:8px;"> PA</span>';
	}
	// ajout du tip
	add_tip(syndicate[chosen.opponent].player[i].attaque_zone, chosen.name_div.innerHTML + ' attaque <span style="color:' + chosen.opponent_color + '">' + syndicate[chosen.opponent].player[i].name + '</span>', tip_txt);

	// zone défense
	// affichage
	att_txt = '';
	def_txt = '';
	tip_txt = 'Informations';
	lack_PA = 0;
	attaque = 0;
	defense = 0;
	// PA
	contact = syndicate[chosen.opponent].player[i].PA - dist;
	attaque_KO = Math.ceil(chosen.life / (ATTAQUE_PERCENT * syndicate[chosen.opponent].player[i].combat));
	defense_KO = Math.ceil(syndicate[chosen.opponent].player[i].life / (DEFENSE_PERCENT * chosen.combat));
	PA_KO = Math.min(attaque_KO, defense_KO);
	// chances de pousser un adversaire
	att_100 = Math.max(0, syndicate[chosen.opponent].player[i].att_max - Math.max(syndicate[chosen.opponent].player[i].att_min, chosen.def_max) + 1);
	att_0 = Math.max(0, Math.min(syndicate[chosen.opponent].player[i].att_max, chosen.def_min) - syndicate[chosen.opponent].player[i].att_min + 1);
	ZC_min = Math.max(syndicate[chosen.opponent].player[i].att_min, chosen.def_min);
	ZC_max = Math.min(syndicate[chosen.opponent].player[i].att_max, chosen.def_max);
	ZC_nb = Math.max(0, ZC_max - ZC_min + 1);
	def_0 = Math.max(0, ZC_min - chosen.def_min + 1);
	ZC_att = ZC_nb / chosen.def_nb * ((ZC_nb - 1) / 2  + def_0);
	att_chances = Math.round((ZC_att + att_100) / (att_0 + ZC_nb + att_100) * 10000) / 100;
	def_chances = Math.round((100 - att_chances) * 100) / 100;
	tip_txt += '<br>Chances de réussir la défense : <strong>' + def_chances + '%</strong>';

	if (contact > 0) {
		// aucun KO
		if (contact < PA_KO) {
			attaque = Math.round(contact * ATTAQUE_PERCENT * syndicate[chosen.opponent].player[i].combat);
			defense = Math.round(contact * DEFENSE_PERCENT * chosen.combat);
			if (attaque_KO < defense_KO) {
				lack_PA = attaque_KO - contact;
				if (lack_PA < chosen.min_KO_value) {
					chosen.min_KO_value = lack_PA;
					chosen.min_KO_guy_div.innerHTML = syndicate[chosen.opponent].player[i].name;
				}
				tip_txt += '<br>' + chosen.name_div.innerHTML + ' sera <strong>KO</strong> dans ' + lack_PA + ' PA (à&nbsp;' + echeance(lack_PA) + ')';
			}
			else {
				lack_PA = defense_KO - contact;
				tip_txt += '<br>' + opponent_name + ' sera <strong>KO</strong> dans ' + lack_PA + ' PA (à&nbsp;' + echeance(lack_PA) + ')';
			}
			att_txt = syndicate[chosen.opponent].player[i].life - defense;
			def_txt = chosen.life - attaque;
			tip_txt += '<br>Distance : ' + dist + ' PA';
			tip_txt += '<br>Frappe : ' + contact + ' PA';
			tip_txt += '<br>Déplacement estimé : ' + Math.round(contact*att_chances/100) + ' cases';
			tip_txt += '<br>PV restants pour ' + opponent_name + ' : ' + att_txt;
			tip_txt += '<br>PV restants pour ' + chosen.name_div.innerHTML + ' : ' + def_txt;
		}
		else {
			// si defense_KO < attaque_KO, alors adversaire KO avant chosen
			if (defense_KO < attaque_KO) {
				attaque = Math.round(defense_KO * ATTAQUE_PERCENT * syndicate[chosen.opponent].player[i].combat);
				defense = Math.round(defense_KO * DEFENSE_PERCENT * chosen.combat);
				att_txt = '<strong>KO</strong>';
				def_txt = chosen.life - attaque;
				tip_txt += '<br>Distance : ' + dist + ' PA';
				tip_txt += '<br>Frappe : ' + defense_KO + ' PA';
				tip_txt += '<br>Déplacement estimé : ' + Math.round(defense_KO*att_chances/100) + ' cases';
				tip_txt += '<br>' + opponent_name + ' sera KO';
				tip_txt += '<br>PV restants pour ' + chosen.name_div.innerHTML + ' : ' + def_txt;
			}
			else {
				chosen.min_KO_value = 0;
				chosen.min_KO_guy_div.innerHTML = syndicate[chosen.opponent].player[i].name;
				attaque = Math.round(attaque_KO * ATTAQUE_PERCENT * syndicate[chosen.opponent].player[i].combat);
				defense = Math.round((attaque_KO-1) * DEFENSE_PERCENT * chosen.combat);
				// ici, adversaire KO, mais en même temps
				if (defense >= syndicate[chosen.opponent].player[i].life) {
					att_txt = '<strong>KO</strong>';
					def_txt = '<strong>KO</strong>';
					tip_txt += '<br>Distance : ' + dist + ' PA';
					tip_txt += '<br>Frappe : ' + attaque_KO + ' PA';
					tip_txt += '<br>Déplacement estimé : ' + Math.round(attaque_KO*att_chances/100) + ' cases';
					tip_txt += '<br>' + opponent_name + ' sera KO';
					tip_txt += '<br>' + chosen.name_div.innerHTML + ' sera KO';
				}
				// seul chosen KO
				else {
					att_txt = syndicate[chosen.opponent].player[i].life - defense;
					def_txt = '<strong>KO</strong><span style="font-size:10px;">(' + (syndicate[chosen.opponent].player[i].PA - attaque_KO - dist) + ')</span>';
					tip_txt += '<br>Distance : ' + dist + ' PA';
					tip_txt += '<br>Frappe : ' + attaque_KO + ' PA';
					tip_txt += '<br>Déplacement estimé : ' + Math.round(attaque_KO*att_chances/100) + ' cases';
					tip_txt += '<br>PV restants pour ' + opponent_name + ' : ' + att_txt;
					tip_txt += '<br>PA restants pour ' + opponent_name + ' : ' + (syndicate[chosen.opponent].player[i].PA - attaque_KO - dist);
					tip_txt += '<br>' + chosen.name_div.innerHTML + ' sera KO';
				}
			}
		}
		// affichage si baston
		syndicate[chosen.opponent].player[i].defense_zone.innerHTML = '<span style="color:' + chosen.friend_color + '">' + def_txt + '</span>|<span style="color:' + chosen.opponent_color + '">' + att_txt + '</span>';
	}
	else {
		if (attaque_KO < defense_KO) {
			lack_PA = attaque_KO + (-contact);
			if (lack_PA < chosen.min_KO_value) {
				chosen.min_KO_value = lack_PA;
				chosen.min_KO_guy_div.innerHTML = syndicate[chosen.opponent].player[i].name;
			}
			tip_txt += '<br>' + chosen.name_div.innerHTML + ' sera <strong>KO</strong> dans ' + lack_PA + ' PA (à&nbsp;' + echeance(lack_PA) + ')';
		}
		else {
			lack_PA = defense_KO + (-contact);
			tip_txt += '<br>' + opponent_name + ' sera <strong>KO</strong> dans ' + lack_PA + ' PA (à&nbsp;' + echeance(lack_PA) + ')';
		}
		// affichage si pas de contact
		syndicate[chosen.opponent].player[i].defense_zone.innerHTML = (-contact + 1) + '<span style="font-size:8px;"> PA</span>';
	}
	// ajout du tip
	add_tip(syndicate[chosen.opponent].player[i].defense_zone, chosen.name_div.innerHTML + ' se défend contre ' + opponent_name, tip_txt);
}
function compute_pass(i) {
	if (syndicate[chosen.friend].player[i].id == chosen.id) {
		syndicate[chosen.friend].player[i].passe = '';
		syndicate[chosen.friend].player[i].reception = '';
		return;
	}
	var friend_name = '<span style="color:' + chosen.friend_color + '">' + syndicate[chosen.friend].player[i].name + '</span>';
	// zone passe
	var max_remain = -50;
	var pass1 = false;
	var pass = false;
	var x; var y;
	var zone_txt = '';
	for ( step = 0 ; step < 6 ; step++ ) {
		switch (step) {
		case 0:
			y = syndicate[chosen.friend].player[i].posY + 2;
			x = syndicate[chosen.friend].player[i].posX;
			break;
		case 1:
			y = syndicate[chosen.friend].player[i].posY + 1;
			x = syndicate[chosen.friend].player[i].posX;
			break;
		case 2:
			y = syndicate[chosen.friend].player[i].posY - 2;
			x = syndicate[chosen.friend].player[i].posX;
			break;
		case 3:
			y = syndicate[chosen.friend].player[i].posY - 1;
			x = syndicate[chosen.friend].player[i].posX;
			break;
		case 4:
			y = syndicate[chosen.friend].player[i].posY;
			x = syndicate[chosen.friend].player[i].posX + (SIDE[chosen.friend] * 2);
			break;
		default:
			y = syndicate[chosen.friend].player[i].posY;
			x = syndicate[chosen.friend].player[i].posX + SIDE[chosen.friend];
		}
		var dist = distance(chosen.posX, chosen.posY, x, y) + 1;
		PA_remain = chosen.PA - dist;
		if (PA_remain >= 0) {
			pass = true;
			if (step % 2 == 1) pass1 = true;
		}
		if (PA_remain > max_remain) max_remain = PA_remain;
	}
	if (pass) {
		if (pass1) zone_txt = '<b>Ok</b><span style="font-size:10px;">(' + max_remain + ')</span>';
		else zone_txt = '<b>Ok!</b><span style="font-size:10px;">(' + max_remain + ')</span>';
		add_tip(syndicate[chosen.friend].player[i].passe_zone, '', 'Passe de ' + chosen.name_div.innerHTML + ' à ' + friend_name + '<br>PA restants pour ' + chosen.name_div.innerHTML + ' : ' + max_remain);
	}
	else {
		zone_txt = -max_remain + '<span style="font-size:8px;"> PA</span>';
		add_tip(syndicate[chosen.friend].player[i].passe_zone, '', 'Passe de ' + chosen.name_div.innerHTML + ' à ' + friend_name + '<br>Passe possible dans ' + (-max_remain) + ' PA');
	}
	syndicate[chosen.friend].player[i].passe_zone.innerHTML = zone_txt;

	// zone reception
	max_remain = -50;
	pass1 = false;
	pass = false;
	zone_txt = '';
	for ( step = 0 ; step < 6 ; step++ ) {
		switch (step) {
		case 0:
			y = chosen.posY + 2;
			x = chosen.posX;
			break;
		case 1:
			y = chosen.posY + 1;
			x = chosen.posX;
			break;
		case 2:
			y = chosen.posY - 2;
			x = chosen.posX;
			break;
		case 3:
			y = chosen.posY - 1;
			x = chosen.posX;
			break;
		case 4:
			y = chosen.posY;
			x = chosen.posX + (SIDE[chosen.friend] * 2);
			break;
		default:
			y = chosen.posY;
			x = chosen.posX + SIDE[chosen.friend];
		}
		var dist = distance(syndicate[chosen.friend].player[i].posX, syndicate[chosen.friend].player[i].posY, x, y) + 1;
		PA_remain = syndicate[chosen.friend].player[i].PA - dist;
		if (PA_remain >= 0) {
			pass = true;
			if (step % 2 == 1) pass1 = true;
		}
		if (PA_remain > max_remain) max_remain = PA_remain;
	}
	if (pass) {
		if (pass1) zone_txt = '<b>Ok</b><span style="font-size:10px;">(' + max_remain + ')</span>';
		else zone_txt = '<b>Ok!</b><span style="font-size:10px;">(' + max_remain + ')</span>';
		add_tip(syndicate[chosen.friend].player[i].reception_zone, '', 'Passe de ' + friend_name + ' à ' + chosen.name_div.innerHTML + '<br>PA restants pour ' + friend_name + ' : ' + max_remain);
	}
	else {
		zone_txt = -max_remain + '<span style="font-size:8px;"> PA</span>';
		add_tip(syndicate[chosen.friend].player[i].reception_zone, '', 'Passe de ' + friend_name + ' à ' + chosen.name_div.innerHTML + '<br>Réception possible dans ' + (-max_remain) + ' PA');
	}
	syndicate[chosen.friend].player[i].reception_zone.innerHTML = zone_txt;
}
function compute_contacts() {
	// équipe OPPONENT
	chosen.min_KO_value = INVINCIBLE;
	for ( var i = 0 ; i < syndicate[chosen.opponent].player_size ; i++ ) compute_fight(i);
	if (chosen.min_KO_value == INVINCIBLE) {
		chosen.min_KO_div.innerHTML = 'impossible';
		chosen.min_KO_guy_div.innerHTML = 'tout le monde';
	}
	else chosen.min_KO_div.innerHTML = echeance(chosen.min_KO_value);

	// équipe FRIEND
	for ( var i = 0 ; i < syndicate[chosen.friend].player_size ; i++ ) compute_pass(i);
}

// sauvegarde des tailles des tableaux
function save_sizes () {
	old_player_size = new Array();
	old_player_size[BLUE] = syndicate[BLUE].player_size;
	old_player_size[RED] = syndicate[RED].player_size;
	old_KO_size = new Array();
	old_KO_size[BLUE] = syndicate[BLUE].KO_size;
	old_KO_size[RED] = syndicate[RED].KO_size;
	old_crew_size = new Array();
	old_crew_size[BLUE] = syndicate[BLUE].crew_size;
	old_crew_size[RED] = syndicate[RED].crew_size;
}

// mise à jour des statistiques
function update_stats () {
	if (chosen.newguy) {
		if (isplayer(BLUE, chosen.id) != NO) {
			syndicate[BLUE].type = FRIEND;
			chosen.friend = BLUE;
			chosen.friend_color = "blue";
			syndicate[RED].type = OPPONENT;
			chosen.opponent = RED;
			chosen.opponent_color = "red";
		}
		else {
			if (isplayer(RED, chosen.id) != NO) {
				syndicate[RED].type = FRIEND;
				chosen.friend = RED;
				chosen.friend_color = "red";
				syndicate[BLUE].type = OPPONENT;
				chosen.opponent = BLUE;
				chosen.opponent_color = "blue";
			}
			else {
				syndicate[BLUE].type = PLAYER;
				syndicate[RED].type = PLAYER;
				chosen.friend = NO;
				chosen.friend_color = "black";
				chosen.opponent = NO;
				chosen.opponent_color = "black";
			}
		}
	}

	// tableaux players
	if (old_player_size[BLUE] != syndicate[BLUE].player_size || chosen.newguy || old_KO_size[BLUE] != syndicate[BLUE].KO_size) {
		remove_table(BLUE, PLAYER);
		if (syndicate[BLUE].player_size > 0)
			add_table(syndicate[BLUE].player_div, BLUE, syndicate[BLUE].type);
	}
	if (old_player_size[RED] != syndicate[RED].player_size || chosen.newguy || old_KO_size[RED] != syndicate[RED].KO_size) {
		remove_table(RED, PLAYER);
		if (syndicate[RED].player_size > 0)
			add_table(syndicate[RED].player_div, RED, syndicate[RED].type);
	}

	// espionnage
	if (script_online) {
		if (syndicate[BLUE].check_body == 0 || old_player_size[BLUE] != syndicate[BLUE].player_size) {
			remove_table(BLUE, SPY);
			add_table_check(syndicate[BLUE].check_div, BLUE);
			fill_spies(BLUE);
		}
		if (syndicate[RED].check_body == 0 || old_player_size[RED] != syndicate[RED].player_size) {
			remove_table(RED, SPY);
			add_table_check(syndicate[RED].check_div, RED);
			fill_spies(RED);
		}
	}
	else {
		syndicate[BLUE].check_div.innerHTML = '&nbsp;';
	}

	// calcule des stats
	compute_stats();
	// affichage de l'aide au joueur
	if (chosen.friend != NO) {
		compute_contacts();
		chosen.help_div.style.display = 'block';
	}
	// remplir les tableaux
	fill_stats_player(BLUE);
	fill_stats_player(RED);

	// KO
	if (old_KO_size[BLUE] != syndicate[BLUE].KO_size) {
		remove_table(BLUE, KO);
		add_table(syndicate[BLUE].KO_div, BLUE, KO);
		fill_stats_KO(BLUE);
	}
	if (old_KO_size[RED] != syndicate[RED].KO_size) {
		remove_table(RED, KO);
		add_table(syndicate[RED].KO_div, RED, KO);
		fill_stats_KO(RED);
	}

	// le Crew si besoin
	if (old_crew_size[BLUE] != syndicate[BLUE].crew_size) {
		remove_table(BLUE, CREW);
		add_table(syndicate[BLUE].crew_div, BLUE, CREW);
		fill_status(BLUE, CREW);
	}
	if (old_crew_size[RED] != syndicate[RED].crew_size) {
		remove_table(RED, CREW);
		add_table(syndicate[RED].crew_div, RED, CREW);
		fill_status(RED, CREW);
	}
}

// récupération des infos du match
function get_match_data() {
	getDOC(GM_info.script.namespace + '/mbl/' + match_ID + '/data.xml', function (match_page) {
		if (detect_fuseau(match_page)) return;
		save_sizes();
		match.data = match_page.documentElement.textContent;
		parse_match();
		update_stats();
		updating_button.style.visibility = 'visible';
		data_OK = true;
	});
}

function get_crew(color) {
getDOC(GM_info.script.namespace + '/syndicate/' + syndicate[color].id, function (syndicat) {
	if (detect_fuseau(syndicat)) return;

	if (!syndicate[color].crew_read) {
		syndicate[color].crew_read = true;

		// récupération de la liste des syndiqués
		var crew_list = syndicat.getElementById('membersTable').getElementsByTagName('a');
		// pour chaque joueur trouvé
		var j = 0;
		for (var i = 0 ; i < crew_list.length ; i++) {
			// dirlo hack: reject if pattern "cancel.gif" is found
			var name = crew_list[i].innerHTML;
			var pattern = /cancel.gif/m;
			var reject = '';
			reject = name.match(pattern);
			if (reject != 'cancel.gif') {
				// identifiant du joueur
				var link = crew_list[i].getAttribute('href');
				var id = parseInt(link.substring(link.lastIndexOf("/") + 1), 10);
				// nom du joueur (sans le 'P' de "pouce")
				pattern =/[^ ]+/m;
				name = name.match(pattern);
				// ajout de l'agence dans le tableau si pas déjà dans player ou KO
				if (isplayer(color, id) == NO && isKO(color, id) == NO) {
					syndicate[color].crew[j] = new Crew(id, '', name);
					j++;
				}
			}
		}
		syndicate[color].crew_size = j;
		syndicate[color].crew.sort(sortfunc);
		add_table(syndicate[color].crew_div, color, CREW);
	}
	my_team = get_my_team();
	fill_status(color, CREW);
});}

function check_syndicate(color) {
	fill_status(color, PLAYER);
	fill_status(color, KO);
	if (!syndicate[color].crew_read) get_crew(color);
	else fill_status(color, CREW);
}

// gestion des paramètres
function verify_params() {
	//~ if (params.son != checkbox.son.checked || params.notify != checkbox.notify.checked || params.suivi != checkbox.suivi.checked)
	if (params.son != checkbox.son.checked || params.notify != checkbox.notify.checked)
		set_params();
}
function init_params() {
	checkbox.son.checked = true;
	checkbox.notify.checked = notifications_allowed;
	//~ checkbox.suivi.checked = true;
//~ checkbox.suivi.checked = false;
	set_params();
}
function reinit_params() {
	init_params();
	get_params();
}
function get_params() {
	// récupération des données sauvegardées
	var temp_params = GM_getValue('params_' + my_ID, 'null');
	// lecture si possible
	if (temp_params != 'null') params = eval(temp_params);
	// si les données n'existent pas, initialisation
	else init_params();

// TODO
params.suivi = false;
	checkbox.son.checked = params.son;
	checkbox.notify.checked = params.notify && notifications_allowed;
	//~ checkbox.suivi.checked = params.suivi;
}
function set_params() {
	params.son = checkbox.son.checked;
	params.notify = checkbox.notify.checked && notifications_allowed;
	//~ params.suivi = checkbox.suivi.checked;
//~ params.suivi = false;
//~ checkbox.suivi.checked = false;
	GM_setValue('params_' + my_ID, '(' + JSON.stringify(params) + ')');
}

function print_event(text, flag_heure, flag_action) {
	id = 'event' + cpt_evenement;
	my_tr = document.createElement('tr');
	my_tr.setAttribute('id', GM_info.script.name + '_tr_' + id);
	event_histo_body.insertBefore(my_tr, event_histo_body.firstChild) 
	odd = cpt_evenement % 2;
	if (odd) my_tr.setAttribute('class', 'false');
	else my_tr.setAttribute('class', 'true');
	// heure
	id = 'H' + cpt_evenement;
	my_td = add(my_tr, 'td', id + '_H');
	my_td.setAttribute('style', 'width:80px;text-align:center;');
	if (flag_heure) {
		var d = new Date();
		var H = d.getHours();
		if (H < 10) H = '0' + H;
		var M = d.getMinutes();
		if (M < 10) M = '0' + M;
		var S = d.getSeconds();
		if (S < 10) S = '0' + S;
		add_text(my_td, 'span', '', '', H + ':' + M + ':' + S);
	}
	else {
		add_text(my_td, 'span', '', '', '');
	}
	// numéro de l'action
	my_td = add(my_tr, 'td', id + '_N');
	my_td.setAttribute('style', 'width:80px;text-align:center;');
	if (flag_action) add_text(my_td, 'span', '', '', histo.cpt_actions);
	else add_text(my_td, 'span', '', '', '');
	// événement
	id = 'text' + cpt_evenement;
	my_td = add(my_tr, 'td', id + '_E');
	add_text(my_td, 'span', '', '', text);
	cpt_evenement++;
}

// attente du fuseau
function verify_timer_fuseau () {
	if (nb_timers == 0 && fuseau.wait) {
		if (fuseau.timer_type == INTERVAL) clearInterval(fuseau.timer);
		if (fuseau.timer_type == TIMEOUT) clearTimeout(fuseau.timer);
	}
}
function wait_after_fuseau() {
getDOC(GM_info.script.namespace + '/mbl/' + match_ID + '/data.xml', function (doc) {
	if (!detect_fuseau(doc)) {
		clearInterval(fuseau.timer);
		mbl_flash.innerHTML = fuseau.save;
		fuseau.wait = false;
	}
});}
function wait_fuseau() {
getDOC(GM_info.script.namespace + '/mbl/' + match_ID + '/data.xml', function (doc) {
	if (detect_fuseau(doc)) {
		if (mbl_flash.innerHTML == 'Fuseau !') return;
		fuseau.save = mbl_flash.innerHTML;
		mbl_flash.innerHTML = 'Fuseau !';
		clearInterval(fuseau.timer);
		fuseau.timer = setInterval(wait_after_fuseau, 500);
	}
});}
function start_wait_fuseau() {
	fuseau.timer_type = INTERVAL;
	fuseau.timer = setInterval(wait_fuseau, 500);
}
function anticiper_fuseau() {
	if (!fuseau.wait) {
		var now = new Date();
		var seconds = now.getMinutes() * 60 + now.getSeconds() - fuseau.diff;
		if (seconds < 5) {
			if (!fuseau.wait) {
				fuseau.wait = true;
				fuseau.timer_type = INTERVAL;
				fuseau.timer = setInterval(wait_fuseau, 500);
			}
		}
		if (seconds >= 100) {
			if (!fuseau.wait) {
				fuseau.wait = true;
				fuseau.timer_type = TIMEOUT;
				setTimeout(start_wait_fuseau, (3600 - seconds) * 1000);
			}
		}
	}
}

// construction du tip pour afficher des infos sur les monstres
function build_tip(mid, fight_stat) {
	var txt = '';
	var temp;
	var duree = Math.min(histo.time, histo.monster_infos[mid].infos.KO_time) - histo.monster_infos[mid].infos.enter_time;

	txt += 'Durée de jeu : ' + duree + ' ajouts de PA';
	if (histo.monster_infos[mid].infos.goal != 0)
		txt += '<br><strong>Buts : ' + histo.monster_infos[mid].infos.goal + '</strong>';
	if (histo.monster_infos[mid].infos.bad_goal != 0)
		txt += '<br>Buts CsC : ' + histo.monster_infos[mid].infos.bad_goal;
	if (histo.monster_infos[mid].infos.stop != 0)
		txt += '<br><strong>Interceptions : ' + histo.monster_infos[mid].infos.stop + '</strong>';
	if (histo.monster_infos[mid].infos.box != 0)
		txt += '<br>Boîtes : ' + histo.monster_infos[mid].infos.box;
	if (histo.monster_infos[mid].infos.pass != 0)
		txt += '<br>Passes : ' + histo.monster_infos[mid].infos.pass;
	if (histo.monster_infos[mid].infos.getball != 0)
		txt += '<br>Mioches pris : ' + histo.monster_infos[mid].infos.getball;
	if (histo.monster_infos[mid].infos.KO != 0) {
		txt += '<br><strong>KO : ' + histo.monster_infos[mid].infos.KO + '</strong>';
		txt += '<br> PA des victimes : ' + histo.monster_infos[mid].infos.PA_kill + ' (moyenne : ' + (Math.round(histo.monster_infos[mid].infos.PA_kill/histo.monster_infos[mid].infos.KO*100)/100) + ')';
	}
	if (histo.monster_infos[mid].infos.fight_att != 0) {
		txt += '<br><strong>Coups donnés : ' + histo.monster_infos[mid].infos.fight_att + '</strong>';
		if (histo.monster_infos[mid].infos.PV_kill_att != 0) {
			temp = histo.monster_infos[mid].infos.PV_kill_att/histo.monster_infos[mid].infos.fight_att;
			txt += '<br> PV pris : ' + histo.monster_infos[mid].infos.PV_kill_att + '<br>Moyenne : ' + (Math.round(temp*100)/100) + ' (' + (Math.round(temp/fight_stat*10000)/100) + '% de ' + fight_stat + '<img src=\\\'/gfx/icons/fight.gif\\\' width=\\\'13\\\'>)';
		}
		if (histo.monster_infos[mid].infos.PV_kill_att_min != PV_KILL_INIT_MIN)
			txt += '<br> PV pris MIN : ' + histo.monster_infos[mid].infos.PV_kill_att_min + ' (' + (Math.round(histo.monster_infos[mid].infos.PV_kill_att_min/fight_stat*10000)/100) + '% de ' + fight_stat + '<img src=\\\'/gfx/icons/fight.gif\\\' width=\\\'13\\\'>)';
		if (histo.monster_infos[mid].infos.PV_kill_att_max != PV_KILL_INIT_MAX)
			txt += '<br> PV pris MAX : ' + histo.monster_infos[mid].infos.PV_kill_att_max + ' (' + (Math.round(histo.monster_infos[mid].infos.PV_kill_att_max/fight_stat*10000)/100) + '% de ' + fight_stat + '<img src=\\\'/gfx/icons/fight.gif\\\' width=\\\'13\\\'>)';
	}
	if (histo.monster_infos[mid].infos.fight_def != 0) {
			txt += '<br><strong>Coups rendus : ' + histo.monster_infos[mid].infos.fight_def + '</strong>';
		if (histo.monster_infos[mid].infos.PV_kill_def != 0) {
			temp = histo.monster_infos[mid].infos.PV_kill_def/histo.monster_infos[mid].infos.fight_def;
			txt += '<br> PV pris : ' + histo.monster_infos[mid].infos.PV_kill_def + '<br>Moyenne : ' + (Math.round(temp*100)/100) + ' (' + (Math.round(temp/fight_stat*10000)/100) + '% de ' + fight_stat + '<img src=\\\'/gfx/icons/fight.gif\\\' width=\\\'13\\\'>)';
		}
		if (histo.monster_infos[mid].infos.PV_kill_def_min != PV_KILL_INIT_MIN)
			txt += '<br> PV pris MIN : ' + histo.monster_infos[mid].infos.PV_kill_def_min + ' (' + (Math.round(histo.monster_infos[mid].infos.PV_kill_def_min/fight_stat*10000)/100) + '% de ' + fight_stat + '<img src=\\\'/gfx/icons/fight.gif\\\' width=\\\'13\\\'>)';
		if (histo.monster_infos[mid].infos.PV_kill_def_max != PV_KILL_INIT_MAX)
			txt += '<br> PV pris MAX : ' + histo.monster_infos[mid].infos.PV_kill_def_max + ' (' + (Math.round(histo.monster_infos[mid].infos.PV_kill_def_max/fight_stat*10000)/100) + '% de ' + fight_stat + '<img src=\\\'/gfx/icons/fight.gif\\\' width=\\\'13\\\'>)';
	}
	txt += '<br><strong>PA perdus</strong>';
	txt += '<br>Full PA : ' + histo.monster_infos[mid].infos.PA_lost_full;
	txt += '<br>Trop avancé inutilement : ' + histo.monster_infos[mid].infos.PA_lost_goal;

	return txt;
}

// attente des affichages du match pour lancer l'historique
function wait_for_data(func) {
	if (!data_OK) { setTimeout(function () { wait_for_data(func); }, 10); }
	else func();
}

// lecture de l'historique
function wait_for_histo(func) {
	if (histo.lock) { setTimeout(function () { wait_for_histo(func); }, 10); }
	else {
		histo.lock = true;
		func();
	}
}
function H_get_int() {
	var ref;
	ref = parseInt(histo.data.substr(histo.read), 10);
	histo.read = histo.read + int_size(ref);
	return ref;
}
function H_get_integer_field() {
	var integer;
	if (histo.data[histo.read] == 'i') {
		histo.read++;
		integer = parseInt(histo.data.substr(histo.read), 10);
		histo.read = histo.read + int_size(integer);
	}
	else {
		// si histo.read est sur 'n' ou 'z'
		integer = 0;
		histo.read++;
	}
	return integer;
}
function H_get_minute () {
	return parseInt(histo.data.substr(histo.read+15,2), 10);
}
function H_get_date () {
	// pour le moment, je ne veux que le jour
	histo.read += 9;
	var day = histo.data.substr(histo.read,2);
	histo.read += 11;
	return parseInt(day, 10);
}
function H_skip_string() {
	var size = H_get_int();
	histo.read++;
	histo.references.push(histo.data.substr(histo.read, size));
	histo.read = histo.read + size;
}
function H_get_string() {
	var size = H_get_int();
	histo.read++;
	string = histo.data.substr(histo.read, size);
	histo.read = histo.read + size;
	histo.references.push(string);
	return string;
}
function H_print_move(nb, id, direction, flag) {
	var image = '';
	var sens = '';
	histo.monster_infos[id].infos.PA -= nb;
	histo.monster_infos[id].infos.move += nb;
	histo.monster_infos[id].infos.new_tip = true;
	switch (direction) {
		case 'Right':
			image = '<img src="/gfx/forum/icon_arrow.gif" width="13px"> ';
			sens = 'vers la <strong>droite</strong>';
			histo.monster_infos[id].infos.X += nb;
			break;
		case 'Left':
			image = '<img src="/gfx/forum/icon_arrow.gif" style="transform:rotate(180deg);" width="13px"> ';
			sens = 'vers la <strong>gauche</strong>';
			histo.monster_infos[id].infos.X -= nb;
			break;
		case 'Up':
			image = '<img src="/gfx/forum/icon_arrow.gif" style="transform:rotate(270deg);" width="13px"> ';
			sens = 'vers le <strong>haut</strong>';
			break;
		case 'Down':
			image = '<img src="/gfx/forum/icon_arrow.gif" style="transform:rotate(90deg);" width="13px"> ';
			sens = 'vers le <strong>bas</strong>';
			break;
		default:
			image = '<img src="/gfx/forum/icon_question.gif" width="13px"> ';
			sens = '## erreur de lecture ##';
			break;
	}
	// affichage toujours après le comptage de l'action qui suit le déplacement
	histo.cpt_actions--;
	print_event(image + histo.monster_infos[id].name + ' se déplace ' + sens + ' de <strong>' + nb + '</strong> PA', flag, true);
	histo.cpt_actions++;
}
function H_get_direction() {
	var type = '';
	var dummy = '';
	var direction = '';
	histo.read++;
	type = histo.data[histo.read++];
	if (type == 'y') H_skip_string();
	else dummy = H_get_int();
	type = histo.data[histo.read++];
	if (type == 'y') direction = H_get_string();
	else direction = histo.references[H_get_int()];
	histo.read++;
	dummy = H_get_int();
	return direction;
}
function H_parse_new_monster (flag) {
	var dummy = 0;
	var nb_local_objects = 0;
	var type = '';
	var title = '';
	var reading = true;
	while (reading) {
		type = histo.data[histo.read++];
		switch (type) {
			case 'o': nb_local_objects++; break;
			case 'g': nb_local_objects--; if (nb_local_objects == 0) reading = false; break;
			case 'l': case 'h': case 'w': case 'n': case 'z': case 't': case 'f': break;
			case ':': case 'i': dummy = H_get_int(); break;
			default:
				if (type == 'y') title = H_get_string();
				else title = histo.references[H_get_int()]; // type == 'R'
				switch (title) {
					case "life": var life = H_get_integer_field(); break;
					case "x": var posX = H_get_integer_field(); break;
					case "id": var monster_id = H_get_integer_field(); break;
					case "owner":
						type = histo.data[histo.read++];
						if (type == 'y') var name = H_get_string();
						else var name = histo.references[H_get_int()]; // type == 'R'
						break;
					case "fight": var combat = H_get_integer_field(); break;
					case "caracs": break;
					default:
						type = histo.data[histo.read];
						if (type == 'y') {
							histo.read++;
							H_skip_string();
						}
						else {
							if (type == 'R') {
								histo.read++;
								dummy = H_get_int();
							}
							else dummy = H_get_integer_field();
						}
				}
		}
	}
	histo.actions.push([ActEnters, monster_id, combat, life]);
	histo.monster_infos[monster_id] = new Object();
	histo.monster_infos[monster_id].infos = new Object();
	if (posX == BLUE_SIDE) {
		histo.monster_infos[monster_id].team = BLUE;
		histo.monster_infos[monster_id].name = '<span style="color:blue;">' + name + '</span>';
		histo.monster_infos[monster_id].infos.X = BLUE_SIDE;
	}
	else {
		histo.monster_infos[monster_id].team = RED;
		histo.monster_infos[monster_id].name = '<span style="color:red;">' + name + '</span>';
		histo.monster_infos[monster_id].infos.X = RED_SIDE;
	}
	histo.monster_infos[monster_id].name_alert = name;
	histo.monster_infos[monster_id].infos.useful = false;
	histo.monster_infos[monster_id].infos.PA = INITPA;
	histo.monster_infos[monster_id].infos.PV = life;
	histo.monster_infos[monster_id].infos.enter_time = histo.time;
	histo.monster_infos[monster_id].infos.KO_time = 1000;
	histo.monster_infos[monster_id].infos.move = 0;
	histo.monster_infos[monster_id].infos.goal = 0;
	histo.monster_infos[monster_id].infos.bad_goal = 0;
	histo.monster_infos[monster_id].infos.stop = 0;
	histo.monster_infos[monster_id].infos.box = 0;
	histo.monster_infos[monster_id].infos.pass = 0;
	histo.monster_infos[monster_id].infos.getball = 0;
	histo.monster_infos[monster_id].infos.KO = 0;
	histo.monster_infos[monster_id].infos.PA_kill = 0;
	histo.monster_infos[monster_id].infos.fight_att = 0;
	histo.monster_infos[monster_id].infos.PV_kill_att = 0;
	histo.monster_infos[monster_id].infos.PV_kill_att_min = PV_KILL_INIT_MIN;
	histo.monster_infos[monster_id].infos.PV_kill_att_max = PV_KILL_INIT_MAX;
	histo.monster_infos[monster_id].infos.fight_def = 0;
	histo.monster_infos[monster_id].infos.PV_kill_def = 0;
	histo.monster_infos[monster_id].infos.PV_kill_def_min = PV_KILL_INIT_MIN;
	histo.monster_infos[monster_id].infos.PV_kill_def_max = PV_KILL_INIT_MAX;
	histo.monster_infos[monster_id].infos.PA_lost_goal = 0;
	histo.monster_infos[monster_id].infos.PA_lost_full = 0;
	histo.monster_infos[monster_id].infos.new_tip = true;
	histo.nb_monsters++;
	histo.monsterids.push(monster_id);
	print_event('<img src="/gfx/forum/ugliness.gif"> Entrée de ' + histo.monster_infos[monster_id].name + ' (<img src="/gfx/icons/fight.gif"> ' + combat + ' / <img src="/gfx/icons/endurance.gif"> ' + life +')', flag, true);
}
function parse_histo (flag) {
	/**
	 * Actions gérées
	 * 'ActBoxAppear' => ActBoxAppear
	 * 'ActResetMonster' => ActResetMonster
	 * 'ActMove' => ActMove
	 * 'ActDistributeActions2' => ActDistributeActions
	 * 'ActEnters' => ActEnters
	 * 'ActBoxOpened' => ActBoxOpened
	 * 'ActFightBall' => ActFightBall
	 * 'ActGetBall' => ActGetBall
	 * 'ActPass' => ActPass
	 * 'ActGoal' => ActGoal
	 * 'ActBadGoal' => ActBadGoal
	 * 'ActFight' => ActFight
	 * 'ActProjected' => ActProjected
	 * 'ActDropBall' => ActDropBall
	 * 'ActMonsterDie' => ActMonsterDie
	 * 'ActEnd' => ActEnd
	**/
	histo.read = 0;
	var i;
	var dummy = 0;
	var monster_id = 0;
	var monster_id2 = 0;
	var direction = '';
	var temp1 = 0;
	var temp2 = 0;
	var type = '';
	var title = '';
	var action = '';
	var nb_current_boxes = 0;
	var nb_current_monsters_reinit = 0;
	var reading = true;
	var move = new Object();
	move.nb = 0;
	move.save_id = 0;
	move.save_direction = '';
	// re-init des "new tip" pour ne pas forcer un affichage inutile
	for ( i = 0 ; i < histo.monsterids.length ; i++ ) histo.monster_infos[histo.monsterids[i]].infos.new_tip = false;
	while (reading) {
		type = histo.data[histo.read++];
		switch (type) {
			case 'w': case 'l': case 'o': case 'g': case 'n': case 'z': case 't': case 'f': break;
			case 'h': if (histo.last_read + histo.read >= histo.size) reading = false; break;
			case ':': case 'i': dummy = H_get_int(); break;
			default:
				if (type == 'y') title = H_get_string();
				else title = histo.references[H_get_int()]; // type == 'R'
				switch (title) {
					case 'mbl.Action':
						histo.cpt_actions++;
						type = histo.data[histo.read++];
						if (type == 'y') action = H_get_string();
						else action = histo.references[H_get_int()]; // type == 'R'
						if (move.nb > 0 && action != 'ActMove') {
							H_print_move(move.nb, move.save_id, move.save_direction, flag);
							move.nb = 0;
						}
						switch (action) {
							case 'ActBoxAppear':
								histo.read++;
								dummy = H_get_int(); // 2 champs
								temp1 = H_get_integer_field(); // x
								temp2 = H_get_integer_field(); // y
								histo.actions.push([ActBoxAppear, temp1, temp2]);
								nb_current_boxes++;
								if (nb_current_boxes == 3) {
									nb_current_boxes = 0;
									print_event('<img src="/gfx/forum/greediness.gif"> Empaquetage des mioches et mise en place des boîtes', flag, true);
								}
								break;
							case 'ActResetMonster':
								histo.read++;
								dummy = H_get_int(); // 3 champs
								monster_id = H_get_integer_field(); // monstre
								temp1 = H_get_integer_field(); // x
								temp2 = H_get_integer_field(); // y
								histo.actions.push([ActResetMonster, monster_id, temp1, temp2]);
								nb_current_monsters_reinit++;
								if (nb_current_monsters_reinit == histo.nb_monsters) {
									nb_current_monsters_reinit = 0;
									print_event('<img src="/gfx/forum/ugliness.gif"> Retour des monstres dans leurs camps respectifs', flag, true);
								}
								if (!histo.monster_infos[monster_id].infos.useful) {
									histo.monster_infos[monster_id].infos.PA_lost_goal = Math.min(distance(histo.monster_infos[monster_id].infos.X, 0, temp1, 0), (FULLPA - histo.monster_infos[monster_id].infos.PA));
									histo.monster_infos[monster_id].infos.new_tip = true;
								}
								histo.monster_infos[monster_id].infos.X = temp1;
								histo.monster_infos[monster_id].infos.useful = false;
								break;
							case 'ActMove':
								histo.read++;
								dummy = H_get_int(); // 2 champs
								monster_id = H_get_integer_field(); //montsre
								direction = H_get_direction(); // direction
								histo.actions.push([ActMove, monster_id, direction]);
								if (move.nb > 0 && (move.save_id != monster_id || move.save_direction != direction)) {
									H_print_move(move.nb, move.save_id, move.save_direction, flag);
									move.nb = 0;
								}
								if (move.nb == 0) {
									move.save_id = monster_id;
									move.save_direction = direction;
								}
								move.nb++;
								break;
							case 'ActDistributeActions2':
								histo.read++;
								histo.time++;
								dummy = H_get_int(); // 1 champ
								temp1 = H_get_integer_field(); // nb PA
								histo.actions.push([ActDistributeActions, temp1]);
								print_event('<img src="/gfx/forum/reputation.gif"> Ajout de ' + temp1 + ' PA', flag, true);
								for ( i = 0 ; i < histo.monsterids.length ; i++ ) {
									monster_id = histo.monsterids[i];
									histo.monster_infos[monster_id].infos.PA += temp1;
									if (histo.monster_infos[monster_id].infos.PA > FULLPA) {
										histo.monster_infos[monster_id].infos.PA_lost_full += (histo.monster_infos[monster_id].infos.PA - FULLPA);
										histo.monster_infos[monster_id].infos.PA = FULLPA;
										histo.monster_infos[monster_id].infos.new_tip = true;
									}
								}
								break;
							case 'ActEnters':
								histo.read++;
								dummy = H_get_int(); // 1 champ
								H_parse_new_monster(flag); // détails du monstre
								break;
							case 'ActBoxOpened':
								histo.read++;
								dummy = H_get_int(); // 4 champs
								monster_id = H_get_integer_field(); // monstre
								dummy = H_get_integer_field(); // x
								temp1 = H_get_integer_field(); // y
								type = histo.data[histo.read]; // 'n' si pas de mioche, objet du mioche sinon
								histo.actions.push([ActBoxOpened, monster_id, dummy, temp1, type != 'n']);
								switch (temp1) {
									case 4: temp2 = 'haut'; break;
									case 8: temp2 = 'milieu'; break;
									case 12: temp2 = 'bas'; break;
									default: temp2 = '## erreur de lecture ##';
								}
								if (type == 'n') print_event('<img src="/gfx/forum/noburp.gif"> ' + histo.monster_infos[monster_id].name + ' ouvre la boîte du ' + temp2 + ' : elle est vide', flag, true);
								else print_event('<img src="/gfx/forum/greediness.gif"> ' + histo.monster_infos[monster_id].name + ' ouvre la boîte du ' + temp2 + ' : un mioche se cachait dedans !', flag, true);
								histo.monster_infos[monster_id].infos.useful = true;
								histo.monster_infos[monster_id].infos.box++;
								histo.monster_infos[monster_id].infos.PA--;
								histo.monster_infos[monster_id].infos.new_tip = true;
								break;
							case 'ActFightBall':
								histo.read++;
								dummy = H_get_int(); // 3 champs
								monster_id = H_get_integer_field(); // monstre
								dummy = H_get_integer_field(); // id du mioche
								temp1 = histo.data[histo.read++]; // booléen pour le mioche chopé (t/f)
								histo.actions.push([ActFightBall, monster_id, dummy, temp1 == 't']);
								if (temp1 == 'f') print_event('<img src="/gfx/forum/noburp.gif"> ' + histo.monster_infos[monster_id].name + ' rate son contrat, le mioche refuse tout contact', flag, true);
								else print_event('<img src="/gfx/forum/greediness.gif"> ' + histo.monster_infos[monster_id].name + ' réussit son contrat, dorénavant, le mioche sera docile', flag, true);
								histo.monster_infos[monster_id].infos.useful = true;
								histo.monster_infos[monster_id].infos.PA--;
								break;
							case 'ActGetBall':
								histo.read++;
								dummy = H_get_int(); // 2 champs
								monster_id = H_get_integer_field(); // monstre
								dummy = H_get_integer_field(); // id du mioche
								histo.actions.push([ActGetBall, monster_id, dummy]);
								print_event('<img src="/gfx/forum/greediness.gif"> ' + histo.monster_infos[monster_id].name + ' attrape un mioche', flag, true);
								histo.monster_infos[monster_id].infos.useful = true;
								histo.monster_infos[monster_id].infos.getball++;
								histo.monster_infos[monster_id].infos.new_tip = true;
								break;
							case 'ActPass':
								histo.read++;
								dummy = H_get_int(); // 3 champs
								monster_id = H_get_integer_field(); // monstre
								direction = H_get_direction(); // direction
								temp1 = H_get_integer_field(); // distance
								histo.actions.push([ActPass, monster_id, direction, temp1]);
								switch (direction) {
									case 'Right': direction = ' vers la droite'; break;
									case 'Left': direction = ' vers la gauche'; break;
									case 'Up': direction = ' vers le haut'; break;
									case 'Down': direction = ' vers le bas'; break;
									default: direction = '## erreur de lecture ##';
								}
								print_event('<img src="/gfx/forum/greediness.gif"> ' + histo.monster_infos[monster_id].name + ' fait une passe de distance ' + temp1 + direction, flag, true);
								histo.monster_infos[monster_id].infos.useful = true;
								histo.monster_infos[monster_id].infos.pass++;
								histo.monster_infos[monster_id].infos.PA--;
								histo.monster_infos[monster_id].infos.new_tip = true;
								break;
							case 'ActGoal':
								histo.read++;
								dummy = H_get_int(); // 1 champ
								monster_id = H_get_integer_field(); // monstre
								histo.actions.push([ActGoal, monster_id]);
								print_event('<img src="/gfx/forum/icon_exclaim.gif" width="13px"> ' + histo.monster_infos[monster_id].name + ' marque un <strong>BUT</strong> !', flag, true);
								histo.monster_infos[monster_id].infos.goal++;
								histo.monster_infos[monster_id].infos.new_tip = true;
								break;
							case 'ActBadGoal':
								histo.read++;
								dummy = H_get_int(); // 1 champ
								monster_id = H_get_integer_field(); // monstre
								histo.actions.push([ActBadGoal, monster_id]);
								print_event('<img src="/gfx/forum/icon_question.gif" width="13px"> ' + histo.monster_infos[monster_id].name + ' marque contre son camp...', flag, true);
								histo.monster_infos[monster_id].infos.bad_goal++;
								histo.monster_infos[monster_id].infos.new_tip = true;
								break;
							case 'ActFight':
								histo.read++;
								dummy = H_get_int(); // 6 champs
								monster_id = H_get_integer_field(); // attaquant
								monster_id2 = H_get_integer_field(); // victime
								dummy = H_get_integer_field(); // id attaquant (encore)
								temp1 = H_get_integer_field(); // PV perdus par l'attaquant
								temp2 = H_get_integer_field(); // PV perdus par la victime
								dummy = histo.data[histo.read++]; // booléen pour savoir si l'attaquant repousse la victime
								histo.actions.push([ActFight, monster_id, monster_id2, temp1, temp2]);
								print_event('<img src="/gfx/forum/fight.gif"> ' + histo.monster_infos[monster_id2].name + ' (-' + temp2 + ' <img src="/gfx/forum/endurance.gif">) est attaqué par ' + histo.monster_infos[monster_id].name + ' (-' + temp1 + ' <img src="/gfx/forum/endurance.gif">)', flag, true);
								histo.monster_infos[monster_id].infos.useful = true;
								// stats attaque
								histo.monster_infos[monster_id].infos.new_tip = true;
								histo.monster_infos[monster_id].infos.fight_att++;
								histo.monster_infos[monster_id].infos.PV_kill_att += temp2;
								if (temp2 > histo.monster_infos[monster_id].infos.PV_kill_att_max) histo.monster_infos[monster_id].infos.PV_kill_att_max = temp2;
								if (temp2 < histo.monster_infos[monster_id].infos.PV_kill_att_min) histo.monster_infos[monster_id].infos.PV_kill_att_min = temp2;
								histo.monster_infos[monster_id2].infos.PV -= temp2;
								// stats défense
								if (temp1 > 0) {
									histo.monster_infos[monster_id2].infos.new_tip = true;
									histo.monster_infos[monster_id2].infos.fight_def++;
									histo.monster_infos[monster_id2].infos.PV_kill_def += temp1;
									if (temp1 > histo.monster_infos[monster_id2].infos.PV_kill_def_max) histo.monster_infos[monster_id2].infos.PV_kill_def_max = temp1;
									if (temp1 < histo.monster_infos[monster_id2].infos.PV_kill_def_min) histo.monster_infos[monster_id2].infos.PV_kill_def_min = temp1;
									histo.monster_infos[monster_id].infos.PV -= temp1;
								}
								// autres
								if (temp1 > temp2) histo.monster_infos[monster_id].infos.PA--;
								if (histo.monster_infos[monster_id2].infos.PV <= 0) {
									histo.monster_infos[monster_id].infos.KO++;
									histo.monster_infos[monster_id].infos.PA_kill += histo.monster_infos[monster_id2].infos.PA;
								}
								break;
							case 'ActProjected':
								histo.read++;
								dummy = H_get_int(); // 2 champs
								monster_id = H_get_integer_field(); // monstre
								direction = H_get_direction(); // direction
								histo.actions.push([ActProjected, monster_id, direction]);
								switch (direction) {
									case 'Right': direction = ' vers la droite';
										histo.monster_infos[monster_id].infos.X++;
										break;
									case 'Left': direction = ' vers la gauche';
										histo.monster_infos[monster_id].infos.X--;
										break;
									case 'Up': direction = ' vers le haut';
										break;
									case 'Down': direction = ' vers le bas';
										break;
									default: direction = '## erreur de lecture ##';
								}
								print_event('<img src="/gfx/forum/power.gif"> ' + histo.monster_infos[monster_id].name + ' est repoussé' + direction, flag, true);
								break;
							case 'ActDropBall':
								histo.read++;
								dummy = H_get_int(); // 1 champ
								monster_id = H_get_integer_field(); // monstre
								histo.actions.push([ActDropBall, monster_id]);
								print_event('<img src="/gfx/forum/noburp.gif"> ' + histo.monster_infos[monster_id].name + ' perd son mioche', flag, true);
								break;
							case 'ActMonsterDie':
								histo.read++;
								dummy = H_get_int(); // 1 champ
								monster_id = H_get_integer_field(); // monstre
								histo.actions.push([ActMonsterDie, monster_id]);
								print_event('<img src="/gfx/forum/icon_cross.gif" width="13px"> ' + histo.monster_infos[monster_id].name + ' est <strong>KO</strong> !', flag, true);
								histo.nb_monsters--;
								histo.monster_infos[monster_id].infos.KO_time = histo.time;
								for ( i = 0 ; i < histo.monsterids.length ; i++ ) {
									if (histo.monsterids[i] == monster_id) {
										histo.monsterids.splice(i, 1);
										histo.monster_infos[monster_id].infos.new_tip = true;
									}
								}
								break;
							case 'ActEnd':
								histo.read++;
								dummy = H_get_int(); // 0 champ
								histo.actions.push([ActEnd]);
								print_event('<img src="/gfx/forum/time.gif" width="13px"> Match terminé', true, false);
								reading = false;
								break;
							default: print_event('## erreur de lecture (action) ## : ' + type + title + action, flag, true);
						}
						break;
					default:
						type = histo.data[histo.read];
						if (type == 'y') {
							histo.read++;
							H_skip_string();
						}
						else {
							if (type == 'R') {
								histo.read++;
								dummy = H_get_int();
							}
							else dummy = H_get_integer_field();
						}
				}
		}
	}
	if (move.nb > 0) {
		histo.cpt_actions++; // car le nombre d'actions sera décrémenté dans la fonction d'affichage
		H_print_move(move.nb, move.save_id, move.save_direction, flag);
		histo.cpt_actions--; // retour à un nombre normal pour les prochaines lectures
	}
	histo.last_read = histo.size - 1;
	histo.last_action = histo.cpt_actions;

	// mise à jour des tips d'infos
	for ( var color = BLUE ; color <= RED ; color++ ) {
		for ( i = 0 ; i < syndicate[color].player_size ; i++ ) {
			if (histo.monster_infos[syndicate[color].player[i].mid].infos.new_tip) {
				syndicate[color].player[i].tip = true;
				remove_tip(syndicate[color].player[i].name_zone);
				add_tip(syndicate[color].player[i].name_zone, TIP_TITLE + '<span style="color:' + syndicate[color].color_text + '">' + syndicate[color].player[i].name + '</span>', build_tip(syndicate[color].player[i].mid, syndicate[color].player[i].combat));
			}
		}
		for ( i = 0 ; i < syndicate[color].KO_size ; i++ ) {
			if (histo.monster_infos[syndicate[color].KO[i].mid].infos.new_tip) {
				syndicate[color].KO[i].tip = true;
				remove_tip(syndicate[color].KO[i].name_zone);
				add_tip(syndicate[color].KO[i].name_zone, TIP_TITLE + '<span style="color:' + syndicate[color].color_text + '">' + syndicate[color].KO[i].name + '</span>', build_tip(syndicate[color].KO[i].mid, syndicate[color].KO[i].combat));
			}
		}
	}
}

// résumer les actions trouvées et alerter en fonction de cela
function summarize_and_alert (start_action) {
	/**
	 * Actions gérées
	 * ActDistributeActions
	 * ActEnters
	 * ActMonsterDie
	 * ActMove
	 * ActBoxOpened
	 * ActPass
	 * ActGetBall
	 * ActGoal
	 * ActBadGoal
	 * ActFight
	**/
	var add_PA = 0;
	var actors = new Array();
	var acting = new Object();
	var monster_id;
	var monster_id2;
	var action;
	var move = 0;
	var pass = 0;
	var get_ball = 0;
	var goal = 0;
	var fight = 0;
	var i;
	for ( i=start_action ; i<histo.cpt_actions ; i++ ) {
		action = histo.actions[i][0];
		if (action == ActDistributeActions) {
			add_PA += histo.actions[i][1];
		}
		else {
			if (action != ActBoxAppear && action != ActResetMonster && action != ActFightBall && action != ActProjected && action != ActDropBall && action != ActEnd) {
				monster_id = histo.actions[i][1];
				if (actors.indexOf(monster_id) == NO) {
					actors.push(monster_id);
					acting[monster_id] = new Object();
					acting[monster_id].enter = false;
					acting[monster_id].die = false;
					acting[monster_id].move = 0;
					acting[monster_id].open = 0;
					acting[monster_id].pass = 0;
					acting[monster_id].get_ball = 0;
					acting[monster_id].goal = 0;
					acting[monster_id].bad_goal = 0;
					acting[monster_id].fight = 0;
					acting[monster_id].victims = new Array();
				}
				switch (action) {
					case ActEnters:
						move++;
						acting[monster_id].enter = true;
						break;
					case ActMonsterDie:
						fight++;
						acting[monster_id].die = true;
						break;
					case ActMove:
						move++;
						acting[monster_id].move++;
						break;
					case ActBoxOpened:
						move++;
						acting[monster_id].open++;
						break;
					case ActPass:
						pass++;
						acting[monster_id].pass++;
						break;
					case ActGetBall:
						get_ball++;
						acting[monster_id].get_ball++;
						break;
					case ActGoal:
						goal++;
						acting[monster_id].goal++;
						break;
					case ActBadGoal:
						goal++;
						acting[monster_id].bad_goal++;
						break;
					case ActFight:
						fight++;
						acting[monster_id].fight++;
						monster_id2 = histo.actions[i][2];
						if (acting[monster_id].victims.indexOf(monster_id2) == NO) acting[monster_id].victims.push(monster_id2);
						break;
					default:;
				}
			}
		}
	}
	if (add_PA > 0) {
		refresh_match();
		if (move + pass + get_ball + goal + fight == 0) alert_PA(add_PA);
	}
	else {
		var text = '';
		if (add_PA > 0) text = 'Ajout de ' + add_PA + ' PA. '
		var opponent = false;
		var friend = false;
		var type = NO;
		var temp;
		var name;
		for ( i=0 ; i<actors.length ; i++ ) {
			monster_id = actors[i];
			if (!acting[monster_id].die) {
				if (my_team == histo.monster_infos[monster_id].team) friend = true;
				else opponent = true;
			}
			name = histo.monster_infos[monster_id].name_alert;
			if (acting[monster_id].enter) text = text + name + ' entre. ';
			temp = acting[monster_id].move;
			if (temp > 0) text = text + name + ' bouge de ' + temp + ' PA. ';
			temp = acting[monster_id].open;
			if (temp > 0) {
				if (temp == 1) text = text + name + ' ouvre 1 boîte. ';
				else text = text + name + ' ouvre ' + temp + ' boîtes. ';
			}
			temp = acting[monster_id].pass;
			if (temp > 0) {
				if (temp == 1) text = text + name + ' fait 1 passe. ';
				else text = text + name + ' fait ' + temp + ' passes. ';
			}
			temp = acting[monster_id].get_ball;
			if (temp > 0) {
				if (temp == 1) text = text + name + ' attrape 1 mioche. ';
				else text = text + name + ' attrape ' + temp + ' mioches. ';
			}
			temp = acting[monster_id].goal;
			if (temp > 0) {
				if (temp == 1) text = text + name + ' marque 1 but. ';
				else text = text + name + ' marque ' + acting[monster_id].goal + ' buts. ';
			}
			temp = acting[monster_id].bad_goal;
			if (temp > 0) {
				if (temp == 1) text = text + name + ' marque 1 but contre son camp. ';
				else text = text + name + ' marque ' + acting[monster_id].goal + ' buts contre son camp. ';
			}
			if (acting[monster_id].fight > 0) {
				text = text + name + ' attaque : ' + histo.monster_infos[acting[monster_id].victims[0]].name_alert;
				var j;
				for ( j=1 ; j<acting[monster_id].victims.length ; j++ )
					text = text + ', ' + histo.monster_infos[acting[monster_id].victims[j]].name_alert;
				text = text + '. ';
			}
			if (acting[monster_id].die) text = text + name + ' est KO. ';
		}
		if (my_team != NO && opponent != friend) {
			if (friend) type = FRIEND;
			else type = OPPONENT;
		}
		if (goal == 0 && fight > 0) alert_fight(type, text);
		else if (goal == 0 && (pass > 0 || get_ball > 0)) alert_pass(type, text);
		else alert_move(type, text);
	}
}

// Simple lecture de l'historique du match
function read_histo() {
	read_histo_button.style.visibility = 'hidden';
	print_event('<img src="/gfx/forum/icon_chart.gif" width="13px"> Lecture de l\'historique', true, false);

	wait_for_histo(function() {
	getDOC(GM_info.script.namespace + '/mbl/' + match_ID + '/watcher.xml?h=0&x=wy11:mbl.Commandy7:Refresh:0', function (histo_page) {
		if (!detect_fuseau(histo_page)) {
			var save_old_size = histo.size;
			histo.size = histo_page.documentElement.textContent.length;
			var diff = histo.size - save_old_size;

			if (diff > 0) {
				histo.data = histo_page.documentElement.textContent.substring(histo.last_read)
				parse_histo(false);
			}
		}
		else {
			print_event('<img src="/gfx/forum/icon_chart.gif" width="13px"> Fuseau : pas de lecture !', true, false);
		}
		print_event('<img src="/gfx/forum/icon_chart.gif" width="13px"> Fin de lecture', true, false);
		read_histo_button.style.visibility = 'visible';
		histo.lock = false;
	});});
}

// vérification de l'historique du match
function check_histo(flag) {
	verify_params();
	anticiper_fuseau();
	wait_for_histo(function() {
	getDOC(GM_info.script.namespace + '/mbl/' + match_ID + '/watcher.xml?h=0&x=wy11:mbl.Commandy7:Refresh:0', function (histo_page) {
		if (!detect_fuseau(histo_page)) {
			check_game_sound();
			var save_old_size = histo.size;
			histo.size = histo_page.documentElement.textContent.length;
			var diff = histo.size - save_old_size;
			var save_last_action = histo.last_action;
			if (diff > 0) {
				histo.data = histo_page.documentElement.textContent.substring(histo.last_read)
				parse_histo(flag);
				//~ if (checkbox.suivi.checked) get_match_data();
			}
			if (histo.cpt_check == 0) {
				if (histo.refresh == 1000) var text = ' ';
				else var text = ' ' + (histo.refresh/1000) + ' ';
				print_event('<img src="/gfx/forum/control.gif"> Surveillance du <strong>MATCH ON</strong> (toutes les' + text + 'secondes)', true, false);
			}
			else if (diff > 0) summarize_and_alert(save_last_action);
		}
		histo.cpt_check++;
		if (hack_for_fast_check) timer_match_alert = setTimeout(function(){check_histo(true)}, histo.refresh);
		histo.lock = false;
	});});
}

function get_status_spy (color, index) {
	id = syndicate[color].player[index].id;
	getDOC(GM_info.script.namespace + '/user/' + id, function (guy) {
	if (detect_fuseau(guy)) return;
	var status = guy.getElementsByClassName('aBar')[0].getElementsByTagName('span');
	var status_read = parse_status(status);
	var status_known = syndicate[color].player[index].status;
	if (status_known != '' && status_known != status_read) {
		if (status_read == ONLINE) {
			print_event('<img src="/gfx/icons/online.gif"> <strong>Connexion de ' + colored(color, syndicate[color].player[index].name) + '</strong>', true, false);
			alert_connexion(syndicate[color].player[index].name);
		}
		else {
			print_event('<img src="/gfx/icons/offline.gif"> <strong>Déconnexion de ' + colored(color, syndicate[color].player[index].name) + '</strong>', true, false);
			alert_deconnexion(syndicate[color].player[index].name);
		}
	}
	syndicate[color].player[index].status = status_read;
	syndicate[color].player[index].status_zone.innerHTML = syndicate[color].player[index].status;
	syndicate[color].player[index].cpt_spy++;
});}

function new_one_spy(color, index) {
	print_event('<img src="/gfx/forum/control.gif"> Surveillance de ' + colored(color, syndicate[color].player[index].name) + ' <strong>ON</strong>', true, false);
}

function stop_one_spy(color, index) {
	syndicate[color].player[index].cpt_spy = 0;
	print_event('<img src="/gfx/forum/control.gif"> Surveillance de ' + colored(color, syndicate[color].player[index].name) + ' <strong>OFF</strong>', true, false);
}

function spy(color) {
	anticiper_fuseau();
	var nb_no_spy = 0;
	check_spy_sound();

	for (var i = 0 ; i < syndicate[color].player_size ; i++ ) {
		if (syndicate[color].player[i].checkbox.checked) {
			if (syndicate[color].player[i].cpt_spy == 0) new_one_spy(color, i);
			get_status_spy(color, i);
		}
		else {
			nb_no_spy++;
			if (syndicate[color].player[i].cpt_spy > 0) stop_one_spy(color, i);
		}
	}
	if (nb_no_spy == syndicate[color].player_size) stop_spy(color);
}

function active_spy(color) {
	verify_params();
	check_spy_sound();
	check_notify();
	nb_timers++;

	syndicate[color].spy.style.display = 'none';
	syndicate[color].stop_spy.style.display = 'inline';

	alert_spy_on(color);
	spy(color);
	syndicate[color].timer_spy = setInterval(function(){ spy(color); }, FREQUENCE_SPY);
}

function stop_spy (color) {
	syndicate[color].stop_spy.style.display = 'none';
	syndicate[color].spy.style.display = 'inline';
	clearInterval(syndicate[color].timer_spy);
	for (var i = 0 ; i < syndicate[color].player_size ; i++) {
		if (syndicate[color].player[i].cpt_spy > 0) stop_one_spy(color, i);
	}
	alert_spy_off(color);
	nb_timers--;
	verify_timer_fuseau();
}

function fill_spy(color, index) {
	syndicate[color].player[index].name_spy_zone.innerHTML = colored(color, syndicate[color].player[index].name);
	if (syndicate[color].player[index].cpt_spy > 0)
		syndicate[color].player[index].checkbox = add_input_checkbox(syndicate[color].player[index].check_zone, 'player_' + color + index, true);
	else
		syndicate[color].player[index].checkbox = add_input_checkbox(syndicate[color].player[index].check_zone, 'player_' + color + index, false);
}

function fill_spies(color) {
	for ( i = 0 ; i < syndicate[color].player_size ; i++ )
		fill_spy(color, i);
}

function add_spy(color, index) {
	id = 'tr_spy' + color + index;
	my_tr = add(syndicate[color].check_body, 'tr', id);
	odd = index % 2;
	if (odd) my_tr.setAttribute('class', 'false');
	else my_tr.setAttribute('class', 'true');
	// name
	syndicate[color].player[index].name_spy_zone = add(my_tr, 'td', 'player_name_spy_' + color + index);
	// check
	id = 'player_check_' + color + index;
	syndicate[color].player[index].check_zone = add(my_tr, 'td', id);
	syndicate[color].player[index].check_zone.setAttribute('style', 'width:100px;');
}

function change_display_check_match_begins() {
	read_histo_button.style.visibility = 'hidden';
	check_match_button.refresh.innerHTML = (histo.refresh/1000) + 's';
	check_match_button.span_active.style.display = 'none';
	check_match_button.span_stop.style.display = 'inline';
}
function change_display_check_match_ends() {
	read_histo_button.style.visibility = 'visible';
	check_match_button.span_active.style.display = 'inline';
	check_match_button.span_stop.style.display = 'none';
}
function active_check_histo(refresh) {
	verify_params();
	check_game_sound();
	check_notify();
	nb_timers++;

	my_team = get_my_team();

	histo.refresh = refresh;
	change_display_check_match_begins()
	histo.cpt_check = 0;
	alert_check_on(histo.refresh/1000);
	check_histo(false);
	if (histo.refresh == 1000) {
		hack_for_fast_check = true;
		timer_match_alert = setTimeout(function(){check_histo(true)}, histo.refresh);
	}
	else {
		hack_for_fast_check = false;
		timer_match_alert = setInterval(function(){check_histo(true)}, histo.refresh);
	}
}
function stop_check_histo() {
	change_display_check_match_ends();
histo.lock = false;
	if (hack_for_fast_check) {
		hack_for_fast_check = false;
		clearTimeout(timer_match_alert);
	}
	else clearInterval(timer_match_alert);
	print_event('<img src="/gfx/forum/control.gif"> Surveillance du <strong>MATCH OFF</strong>', true, false);
	alert_check_off();
	nb_timers--;
	verify_timer_fuseau();
}

function add_table_check(here, color) {
	var t = document.createElement('table');
	t.setAttribute('id', GM_info.script.name + '_table_check_' + color);
	t_head = add(t, 'thead', 'head_check_' + color);
	my_tr = add(t_head, 'tr', '');
	switch (color) {
	case BLUE:
		c = t.createCaption();
		t.setAttribute('style', 'width:100%;text-align:left;');
		add_text(c, 'span', '', '', syndicate[color].name + '&nbsp;');
		syndicate[color].spy = add_func_button(c, 'spy_' + BLUE, 'Surveiller', function() {active_spy(BLUE);}, 'Surveille les connexions des joueurs bleus (toutes les ' + (FREQUENCE_SPY/1000) + ' secondes)');
		syndicate[color].stop_spy = add_cancel_button(c, 'stop_spy_' + BLUE, 'Stopper', 'Arrête la surveillance des bleus');
		syndicate[color].stop_spy.addEventListener('click', function() {stop_spy(BLUE);}, false);
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'Agence';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'Surveillance';
		syndicate[color].check_body = add(t, 'tbody', 'body_check_' + color);
		syndicate[color].check_body.setAttribute('style', 'text-align:center;');
		for (i = 0 ; i < syndicate[color].player_size ; i++) add_spy(color, i);
		break;
	case RED:
		c = t.createCaption();
		t.setAttribute('style', 'width:100%;text-align:left;');
		add_text(c, 'span', '', '', syndicate[color].name + '&nbsp;');
		syndicate[color].spy = add_func_button(c, 'spy_' + RED, 'Surveiller', function() {active_spy(RED);}, 'Surveille les connexions des joueurs rouges (toutes les ' + (FREQUENCE_SPY/1000) + ' secondes)');
		syndicate[color].stop_spy = add_cancel_button(c, 'stop_spy_' + RED, 'Stopper', 'Arrête la surveillance des rouges');
		syndicate[color].stop_spy.addEventListener('click', function() {stop_spy(RED);}, false);
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'Agence';
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'Surveillance';
		syndicate[color].check_body = add(t, 'tbody', 'body_check_' + color);
		syndicate[color].check_body.setAttribute('style', 'text-align:center;');
		for (i = 0 ; i < syndicate[color].player_size ; i++) add_spy(color, i);
		break;
	case MATCH:
		// construction du tableau des options
		var t = document.createElement('table');
		t.setAttribute('id', GM_info.script.name + '_table_check_options');
		t_head = add(t, 'thead', 'head_check_options');
		my_tr = add(t_head, 'tr', '');
		t.setAttribute('style', 'width:100%;text-align:left;');
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'Options de surveillance';
		var check_histo_body = add(t, 'tbody', 'check_options');
		my_tr = add(check_histo_body, 'tr', '');
		my_tr.setAttribute('class', 'true');
		my_td = add(my_tr, 'td', '');
		checkbox.son = add_input_checkbox(my_td, 'checkbox.son', false);
		add_text(my_td, 'span', '', '', 'Alerte sonore');
		my_tr = add(check_histo_body, 'tr', '');
		my_tr.setAttribute('class', 'false');
		my_td = add(my_tr, 'td', '');
		checkbox.notify = add_input_checkbox(my_td, 'checkbox.notify', false);
		add_text(my_td, 'span', '', '', 'Alerte visuelle');
		my_tr = add(check_histo_body, 'tr', '');
		my_tr.setAttribute('class', 'true');
		my_td = add(my_tr, 'td', '');
		//~ checkbox.suivi = add_input_checkbox(my_td, 'checkbox.suivi', false);
		//~ add_text(my_td, 'span', '', '', 'Suivi des stats');
		// ajout du tableau des options
		here.appendChild(t);

		// construction du tableau des tests
		var t = document.createElement('table');
		t.setAttribute('id', GM_info.script.name + '_table_test_alert');
		t.setAttribute('style', 'width:100%;text-align:left;');
		t_head = add(t, 'thead', 'head_test_alert');
		my_tr = add(t_head, 'tr', '');
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'Test des alertes&nbsp;';
		add_func_button(my_th, 'test_alert', 'Tester', test_alert, 'Teste une alerte');
		test_alert_body = add(t, 'tbody', 'test_alert');
		my_tr = add(test_alert_body, 'tr', '');
		my_tr.setAttribute('class', 'true');
		my_td = add(my_tr, 'td', '');
		add_text(my_td, 'span', '', '', 'Mouvement :');
		radio_test_opponent_move = add_input_radio(my_td, 'test_alert', 'opponent_move', true);
		add_text(my_td, 'span', '', '', 'ennemi /');
		radio_test_friend_move = add_input_radio(my_td, 'test_alert', 'friend_move', false);
		add_text(my_td, 'span', '', '', 'ami');
		my_tr = add(test_alert_body, 'tr', '');
		my_tr.setAttribute('class', 'false');
		my_td = add(my_tr, 'td', '');
		add_text(my_td, 'span', '', '', 'Passe :');
		radio_test_opponent_passe = add_input_radio(my_td, 'test_alert', 'opponent_pass', false);
		add_text(my_td, 'span', '', '', 'ennemie /');
		radio_test_friend_passe = add_input_radio(my_td, 'test_alert', 'friend_pass', false);
		add_text(my_td, 'span', '', '', 'amie');
		my_tr = add(test_alert_body, 'tr', '');
		my_tr.setAttribute('class', 'true');
		my_td = add(my_tr, 'td', '');
		add_text(my_td, 'span', '', '', 'Attaque :');
		radio_test_opponent_fight = add_input_radio(my_td, 'test_alert', 'opponent_fight', false);
		add_text(my_td, 'span', '', '', 'ennemie /');
		radio_test_friend_fight = add_input_radio(my_td, 'test_alert', 'friend_fight', false);
		add_text(my_td, 'span', '', '', 'amie');
		my_tr = add(test_alert_body, 'tr', '');
		my_tr.setAttribute('class', 'false');
		my_td = add(my_tr, 'td', '');
		radio_test_PA = add_input_radio(my_td, 'test_alert', 'PA', false);
		add_text(my_td, 'span', '', '', 'ajout PA /');
		radio_test_connexion = add_input_radio(my_td, 'test_alert', 'connexion', false);
		add_text(my_td, 'span', '', '', 'connexion /');
		radio_test_deconnexion = add_input_radio(my_td, 'test_alert', 'deconnexion', false);
		add_text(my_td, 'span', '', '', 'déco.');
		my_tr = add(test_alert_body, 'tr', '');
		my_tr.setAttribute('style', 'display:none');
		my_tr.setAttribute('class', 'false');
		my_td = add(my_tr, 'td', '');
		add_text(my_td, 'span', 'preload_notify', '', '<img src="' + NOTIF_OPPONENT + '"><img src="' + NOTIF_FRIEND + '"><img src="' + NOTIF_FRIEND + '"><img src="' + NOTIF_PASS + '"><img src="' + NOTIF_FIGHT + '"><img src="' + NOTIF_PA + '"><img src="' + NOTIF_PA_SOON + '"><img src="' + NOTIF_CONNEXION + '"><img src="' + NOTIF_DECONNEXION + '"><img src="' + NOTIF_INFO + '">');
		break;
	default: // case HISTO
		t.setAttribute('style', 'width:100%;text-align:left;');
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'Heure';
		my_th.setAttribute('style', 'width:80px;text-align:center;');
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = '# Action';
		my_th.setAttribute('style', 'width:80px;text-align:center;');
		my_th = add(my_tr, 'th', '');
		my_th.innerHTML = 'Événement';
		event_histo_body = add(t, 'tbody', 'body_check_histo');
	}

	here.appendChild(t);
	get_params()
	return t;
}

function init() {
	// demande d'autorisation pour les notifications
	if (get_notification_permission() == "default") allow_notification();
	// lecture de l'autorisation
	if (get_notification_permission() == "granted") notifications_allowed = true;
	else notifications_allowed = false;

	// ajout des div principaux
	script_main_div = document.createElement('div');
	script_main_div.setAttribute('id', GM_info.script.name + '_main_div');
	script_main_div.setAttribute('style', 'text-align:center;display:block;');

	if (script_online) my_ID = get_my_ID();
	chosen.id = my_ID;
	chosen.newguy = true;

	// récupération des infos des équipes
	syndicate_infos = print_area.getElementsByTagName('a');
	var temp = new Array();
	temp[BLUE] = syndicate_infos[BLUE].getAttribute('href');
	temp[RED] = syndicate_infos[RED].getAttribute('href');
	team = new Object();
	syndicate[BLUE] = new Syndicate(temp[BLUE].substring(temp[BLUE].lastIndexOf("/") + 1), syndicate_infos[BLUE].innerHTML, PLAYER);
	syndicate[BLUE].color_text = 'blue';
	syndicate[RED] = new Syndicate(temp[RED].substring(temp[RED].lastIndexOf("/") + 1), syndicate_infos[RED].innerHTML, PLAYER);
	syndicate[RED].color_text = 'red';

	// statistiques
	var stats_title = add_title('stats', 'Aide de jeu et statistiques');
	updating_button = add_func_button(stats_title, 'update_stats', 'Mise à jour', update, 'Met à jour les statistiques');
	stats_div = add_main('stats');

	// zone d'aide du joueur
	chosen.help_div = add_text(stats_div, 'div', 'help_who', 'display:none;clear:both;overflow:auto;margin:8px 0px;', '<img src=\"/gfx/icons/fight.gif\"><img src=\"/gfx/icons/fight.gif\"> Aide de jeu pour ');
	chosen.name_div = add_text(chosen.help_div, 'span', 'help_who', 'font-weight:bold;', '');
	add_text(chosen.help_div, 'span', 'help_who_label1', '', ' <img src=\"/gfx/icons/fight.gif\"><img src=\"/gfx/icons/fight.gif\"><br><img src=\"/gfx/icons/lvl.gif\"> Déplacement maxi conseillé : ');
	chosen.max_move_div = add_text(chosen.help_div, 'span', 'max_move', 'font-weight:bold;', '');
	add_text(chosen.help_div, 'span', 'help_who_label2', '', ' PA <img src=\"/gfx/icons/lvl.gif\"><br><img src=\"/gfx/icons/lvl.gif\"> Full PA à : ');
	chosen.full_PA_div = add_text(chosen.help_div, 'span', 'full_PA', 'font-weight:bold;', '');
	add_text(chosen.help_div, 'span', 'help_who_label3', '', ' <img src=\"/gfx/icons/lvl.gif\"><br><img src=\"/gfx/icons/lvl.gif\"> KO possible à : ');
	chosen.min_KO_div = add_text(chosen.help_div, 'span', 'min_KO', 'font-weight:bold;', '');
	add_text(chosen.help_div, 'span', 'help_who_label3', '', ' par ');
	chosen.min_KO_guy_div = add_text(chosen.help_div, 'span', 'min_KO_guy', 'font-weight:bold;', '');
	add_text(chosen.help_div, 'span', 'help_who_label4', '', ' <img src=\"/gfx/icons/lvl.gif\">');

	// colonne bleue
	team_div = add_text(stats_div, 'div', 'team_' + BLUE, 'text-align:center;float:left;width:50%;', '');
	var div = add_text(team_div, 'div', 'team_header_' + BLUE, '', "<a href='" + GM_info.script.namespace + "/syndicate/" + syndicate[BLUE].id + "' target='_blank'>" + syndicate[BLUE].name + "</a>, but : ")
	syndicate[BLUE].goal_span = add_text(div, 'span', 'next_goal_' + BLUE, 'color:blue;', '');
	add_text(div, 'span', 'space', '', '&nbsp;');
	if (script_online) {
		syndicate[BLUE].status_button = add_func_button(div, 'check_players_' + BLUE, 'Joueurs', function() {fill_status(BLUE, PLAYER);}, '');
		add_func_button(div, 'check_syndicate_' + BLUE, 'Tous', function() {check_syndicate(BLUE);}, 'Vérifie la présence de toutes les agences de ' + syndicate[BLUE].name);
	}
	syndicate[BLUE].player_div = add(team_div, 'div', '' + BLUE + PLAYER + '');
	syndicate[BLUE].KO_div = add(team_div, 'div', '' + BLUE + KO + '');
	syndicate[BLUE].crew_div = add(team_div, 'div', '' + BLUE + CREW + '');

	// colonne rouge
	team_div = add_text(stats_div, 'div', 'team_' + RED, 'text-align:center;float:left;width:50%;', '');
	div = add_text(team_div, 'div', 'team_header_' + RED, '', "<a href='" + GM_info.script.namespace + "/syndicate/" + syndicate[RED].id + "' target='_blank'>" + syndicate[RED].name + "</a>, but : ")
	syndicate[RED].goal_span = add_text(div, 'span', 'next_goal_' + RED, 'color:red;', '');
	add_text(div, 'span', 'space', '', '&nbsp;');
	if (script_online) {
		syndicate[RED].status_button = add_func_button(div, 'check_players_' + RED, 'Joueurs', function() {fill_status(RED, PLAYER);}, '');
		add_func_button(div, 'check_syndicate_' + RED, 'Tous', function() {check_syndicate(RED);}, 'Vérifie la présence de toutes les agences de ' + syndicate[RED].name);
	}
	syndicate[RED].player_div = add(team_div, 'div', '' + RED + PLAYER + '');
	syndicate[RED].KO_div = add(team_div, 'div', '' + RED + KO + '');
	syndicate[RED].crew_div = add(team_div, 'div', '' + RED + CREW + '');
	add_footer(stats_div);

	// surveillance
	var check_title = add_title('check', 'Surveillance');
	check_match_button.span_active = add_text(check_title, 'span', 'check_span_button', '', '');
	add_func_button(check_match_button.span_active, 'check_match_fast', 'MATCH | 2s', function () { active_check_histo(2000); }, 'Surveillance toutes les 2 secondes : rythme rapide, à n\'utiliser que si c\'est nécessaire ! (pensez aux pauvres serveurs de Croque Monster)');
	add_func_button(check_match_button.span_active, 'check_match_meidum', 'MATCH | 15s', function () { active_check_histo(15000); }, 'Surveillance toutes les 15 secondes : rythme moyen, en attente de quelque chose si vous ne pouvez rien faire');
	add_func_button(check_match_button.span_active, 'check_match_slow', 'MATCH | 60s', function () { active_check_histo(60000); }, 'Surveillance toutes les minutes : rythme lent, idéal pour laisser la page ouverte');
	check_match_button.span_stop = add_text(check_title, 'span', 'check_span_stop', '', '');
	check_match_button.stop = add_func_cancel_button(check_match_button.span_stop, 'check_match_stop', 'MATCH | Stopper', stop_check_histo, 'Arrêter la surveillance');
	check_match_button.stop.style.display = 'inline';
	add_text(check_match_button.span_stop, 'span', 'check_match_info', '', '&nbsp;Fréquence actuelle&nbsp;:&nbsp;');
	check_match_button.refresh = add_text(check_match_button.span_stop, 'span', 'check_match_refresh', '', '');
	check_match_button.span_stop.style.display = 'none';
	check_div = add_main('check');
	syndicate[BLUE].check_div = add_text(check_div, 'div', 'team_' + BLUE, 'text-align:center;float:left;width:34%;', '');
	div = add_text(check_div, 'div', 'check_histo', 'text-align:center;float:left;width:32%;', '');
	syndicate[RED].check_div = add_text(check_div, 'div', 'team_' + RED, 'text-align:center;float:left;width:34%;', '');
	add_table_check(div, MATCH);
	add_footer(check_div);
	change_visibility('check');

	// historique
	var histo_title = add_title('histo', 'Historique du jeu et de la surveillance');
	read_histo_button = add_func_button(histo_title, 'read_histo', 'Lire l\'historique', read_histo, 'Lire l\'historique sans surveiller le match');
	histo_div = add_main('histo');
	add_table_check(histo_div, HISTO);
	add_footer(histo_div);

	// aide
	add_title('help', 'Aide pour comprendre les statistiques (' + GM_info.script.name + ' v' + GM_info.script.version + ')');
	help_div = add_main('help');
	help_div.style.display = 'none';
	help_div.style.textAlign = 'left';
	help_div.innerHTML = "<br><img src=\"/gfx/icons/lvl.gif\"> <b>Bases à savoir et comprendre :</b><ul><li>Ce script donne des informations concernant chaque joueur mais surtout il donne des informations concernant un joueur en particulier. Par défaut, si vous avez un monstre sur le terrain de MBL, alors votre monstre est sélectionné par le script et vous donnera des informations vous concernant. À tout moment, en cliquant sur un nom d'agence, vous pouvez recalculer toutes ces informations spéciales pour cet autre joueur.</li><li>Ce script donne des informations de contacts directs, c'est-à-dire que les distances sont calculées d'un point à un autre au plus court. Si quelque chose rend la trajectoire impossible, ce script ne le détecte pas. Exemple : un joueur peut marquer mais le ballon est jalousement gardé par l'équipe adverse, alors le script donne simplement l'information que le but est possible même si il faudrait que les adversaires déposent le mioche devant le joueur pouvant marquer. <b>Utilisez donc ce script avec du recul ! L'idéal est de le combiner avec le magnéto Singe pour les actions à plusieurs joueurs.</b></li><li>Les boutons \"Joueurs\" et \"Tous\" permettent de connaître quels sont les joueurs connectés à CM.</li></ul><br><img src=\"/gfx/icons/lvl.gif\"> <b>Informations spéciales pour le joueur sélectionné :</b><ul><li>\"Aide de jeu pour\" : donne le nom du joueur sélectionné.</li><li>\"Déplacement maxi conseillé\" : donne le nombre de PA que vous pouvez dépenser sans craindre d'en dépenser pour rien car aucun but ne pourra être marqué assez tôt.</li><li>\"Full PA à\" : donne l'heure à laquelle le joueur sélectionné sera full PA.</li><li>\"KO possible à\" : donne l'heure à laquelle le joueur sélectionné peut être KO et par qui (seulement un nom même si plusieurs joueurs peuvent mettre KO le joueur sélectionné).</li></ul><br><img src=\"/gfx/icons/lvl.gif\"> <b>Informations communes :</b><ul><li>Agence : Cliquez sur le nom d'un joueur pour calculer ses informations personnelles et les distances de passe et distances d'attaque par rapport à ce joueur.</li><li><img src=\"/gfx/icons/online.gif\">/<img src=\"/gfx/icons/offline.gif\"> : Présence ou non sur CM.</li><li><img src=\"/gfx/icons/greediness.gif\"> : Joueur porteur d'un ballon.</li><li>PA : Nombre de Points d'Action du joueur.</li><li><img src=\"/gfx/icons/fight.gif\"> : Puissance de combat.</li><li><img src=\"/gfx/icons/endurance.gif\"> : Points de Vie (PV) restants, survolez ce nombre avec la souris pour voir les points de vie de départ (affiche seulement les points de vie de départ pour les agences KO).</li><li>but : nombre de PA manquants pour marquer (survolez pour savoir l'heure). \"Ok\" si un but est possible, avec un \"!\" si le joueur doit faire une ligne droite pour marquer. En gras : les joueurs qui peuvent marquer le plus tôt de leur équipe.</li><li>(xx) : Lorsqu'un petit nombre entre parenthèses apparaît, c'est le nombre de PA restants au joueur sélectionné après qu'il ait fait son action (attaque, défense, passe, réception ou but).</li></ul><img src=\"/gfx/icons/lvl.gif\"> <b>Informations liées au joueur sélectionné avec ses coéquipiers :</b><ul><li>passe : nombre de PA manquants pour que le joueur sélectionné puisse faire une passe à ce joueur. \"Ok\" si une passe est possible, avec un \"!\" si le joueur doit utiliser la passe de longueur 2.</li><li>réception : nombre de PA manquants pour que ce joueur puisse faire une passe au joueur sélectionné. \"Ok\" si une passe est possible, avec un \"!\" si le joueur doit utiliser la passe de longueur 2.</li><li>PA : En gras si le maximum de PA est atteint, pensez à bouger avant le prochain ajout de PA (à 15m et 45m de chaque heure entre 10h15m et 21h45m).</li></ul><img src=\"/gfx/icons/lvl.gif\"> <b>Informations liées au joueur sélectionné avec les adversaires :</b><ul><li>attaque : Si pas à distance de frappe, indique le nombre de PA nécessaire au joueur sélectionné pour atteindre sa cible (= le joueur de cette ligne). Si une attaque est possible, indique les PV restants aux joueurs, la couleur aide à savoir à quel joueur le nombre de PV fait référence. Si le KO est possible, cela est indiqué. En survolant avec la souris, vous aurez plus de détails : nombre de PA de frappe, heure du KO possible si ce n'est pas encore le cas.</li><li>défense : Idem que pour l'attaque mais si le joueur de cette ligne attaque le joueur sélectionné</li><li><b>Attention</b> : ces deux dernières statistiques sont basées sur une approximation car les combats comportent une dose de hasard.</li></ul>";

	// ajout du div principal sur la page
	print_area.insertBefore(script_main_div, document.getElementById("contentBottom"));
}

function update() {
	updating_button.style.visibility = 'hidden';
	compute_time();
	get_match_data();
}

// constructeurs utiles
function Chosen_Guy(id, owner, posX, posY, PA, combat, life) {
	this.id = id;
	this.newguy = false;
	this.owner = owner;
	this.posX = posX;
	this.posY = posY;
	this.PA = PA;
	this.combat = combat;
	this.att_max = combat;
	this.att_min = Math.floor(combat/2);
	this.att_nb = this.att_max - this.att_min + 1;
	this.def_max = Math.round(combat/1.2);
	this.def_min = Math.floor(combat/3);
	this.def_nb = this.def_max - this.def_min + 1;
	this.life = life;
	this.help_div = 0;
	this.name_div = 0;
	this.max_move_div = 0;
	this.full_PA_div = 0;
	this.min_KO_value = INVINCIBLE;
	this.min_KO_div = 0;
	this.min_KO_guy_div = 0;
	this.opponent = NO;
	this.opponent_color = "black";
	this.friend = NO;
	this.friend_color = "black";
}
function Player(id, mid, status, name, owner, PA, posX, posY, combat, life, lifemax) {
	this.id = id;
	this.mid = mid;
	this.status = status;
	this.status_zone = 0;
	this.name = name;
	this.name_zone = 0;
	this.owner = owner;
	this.owner_zone = 0;
	this.PA = PA;
	this.PA_zone = 0;
	this.posX = posX;
	this.posY = posY;
	this.combat = combat;
	this.combat_zone = 0;
	this.att_max = combat;
	this.att_min = Math.floor(combat/2);
	this.att_nb = this.att_max - this.att_min + 1;
	this.def_max = Math.round(combat/1.2);
	this.def_min = Math.floor(combat/3);
	this.def_nb = this.def_max - this.def_min + 1;
	this.life = life;
	this.lifemax = lifemax;
	this.life_zone = 0;
	this.but = 0;
	this.but_zone = 0;
	this.passe = 0
	this.passe_zone = 0;
	this.reception = 0
	this.reception_zone = 0;
	this.attaque = 0;
	this.attaque_tip = 0;
	this.attaque_zone = 0;
	this.defense = 0;
	this.defense_tip = 0;
	this.defense_zone = 0;
	this.name_spy_zone = 0;
	this.check_zone = 0;
	this.checkbox = 0;
	this.cpt_spy = 0;
	this.tip = false;
}
function Ko(id, mid, status, name, combat, lifemax) {
	this.id = id;
	this.mid = mid;
	this.status = status;
	this.status_zone = 0;
	this.name = name;
	this.name_zone = 0;
	this.combat = combat;
	this.combat_zone = 0;
	this.lifemax = lifemax;
	this.life_zone = 0;
	this.tip = false;
}
function Crew(id, status, name) {
	this.id = id;
	this.status = status;
	this.status_zone = 0;
	this.name = name;
	this.name_zone = 0;
}
function Syndicate(id, name, type) {
	this.id = id;
	this.color_text = '';
	this.name = name;
	this.type = type
	this.goal_span = 0;
	this.min_goal = 0;
	this.player_div = 0;
	this.player = new Array();
	this.player_size = 0;
	this.player_body = 0;
	this.status_button = 0;
	this.check_div = 0;
	this.check_body = 0;
	this.spy = 0;
	this.stop_spy = 0;
	this.timer_spy = 0;
	this.KO_div = 0;
	this.KO = new Array();
	this.KO_size = 0;
	this.KO_body = 0;
	this.crew_div = 0;
	this.crew = new Array();
	this.crew_read = false;
	this.crew_size = 0;
	this.crew_body = 0;
}
// tri du tableau des agences par numéro d'id croissant
function sortfunc(a,b) { return a.id - b.id; }

// test si une id d'agence est dans un tableau de player ou pas
function isplayer(color, id) {
	for ( i = 0 ; i < syndicate[color].player_size ; i++ ) {
		var temp_id = syndicate[color].player[i].id;
		if (temp_id == id) return i;
	}
	return NO;
}
// test si une id d'agence est dans un tableau KO ou pas
function isKO(color, id) {
	for ( i = 0 ; i < syndicate[color].KO_size ; i++ ) {
		var temp_id = syndicate[color].KO[i].id;
		if (temp_id == id) return i;
	}
	return NO;
}
// test si une id d'agence est dans le tableau des remplaçants d'une couleur
function iscrew(color, id) {
	for ( i = 0 ; i < syndicate[color].crew_size ; i++ ) {
		var temp_id = syndicate[color].crew[i].id;
		if (temp_id == id) return i;
	}
	return NO;
}
// copie du dernier joueur vers le joueur d'index i
function copy_last_player(i, color) {
	var last = syndicate[color].player_size-1;
	syndicate[color].player[i].id = syndicate[color].player[last].id;
	syndicate[color].player[i].status = syndicate[color].player[last].status;
	syndicate[color].player[i].name = syndicate[color].player[last].name;
	syndicate[color].player[i].owner = syndicate[color].player[last].owner;
	syndicate[color].player[i].PA = syndicate[color].player[last].PA;
	syndicate[color].player[i].posX = syndicate[color].player[last].posX;
	syndicate[color].player[i].posY = syndicate[color].player[last].posY;
	syndicate[color].player[i].combat = syndicate[color].player[last].combat;
	syndicate[color].player[i].life = syndicate[color].player[last].life;
	syndicate[color].player[i].lifemax = syndicate[color].player[last].lifemax;
	syndicate[color].player[i].but = syndicate[color].player[last].but;
	syndicate[color].player[i].passe = syndicate[color].player[last].passe;
	syndicate[color].player[i].reception = syndicate[color].player[last].reception;
	syndicate[color].player[i].attaque = syndicate[color].player[last].attaque;
	syndicate[color].player[i].defense = syndicate[color].player[last].defense;
}
// copie du dernier crew vers le crew d'index i
function copy_last_crew(i, color) {
	var last = syndicate[color].crew_size-1;
	syndicate[color].crew[i].id = syndicate[color].crew[last].id;
	syndicate[color].crew[i].status = syndicate[color].crew[last].status;
	syndicate[color].crew[i].name = syndicate[color].crew[last].name;
}

// fonctions d'alerte
function check_game_sound() {
	if (checkbox.son.checked && !sound_game_loaded) {
		load_game_sound();
		sound_game_loaded = true;
	}
}
function check_spy_sound() {
	if (checkbox.son.checked && !sound_spy_loaded) {
		load_spy_sound();
		sound_spy_loaded = true;
	}
}
function check_notify() {
	if (checkbox.notify.checked && get_notification_permission() == "default") allow_notification();
}
function alert_move (type, text) {
	switch (type) {
	case OPPONENT:
		if (checkbox.notify.checked) show_notification(NOTIF_OPPONENT, 'Mouvement ENNEMI', text);
		if (checkbox.son.checked) opponent_sound.play();
		break;
	case FRIEND:
		if (checkbox.notify.checked) show_notification(NOTIF_FRIEND, 'Mouvement AMI', text);
		if (checkbox.son.checked) friend_sound.play();
		break;
	default:
		if (checkbox.notify.checked) show_notification(NOTIF_INFO, 'Mouvement', text);
		if (checkbox.son.checked) opponent_sound.play();
	}
}
function alert_pass (type, text) {
	switch (type) {
	case OPPONENT:
		if (checkbox.notify.checked) show_notification(NOTIF_PASS, 'Mioche chez EUX', text);
		if (checkbox.son.checked) {
			opponent_sound.play();
			passe_sound.play();
		}
		break;
	case FRIEND:
		if (checkbox.notify.checked) show_notification(NOTIF_PASS, 'Mioche chez NOUS', text);
		if (checkbox.son.checked) {
			friend_sound.play();
			passe_sound.play();
		}
		break;
	default:
		if (checkbox.notify.checked) show_notification(NOTIF_PASS, 'Passe ou Récupération', text);
		if (checkbox.son.checked) {
			opponent_sound.play();
			passe_sound.play();
		}
	}
}
function alert_fight (type, text) {
	switch (type) {
	case OPPONENT:
		if (checkbox.notify.checked) show_notification(NOTIF_FIGHT, 'Combat ENNEMI', text);
		if (checkbox.son.checked) {
			opponent_sound.play();
			fight_sound.play();
		}
		break;
	case FRIEND:
		if (checkbox.notify.checked) show_notification(NOTIF_FIGHT, 'Combat AMI', text);
		if (checkbox.son.checked) {
			friend_sound.play();
			fight_sound.play();
		}
		break;
	default:
		if (checkbox.notify.checked) show_notification(NOTIF_FIGHT, 'Combat', text);
		if (checkbox.son.checked) {
			opponent_sound.play();
			fight_sound.play();
		}
	}
}
function alert_PA (x) {
	if (checkbox.notify.checked) show_notification(NOTIF_PA, 'Ajout de ' + x + ' PA', '');
	if (checkbox.son.checked) PA_sound.play();
}
function alert_connexion (text) {
	if (checkbox.notify.checked) show_notification(NOTIF_CONNEXION, 'Connexion', text);
	if (checkbox.son.checked) connexion_sound.play();
}
function alert_deconnexion (text) {
	if (checkbox.notify.checked) show_notification(NOTIF_DECONNEXION, 'Déconnexion', text);
	if (checkbox.son.checked) deconnexion_sound.play();
}
function alert_spy_on (color) {
	if (checkbox.notify.checked) show_notification(NOTIF_INFO, 'Surveillance ÉQUIPE', 'Activée pour ' + syndicate[color].name);
}
function alert_spy_off (color) {
	if (checkbox.notify.checked) show_notification(NOTIF_INFO, 'Surveillance ÉQUIPE', 'Désactivée pour ' + syndicate[color].name);
}
function alert_check_on (x) {
	if (checkbox.notify.checked)
		if (x == 1) show_notification(NOTIF_INFO, 'Surveillance MATCH', 'Activée toutes les secondes');
		else show_notification(NOTIF_INFO, 'Surveillance MATCH', 'Activée toutes les ' + x + ' secondes');
}
function alert_check_off () {
	if (checkbox.notify.checked) show_notification(NOTIF_INFO, 'Surveillance MATCH', 'Désactivée');
}
function test_alert () {
	verify_params();
	check_game_sound();
	check_spy_sound();
	check_notify();

	if (radio_test_opponent_move.checked) alert_move(OPPONENT, 'Ici, des détails');
	if (radio_test_friend_move.checked) alert_move(FRIEND, 'Ici, des détails');
	if (radio_test_opponent_passe.checked) alert_pass(OPPONENT, 'Ici, des détails');
	if (radio_test_friend_passe.checked) alert_pass(FRIEND, 'Ici, des détails');
	if (radio_test_opponent_fight.checked) alert_fight(OPPONENT, 'Ici, des détails');
	if (radio_test_friend_fight.checked) alert_fight(FRIEND, 'Ici, des détails');
	if (radio_test_PA.checked) alert_PA(2);
	if (radio_test_connexion.checked) alert_connexion('Nom du joueur');
	if (radio_test_deconnexion.checked) alert_deconnexion('Nom du joueur');
}

function load_spy_sound () {
	add_text(check_div, 'span', '', 'display:none', '<audio id="' + GM_info.script.name + '_connexion"><source src="' + SOUND_LOCATION + 'connexion.ogg" type="audio/ogg" preload="auto"></audio>');
	connexion_sound = document.getElementById(GM_info.script.name + '_connexion');
	add_text(check_div, 'span', '', 'display:none', '<audio id="' + GM_info.script.name + '_deconnexion"><source src="' + SOUND_LOCATION + 'deconnexion.ogg" type="audio/ogg" preload="auto"></audio>');
	deconnexion_sound = document.getElementById(GM_info.script.name + '_deconnexion');
}
function load_game_sound () {
	add_text(check_div, 'span', '', 'display:none', '<audio id="' + GM_info.script.name + '_opponent"><source src="' + SOUND_LOCATION + 'opponent.ogg" type="audio/ogg" preload="auto"></audio>');
	opponent_sound = document.getElementById(GM_info.script.name + '_opponent');
	add_text(check_div, 'span', '', 'display:none', '<audio id="' + GM_info.script.name + '_friend"><source src="' + SOUND_LOCATION + 'friend.ogg" type="audio/ogg" preload="auto"></audio>');
	friend_sound = document.getElementById(GM_info.script.name + '_friend');
	add_text(check_div, 'span', '', 'display:none', '<audio id="' + GM_info.script.name + '_passe"><source src="' + SOUND_LOCATION + 'passe.ogg" type="audio/ogg" preload="auto"></audio>');
	passe_sound = document.getElementById(GM_info.script.name + '_passe');
	add_text(check_div, 'span', '', 'display:none', '<audio id="' + GM_info.script.name + '_fight"><source src="' + SOUND_LOCATION + 'fight.ogg" type="audio/ogg" preload="auto"></audio>');
	fight_sound = document.getElementById(GM_info.script.name + '_fight');
	add_text(check_div, 'span', '', 'display:none', '<audio id="' + GM_info.script.name + '_PA"><source src="' + SOUND_LOCATION + 'PA.ogg" type="audio/ogg" preload="auto"></audio>');
	PA_sound = document.getElementById(GM_info.script.name + '_PA');
}

/** Retourne la permission d’afficher des notifications */
function get_notification_permission() {
	return Notification.permission;
}
/** Demande la permission d’afficher des notifications */
function allow_notification() {
	Notification.requestPermission();
}
/** Affiche la notification */
function show_notification (my_icon, my_title, my_text) {
	var notification = new Notification('MBL - ' + my_title, {
		dir: "auto",
		lang: "",
		body: my_text,
		tag: "CM/MBL",
		icon: my_icon,
		onshow: function() { setTimeout(notification.close(), 1500); },
	});
}
