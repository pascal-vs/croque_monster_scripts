// ==UserScript==
// @name			cmMenuSinge
// @version			1.19
// @namespace		http://www.croquemonster.com
// @description		Ajoute un onglet bien utile sur le dailymonster et votre page personnelle
// @include			http://www.croquemonster.com/news*
// @include			/http://www\.croquemonster\.com/user/[0-9]+/
// @copyright		Pascal Vander-Swalmen aka squalbee from famous RCC! :)
// @grant			GM_getValue
// @grant			GM_setValue
// @grant			GM_xmlhttpRequest
// @license			LGPL http://www.gnu.org/licenses/lgpl.html
// ==/UserScript==

// BUGS liste
// optim sur argent parfois moins bonne que optim sur inferno (souvent même contrats mais trop d'objets utilisés ou mauvais choix de monstre si pas assez de contrats)

// TODO liste
// liens vers les occupations (fonction print_busy)

// récupération de l'heure locale
var local_time = new Date();

// constantes
var equipements = new Object();
equipements.nom = ['Sadisme +1 temporaire', 'Sadisme +1 permanent', 'Sadisme +2 permanents', 'Laideur +1 temporaire', 'Laideur +1 permanent', 'Laideur +2 permanents', 'Force +1 temporaire', 'Force +1 permanent', 'Force +2 permanents', 'Gourmandise +1 temporaire', 'Gourmandise +2 temporaires', 'Gourmandise +1 permanent', 'Gourmandise +2 permanents', 'Contrôle +1 temporaire', 'Contrôle +1 permanent', 'Contrôle +2 permanents', 'Endurance +1 permanent', 'Endurance +2 permanent', 'Fatigue -2h', 'Fatigue -10h', 'Combat +1 temporaire', 'Combat +1 permanent', 'Combat +2 permanents', 'Mallette vivante', 'Tripatouilleur de portail', 'Disrupteur de portail', 'Régulateur de marché VRP', 'Chapeau ZinZin', 'Chapeau Lutin', 'Porte monnaie Bigbluff', 'Sblurbotron', 'Dop-Dopage', 'Part de gâteau d\'anniversaire'];
equipements.prix = [200, 1000, 2000, 200, 1000, 2000, 200, 1000, 2000, 200, 500, 1000, 2000, 200, 1000, 2000, 999999999, 999999999, 999999999, 999999999, 200, 1000, 2000, 1000, 1000, 2000, 4000, 999999999, 999999999, 4000, 999999999, 5000, 999999999];
equipements.bonus = [1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var KO = 0;
var OK = 1;
var MONEY = 0;
var INFERNO = 1;
var BUSY_NONE = -1;
var BUSY_CONTRAT = 0;
var BUSY_ESCORTE = 1;
var BUSY_ATTAQUE = 2;
var BUSY_RACKET = 3;
var BUSY_PROPAGANDE = 4;
var BUSY_MBL = 5;
var NB_ITEMS = 32;
var KIND_KID = 0;
var KIND_EASY = 1;
var KIND_NORMAL = 2;
var KIND_HARD = 3;
var KIND_MONSTER = 4;
var KIND_INFERNAL = 5;
var LIMIT_45 = 2700;
var LIMIT_15 = 900;
var SCORE_V = 100000;
var SCORE_H = 1000000;
var TYPE_RIEN = 0;
var TYPE_ERREUR = 1;
var TYPE_EQUIP = 2;
var TYPE_AFFECT = 3;
var TYPE_INFO = 4;
var NB_LINKS = 6;
var NO = -1;
var HAIDAO = false;

// conteneurs utiles
var contenu_perso = document.createElement('div');
var full_print = new Object();
full_print.OK = true;
var liste_onglets = 0;
var tab_singe = 0;
var mbl_div = 0;
var reads_div = 0;
var raccourcis_div = 0;
var monsters_div = 0;
var monsters_body = 0;
var nb_filtered_div = 0;
var contracts_div = 0;
var special_div = 0;
var special_body = 0;
var monsters_ul = 0;
var stock_div = 0;
var stock_alert = 0;
var stock_button = 0;
var crado_nb_div = 0;
var crado_button = 0;
var crado_link = 0;
var filter_start = 0;
var filter_end = 0;
var gains = new Object();
var valid_div_N = 0;
var valid_div_S = 0;
var config_div = 0;
var config_on = 0;
var config_off = 0;
var help_div = 0;
var help_on = 0;
var help_off = 0;
var simulation = 0;

// variables
var first_time = true;
var affect = false;
var verif = false;
var params_input = new Object();
params_input.shortcuts = new Array(NB_LINKS);
var params = new Object();
params.equip_watched = new Array(NB_ITEMS);
params.stock = new Array(NB_ITEMS);
params.shortcuts = new Array(NB_LINKS);
var stock = new Array(NB_ITEMS);
var stock_tobuild = new Array();
var agency = new Object();
agency.id = get_my_ID();
agency.name = '';
agency.cities = 0;
agency.monsters = 0;
agency.syndicate = '';
agency.syndicateId = 0;
agency.syndicate_OK = false;
var API = new Object();
API.name = '';
API.pass = '';
var API_pass_input = 0;
var API_check = false;
var contracts = new Object();
contracts.table = new Array();
contracts.OK = false;
contracts.nb = 0;
contracts.max = 0;
contracts.Dmax = 0;
contracts.Hmax = 0;
var prout_OK = false;
var PDM = new Object();
PDM.OK = false;
PDM.page = 1;
var monsters = new Array();
var monsters_OK = false;
var reverse_monsters = new Array();
var nb_tired = 0;
var CM = new Object();
var baseH = 0;
var baseM = 0;
var baseS = 0;
var jetlag = 0;
var diff_seconds = 0;
var params_fonction_equipement = new Object();
var retour_fonction_equipement = OK;
var params_fonction_affectation = new Object();
var retour_fonction_affectation = OK;
var cities = new Object();
cities.infernal = new Array();
cities.prout = new Array();
cities.PDM = new Array();
var cities_OK = false;
var sec_var = 0;
var sec_OK = false;
var stock_OK = false;
var dispo = new Object();
dispo.monsters = new Array();
dispo.contracts = new Array();
var specials = new Array();
var paradox = new Object();
var histo = new Object();
var moon = new Object();
moon.done = false;
moon.timezone = new Object();
var special_table = new Array();
var special_OK = false;

//~ var debug_txt = '';

// calcul du décalage horaire
compute_jetlag();

// récupération de mon ID CM
function get_my_ID() {
	level = document.getElementById('level');
	if (level == null) return 0;
	level = level.getElementsByTagName('a');
	user_href = level[0].getAttribute('href');
	return user_href.substring(user_href.lastIndexOf("/") + 1);
}

// test si bonne page si page "user"
function check_page() {
	// si on est sur le daily => OK
	var pattern = /news/;
	if (pattern.test(location.href)) return true;
	// si on est sur la page perso => OK
	if (agency.id == parseInt(location.href.substr(34), 10)) return true;
	return false;
}

// récupération d'une page autre que la page en cours
function getDOC(url, callback) {
    GM_xmlhttpRequest({
        method: 'GET',
        url: url,
		headers: {
			"User-agent": "Mozilla/4.0 (compatible) Greasemonkey",
			"Content-Type": "text/plain; charset=utf-8",
			"Pragma": "no-cache",
			"Refresh": "1",
		},
		onload: function(responseDetails) {
			if (!responseDetails.responseText || responseDetails.responseText == "") {
				print_error('aucune donnée reçue de : ' + url);
				return;
			}
			var doc;
			if (!responseDetails.responseXML) doc = new DOMParser().parseFromString(responseDetails.responseText, "text/xml");
			else doc = responseDetails.responseXML;
			callback(doc); // ici je rappelle la fonction qui va extraire l'information
		},
		onerror: function(responseDetails) {
			print_error('erreur pour charger : ' + ur);
		}
    });
}
function fuseau(doc) {
	if (doc.documentElement.textContent == "") return true;
	if (doc.getElementById('minimal') != null) return true;
	return false;
}

// calcul de la différence entre l'heure du monster pod et l'heure réelle
function compute_jetlag() {
	var time = document.getElementById('serverTime');
	if (time == null) return;
	time = time.innerHTML;
	CM.h = parseInt(time.substr(0, 2), 10);
	CM.m = parseInt(time.substr(3, 2), 10);
	CM.s = parseInt(time.substr(6, 2), 10);
	jetlag = CM.h - local_time.getHours();
	diff_seconds = (CM.m - local_time.getMinutes()) * 60 + CM.s - local_time.getSeconds();
}

// mise à jour du monster pod
function update_pod() {
	// heure
	compute_time();
	var element = document.getElementById('serverTime');
	element.innerHTML = print_2digits(baseH) + ':' + print_2digits(baseM) + ':' + print_2digits(baseS);

	// level
	var element = document.getElementById('level').getElementsByTagName('span')[0];
	if (agency.level != parseInt(element.innerHTML, 10)) element.innerHTML = agency.level;

	// reputation
	element = document.getElementById('reputation').getElementsByTagName('span')[0];
	if (agency.reputation != parseInt(element.innerHTML.replace(/\./g, ''), 10)) element.innerHTML = print_UK_number(agency.reputation);

	// monstercrédits
	element = document.getElementById('gold').getElementsByTagName('span')[0];
	if (agency.gold != parseInt(element.innerHTML.replace(/\./g, ''), 10)) element.innerHTML = print_UK_number(agency.gold);

	// messages
	element = document.getElementById('nbPage').getElementsByTagName('span')[0];
	if (agency.mails != parseInt(element.innerHTML, 10)) {
		element.innerHTML = agency.mails;
		if (agency.mails == 0) element.setAttribute('class', 'msgIcon');
		else element.setAttribute('class', 'msgNewIcon');
	}
}

// calcul de l'heure CM
function compute_time() {
	var time = new Date();

	baseS = 0;
	baseM = 0;
	baseH = 0;

	baseS = time.getSeconds() - diff_seconds;
	if (baseS >= 60) {
		baseS = baseS % 60;
		baseM += 1;
	}
	baseM += time.getMinutes() + Math.floor(diff_seconds / 60);
	if (baseM >= 60) {
		baseM = baseM % 60;
		baseH += 1;
	}
	baseH += (time.getHours() + jetlag) % 24;
}

// attente de la lecture d'une info pour lancer une fonction
function wait_for_API(func) {
	if (!API_check) { setTimeout(function () { wait_for_API(func); }, 10); }
	else func();
}
function wait_for_syndicate(func) {
	if (!agency.syndicate_OK) { setTimeout(function () { wait_for_syndicate(func); }, 10); }
	else func();
}
function wait_for_contracts(func) {
	if (!contracts.OK) { setTimeout(function () { wait_for_contracts(func); }, 10); }
	else func();
}
function wait_for_contracts_and_monsters(func) {
	if (!contracts.OK || !monsters_OK) { setTimeout(function () { wait_for_contracts_and_monsters(func); }, 10); }
	else func();
}
function wait_for_monsters(func) {
	if (!monsters_OK) { setTimeout(function () { wait_for_monsters(func); }, 10); }
	else func();
}
function wait_for_cities(func) {
	if (!cities_OK) { setTimeout(function () { wait_for_cities(func); }, 10); }
	else func();
}
function wait_for_verif(func) {
	if (!verif) { setTimeout(function () { wait_for_verif(func); }, 10); }
	else func();
}
function wait_for_equipement(func) {
	if (!equipement_done) { setTimeout(function () { wait_for_equipement(func); }, 10); }
	else func();
}
function wait_for_affectation(func) {
	if (!affect) { setTimeout(function () { wait_for_affectation(func); }, 10); }
	else func();
}
function wait_for_paradox(func) {
	if (!paradox.done) { setTimeout(function () { wait_for_paradox(func); }, 10); }
	else func();
}
function wait_for_sec(func) {
	if (!sec_OK) { setTimeout(function () { wait_for_sec(func); }, 10); }
	else func();
}
function wait_for_PDM(func) {
	if (!PDM.OK) { setTimeout(function () { wait_for_PDM(func); }, 10); }
	else func();
}
function wait_for_prout(func) {
	if (!prout_OK) { setTimeout(function () { wait_for_prout(func); }, 10); }
	else func();
}
function wait_for_stock(func) {
	if (!stock_OK) { setTimeout(function () { wait_for_stock(func); }, 10); }
	else func();
}
function wait_for_full_print(func) {
	if (!full_print.OK) { setTimeout(function () { wait_for_full_print(func); }, 10); }
	else func();
}
function wait_for_moon(func) {
	if (!moon.done) { setTimeout(function () { wait_for_moon(func); }, 10); }
	else func();
}
function wait_for_special(func) {
	if (!special_OK) { setTimeout(function () { wait_for_special(func); }, 10); }
	else func();
}

// tri d'un tableau sur les valeurs en ordre décroissant
function sortfunc_value(a,b) { return b.value - a.value; }
function sortfunc(a,b) { return b - a; }

// gestion de l'accès API
function set_API() { GM_setValue('API_' + agency.id, '(' + JSON.stringify(API) + ')'); }
function init_API() {
	var rep = prompt('Votre mot de passe API est nécessaire pour ce script.\nMerci de bien vouloir le fournir','');
	if (rep) API.pass = rep;
}
function get_API() {
	var temp_API = GM_getValue('API_' + agency.id, 'null');
	if (temp_API != 'null') API = eval(temp_API);
	if (!API.pass) API.pass = '';
}

// lecture des données de l'agence avec l'API
function get_agency() {
	API_check = false;
	getDOC(GM_info.script.namespace + '/api/agency.xml?id=' + agency.id + ';pass=' + API.pass, function (agency_API) {
		// gestion des erreurs API, test seulement ici car ensuite on sait qu'on a le bon API (sauf si changement entre-temps... mais bon... pfiou...)
		if (agency_API.getElementsByTagName('err').length > 0) {
			API.name = '';
			API.pass = '';
			set_API();
			API_check = true;
			return;
		}

		if (fuseau(agency_API)) {
			setTimeout(get_agency, 1000);
			return;
		}

		var agency_content = agency_API.getElementsByTagName('agency')[0];

		// infos utiles pour le script
		API.name = agency_content.getAttribute('name');
		API_check = true;
		agency.cities = parseInt(agency_content.getAttribute('cities'), 10);
		agency.monsters = parseInt(agency_content.getAttribute('monsters'), 10);
		var temp = agency_content.getAttribute('syndicate');
		if (temp != null) {
			agency.syndicate = temp;
			agency.syndicateId = parseInt(agency_content.getAttribute('syndicateId'), 10);
		}
		else {
			agency.syndicate = '';
			agency.syndicateId = 0;
		}

		// autres infos pour le pod
		agency.level = parseInt(agency_content.getAttribute('level'), 10);
		agency.reputation = parseInt(agency_content.getAttribute('reputation'), 10);
		agency.gold = parseInt(agency_content.getAttribute('gold'), 10);
		agency.mails = parseInt(agency_content.getAttribute('mails'), 10);

		agency.syndicate_OK = true;
	});
}

// fonctions pour gérer l'affichage des options
function show_config() {
	config_on.style.display = "none";
	config_off.style.display = "inline";
	config_div.style.display = "block";
}
function hide_config() {
	config_on.style.display = "inline";
	config_off.style.display = "none";
	config_div.style.display = "none";
}

// fonctions pour gérer l'affichage de l'aide
function show_help() {
	help_on.style.display = "none";
	help_off.style.display = "inline";
	help_div.style.display = "block";
}
function hide_help() {
	help_on.style.display = "inline";
	help_off.style.display = "none";
	help_div.style.display = "none";
}

// fonctions d'ajout de balises input
function add_input_text(here, name, value, size, max) {
	var x = document.createElement('input');
	x.setAttribute('type', 'text');
	x.setAttribute('id', GM_info.script.name + '_input_text_' + name);
	x.setAttribute('name', name);
	x.setAttribute('value', value);
	x.setAttribute('size', size);
	x.setAttribute('maxlength', max);
	here.appendChild(x);
	return x;
}
function add_input_pass(here, name, value, size) {
	var x = document.createElement('input');
	x.setAttribute('type', 'password');
	x.setAttribute('id', GM_info.script.name + '_input_password_' + name);
	x.setAttribute('name', name);
	x.setAttribute('value', value);
	x.setAttribute('size', size);
	here.appendChild(x);
	return x;
}
function add_input_checkbox(here, name, checked) {
	var x = document.createElement('input');
	x.setAttribute('type', 'checkbox');
	x.setAttribute('id', GM_info.script.name + '_input_checkbox_' + name);
	x.setAttribute('name', name);
	x.checked = checked;
	here.appendChild(x);
	return x;
}
function add_input_radio(here, name, value, checked) {
	var x = document.createElement('input');
	x.setAttribute('type', 'radio');
	x.setAttribute('id', GM_info.script.name + '_input_checkbox_' + name + '_' + value);
	x.setAttribute('name', name);
	x.setAttribute('id', value);
	x.checked = checked;
	here.appendChild(x);
	return x;
}

// fonctions pour les selects
function select_set_value(x, value) { x.selectedIndex = value; }
function select_get_value(x) { return x.options[x.selectedIndex].value; }

// fonctions d'ajout de bouton
function add_cancel_button(here, id, text, details) {
	var a = document.createElement('a');
	if (id != '') a.setAttribute('id', GM_info.script.name + '_button_' + id);
	a.setAttribute('class', 'cancelButton');
	a.title = details;
	a.innerHTML = text;
	here.appendChild(a);
	return a;
}
function add_button(here, id, text, details) {
	var a = document.createElement('a');
	if (id != '') a.setAttribute('id', GM_info.script.name + '_button_' + id);
	a.setAttribute('class', 'okButton');
	a.title = details;
	a.innerHTML = text;
	here.appendChild(a);
	return a;
}
function add_link_button(here, id, text, href, details) {
	var a = add_button(here, id, text, details);
	a.href = href;
	return a;
}
function add_link(here, id, text, href) {
	var a = document.createElement('a');
	if (id != '') a.setAttribute('id', GM_info.script.name + '_link_' + id);
	a.innerHTML = text;
	a.href = href;
	here.appendChild(a);
	return a;
}
function add_func_button(here, id, text, func, details) {
	var a = add_button(here, id, text, details);
	a.addEventListener('click', func, false);
	return a;
}
function add_func_cancel_button(here, id, text, func, details) {
	var a = add_cancel_button(here, id, text, details);
	a.addEventListener('click', func, false);
	return a;
}
function add_func_link(here, id, text, func, details) {
	if (here.firstChild) here.removeChild(here.firstChild);
	var a = document.createElement('a');
	if (id != '') a.setAttribute('id', GM_info.script.name + '_link_' + id);
	a.title = details;
	a.innerHTML = text;
	a.addEventListener('click', func, false);
	here.appendChild(a);
	return a;
}
function add_span_func(here, id, classe, text, func) {
	var x = add_text(here, 'span', id, classe, text);
	x.addEventListener('click', func, false);
	x.setAttribute('style', 'cursor:pointer');
	return x;
}
// ajout d'un "Tip" (boîte volante) -- attention aux ' et " dans les chaînes de caractères
function add_tip(element, title, content) {
	element.setAttribute('onmouseout', 'Tip.hide();');
	var temp = 'null';
	if (title != '') temp = '\'' + title + '\'';
	element.setAttribute('onmouseover', 'Tip.show(' + temp + ',\'' + content + '\',null,event);');
}

// pluriel pour 'al' et 'aux'
function pluriel_al(i) {
	if (i > 1) return 'aux';
	else return 'al';
}
// mettre un 's' si pluriel
function pluriel(i) {
	if (i > 1) return 's';
	else return '';
}

// ajout des div et span pour la mise en page
function add_text(here, type, id, classe, text) {
	var x = document.createElement(type);
	if (id != '') x.setAttribute('id', GM_info.script.name + '_' + type + '_' + id);
	if (classe != '') x.setAttribute('class', classe);
	x.innerHTML = text;
	here.appendChild(x);
	return x;
}

// affichage sur fond noir : démarrer
function full_print_start(type, text) {
	full_print.OK = false;
	full_print.id = 'validationBox';
	if (type == KO) full_print.id = 'refreshWarning';
	var print_div = document.createElement('div');
	print_div.setAttribute('id', full_print.id);
	var div = document.createElement('div');
	div.setAttribute('class', 'blackHole');
	print_div.appendChild(div);
	var div_popup = document.createElement('div');
	div_popup.setAttribute('class', 'sitePopup');
	div = document.createElement('div');
	div.setAttribute('class', 'header');
	div_popup.appendChild(div);
	full_print.div = document.createElement('div');
	full_print.div.setAttribute('class', 'content');
	div = document.createElement('div');
	full_print.text = document.createElement('p');
	div.appendChild(full_print.text);
	full_print.div.appendChild(div);
	full_print.text.innerHTML = '<img src="/gfx/forum/icon_chart.gif" width="16px"> <strong>' + text + '</strong><br>';
	div_popup.appendChild(full_print.div);
	div = document.createElement('div');
	div.setAttribute('class', 'footer');
	div_popup.appendChild(div);
	print_div.appendChild(div_popup);
	document.body.insertBefore(print_div,document.getElementById('tooltip'));
}
// affichage sur fond noir : ajouter une ligne
function full_print_add(type, text) {
	var image = '';

	switch(type) {
		case TYPE_ERREUR: image = '<img src="/gfx/forum/icon_cross.gif" width="14px">&nbsp;'; break;
		case TYPE_EQUIP: image = '<img src="/gfx/icons/equip.gif" width="14px">&nbsp;'; break;
		case TYPE_AFFECT: image = '<img src="/gfx/forum/greediness.gif" width="13px">&nbsp;'; break;
		case TYPE_INFO: image = '<img src="/gfx/forum/icon_arrow.gif" width="14px">&nbsp;'; break;
		default:;
	}
	full_print.text.innerHTML += '<br>' + image + text;
}
// affichage sur fond noir : terminer
function full_print_end(text) {
	var b = add_func_button(full_print.div, 'close_fullprint', text, function () {
				document.body.removeChild(document.getElementById(full_print.id));
				full_print.OK = true;
			}, 'Fermer cette information');
	b.setAttribute('class','buttonPopup');
	b.setAttribute('style', 'cursor:pointer');
}

// ajoute un raccourci vers le dernier match MBL
function add_match() {
	if (params.read_mbl) {
		wait_for_syndicate(function () {
			if (agency.syndicateId != 0) {
				getDOC(GM_info.script.namespace + '/mbl/team', function (match_list) {
					if (fuseau(match_list)) {
						setTimeout(add_match, 1000);
						return;
					}

					var div = match_list.getElementById('content');
					var trs = div.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
					if (trs.length > 0) {
						var nb = 0;
						var tds;
						var encours = /En cours/;
						var avenir = /A venir/;
						for ( var i = 0 ; i < trs.length ; i++) {
							tds = trs[i].getElementsByTagName('td');
							if (encours.test(tds[2].innerHTML)) {
								add_text(mbl_div, 'h1', 'mbl_last_' + nb, 'noMarg', '<img src="/gfx/icons/mbl.gif" width="18px"> Match ' + tds[0].innerHTML.toLowerCase() + ' : ' + tds[1].innerHTML + ' -- ' + tds[5].innerHTML);
								nb++;
							}
							if (avenir.test(tds[2].innerHTML)) {
								add_text(mbl_div, 'h1', 'mbl_last_' + nb, 'noMarg', '<img src="/gfx/icons/mbl.gif" width="18px"> Match ' + tds[0].innerHTML.toLowerCase() + ' (bientôt) : ' + tds[1].innerHTML + ' -- ' + tds[5].innerHTML);
								nb++;
							}
						}
						if (nb == 0) {
							add_text(mbl_div, 'h1', 'mbl_last', 'noMarg', '<img src="/gfx/icons/mbl.gif" width="18px"> Aucun match en cours ou à venir -- <a href="' + GM_info.script.namespace + '/mbl/team">liste des matchs</a>');
						}
					}
				});
			}
		});
	}
}

// affiche les liens qui ne s'affichent que si le joueur a rejoint un syndicat
function add_links_only_syndicate(here) {
	wait_for_syndicate(function () {
		if (agency.syndicateId != 0) {
			add_link_button(here, 'tas', 'Tas', '/syndicate/dump', 'Tas d\'ordures');
		}
	});
}

// affiche les liens définis par l'utilisateur
function add_links_player(here) {
	var empty = true;
	var i = 0;
	while (i < NB_LINKS && empty) {
		if (params.shortcuts[i].name != '') empty = false;
		i++;
	}
	if (!empty) {
		var shortcuts = add_text(here, 'div', 'shortcuts_label', '', '<img src="gfx/forum/reputation.gif">&nbsp;<strong>Persos </strong><img src="gfx/forum/reputation.gif">&nbsp;');
		shortcuts.setAttribute('style', 'background-color: #EAF098;border-bottom:3px solid #1C5059;color:#1C5059;padding:3px 3px 3px 20px');
		for ( i = 0 ; i < NB_LINKS ; i++ ) {
			if (params.shortcuts[i].name != '') {
				add_link_button(shortcuts, 'shortcut_'+i, params.shortcuts[i].name.replace(' ', '&nbsp;'), '/' + params.shortcuts[i].link, 'Raccourci personnalisé ' + (i+1));
			}
		}
	}
}

// affiche les lectures
function add_reads() {
	if (params.check_forums) {
		getDOC(GM_info.script.namespace + '/forum/bookmarks', function (fav_page) {
			if (fuseau(fav_page)) {
				setTimeout(add_reads, 1000);
				return;
			}

			var nb = fav_page.getElementById('forum').getElementsByClassName('titlefalse').length + fav_page.getElementById('forum').getElementsByClassName('postitfalse').length;

			if (nb > 0) {
				add_link_button(raccourcis_div, 'bookmarks_forum', 'Forum&nbsp;-&nbsp;Favoris&nbsp;<img style="margin:0px 0px 2px 0px;" src="/gfx/icons/mailblink.gif" alt="(' + nb + ' non lus)">&nbsp;' + nb, '/forum/bookmarks', nb + ' messages non lus');
			}
			else {
				add_link_button(raccourcis_div, 'bookmarks_forum', 'Forum&nbsp;-&nbsp;Favoris&nbsp;<img style="margin:0px 0px 2px 0px;" src="/gfx/icons/nomail.gif" alt="(tous lus)">', '/forum/bookmarks', 'Aucun nouveau message');
			}
		});
	}
	else {
		add_link_button(raccourcis_div, 'bookmarks_forum', 'Forum&nbsp;-&nbsp;Favoris', '/forum/bookmarks', '');
	}
}

// affiche les lectures d'un joueur syndiqué
function add_reads_only_syndicate() {
	wait_for_syndicate(function () {
		if (agency.syndicateId != 0) {
			if (params.check_forums) {
				getDOC(GM_info.script.namespace + '/forum/syndicate', function (forum_page) {
					if (fuseau(forum_page)) {
						setTimeout(add_reads_only_syndicate, 1000);
						return;
					}

					var nb = forum_page.getElementById('forum').getElementsByClassName('titlefalse').length + forum_page.getElementById('forum').getElementsByClassName('postitfalse').length;

					if (nb > 0) {
						add_link_button(raccourcis_div, 'private_forum', 'Forum&nbsp;-&nbsp;Syndicat&nbsp;<img style="margin:0px 0px 2px 0px;" src="/gfx/icons/mailblink.gif" alt="(' + nb + ' non lus)">&nbsp;' + nb, '/forum/syndicate', nb + ' messages non lus');
					}
					else {
						add_link_button(raccourcis_div, 'private_forum', 'Forum&nbsp;-&nbsp;Syndicat&nbsp;<img style="margin:0px 0px 2px 0px;" src="/gfx/icons/nomail.gif" alt="(tous lus)">', '/forum/syndicate', 'Aucun nouveau message');
					}
				});
			}
			else {
				add_link_button(raccourcis_div, 'private_forum', 'Forum&nbsp;-&nbsp;Syndicat', '/forum/syndicate', '');
			}
		}
	});
}

// utilisation d'une cradoline
function confirm_crado() {
	if (params.confirm_option_crado) if (!confirm('Utiliser une tablette de Cradoline pour enlever la fatigue de ' + nb_tired + ' monstre' + pluriel(nb_tired) + ' ?')) return;
	getDOC(crado_link, function (crado) {
		if (fuseau(crado)) {
			alert('Utilisation d\'une cradoline pendant le fuseau, vérifiez par vous-même mais il est très fortement probable que la cradoline n\'a pas été utilisée');
		}

		go();
	});
}

// contruction d'un objet
function build_item () {
	stock_div.innerHTML = 'Constructions en cours...';
	stock_alert.innerHTML = '';
	var iid = stock_tobuild[stock_tobuild.length-1];
	getDOC(GM_info.script.namespace + '/labo/buildItem?id=' + iid, function (item) {
		if (fuseau(item)) {
			setTimeout(build_item, 1000);
			return;
		}
		var erreur = item.getElementById('refreshWarning');
		if (erreur != null) {
			var contenu = erreur.getElementsByTagName('div')[3].innerHTML;
			var pattern = /manque/;
			if (pattern.test(contenu)) {
				var iid;
				while (iid == stock_tobuild[stock_tobuild.length-1]) stock_tobuild.pop();
				stock_div.innerHTML = 'Constructions stoppées !';
				stock_alert.innerHTML = '&nbsp;Vous manquez d\'objets humains, vérifiez par vous-mêmes...';
			}
			else {
				// attendre 1 minute
				stock_alert.innerHTML = '&nbsp;(atelier complet, gardez ce script ouvert pour ajouter les objets automatiquement)';
				setTimeout(build_item, 60000);
			}
		}
		else {
			if (stock_tobuild.length > 0) {
				stock_tobuild.pop();
				build_item();
			}
			else {
				stock_div.innerHTML = 'Conforme aux objectifs';
				stock_alert.innerHTML = '';
			}
		}
	});
}

// vérifie le stock du labo
function verify_stock() {
	stock_OK = false;
	getDOC(GM_info.script.namespace + '/api/inventory.xml?name=' + API.name + ';pass=' + API.pass, function (stock_API) {
		if (fuseau(stock_API)) {
			setTimeout(verify_stock, 1000);
			return;
		}

		// lecture du stock
		var items_list = stock_API.getElementsByTagName('item');
		for (var i = 0 ; i < NB_ITEMS ; i++ ) stock[i] = 0;
		for (var i = 0 ; i < items_list.length ; i++ )
			stock[parseInt(items_list[i].getAttribute('id'), 10)] = parseInt(items_list[i].getAttribute('qty'), 10);
		items_list = stock_API.getElementsByTagName('factory');
		// objets en construction
		if (items_list.length > 0) {
			stock[parseInt(items_list[0].getAttribute('id'), 10)]++;
			items_list = items_list[0].getElementsByTagName('next');
			for (var i = 0 ; i < items_list.length ; i++ )
				stock[parseInt(items_list[i].getAttribute('id'), 10)]++;
		}

		stock_OK = true;
		// objets à construire
		stock_tobuild.splice(0, stock_tobuild.length);
		var temp = 0;
		for (var i = 0 ; i < stock.length ; i++ ) {
			temp = params.stock[i] - stock[i];
			while (temp > 0) {
				stock_tobuild.push(i);
				temp--;
			}
		}
		if (stock_tobuild.length == 0) {
			stock_div.innerHTML = 'Conforme aux objectifs';
			stock_alert.innerHTML = '';
		}
		else {
			stock_div.innerHTML = '';
			stock_alert.innerHTML = '';
			var text = equipements.nom[stock_tobuild[stock_tobuild.length-1]];
			for (var i=stock_tobuild.length-2 ; i>=0 ; i-- ) text += '<br>' + equipements.nom[stock_tobuild[i]];
			text += '<br><br><strong>' + stock_tobuild.length + ' </strong>objet' + pluriel(stock_tobuild.length);
			stock_button = add_func_button(stock_div, 'stock', 'Construire les objets', build_item, '');
			add_tip(stock_button, 'Objets à construire', text);
			// problème : le bouton disparaît en laissant le Tip alors ajout de ceci
			stock_button.setAttribute('onmousedown', function () { stock_button.removeAttribute('onmouseover'); });
			stock_button.setAttribute('onmouseup', 'Tip.hide();');
		}
	});
}

// ajoute la ligne pour le stock
function add_line_stock() {
	var li = add_text(monsters_ul, 'li', 'stock', '', '');
	add_text(li, 'span', 'stock_txt', '', 'Stock : ');
	stock_div = add_text(li, 'span', 'stock', '', 'En cours de vérification...');
	stock_alert = add_text(li, 'span', 'stock_alert', '', '');
	verify_stock();
}

// obtenir les infos sur la cradoline
function get_crado() {
	getDOC(GM_info.script.namespace + '/monster/monstromatic', function (crado) {
		if (fuseau(crado)) {
			setTimeout(get_crado, 1000);
			return;
		}

		var crado_div = crado.getElementById('monstromatic');
		if (crado_div == null) {
			add_text(crado_button, 'span', 'crado_button', '', 'Vous n\'avez pas accès aux cradolines, vous ne pouvez pas retirer la fatigue de vos monstres');
		}
		else {
			var pattern = /<span class="cradoline">.*<\/span>/
			crado_nb_div.innerHTML = pattern.exec(crado_div.innerHTML);
			var numbers = /[0-9]+/;
			if (parseInt(numbers.exec(crado_nb_div.innerHTML), 10) > 0) {
				crado_link = crado_div.getElementsByTagName('a')[0].href;
				if (nb_tired > 0) {
					add_func_button(crado_button, 'crado_button', 'Utiliser une cradoline pour ' + nb_tired + ' monstre' + pluriel(nb_tired), confirm_crado, 'Enlève la fatigue de tous vos monstres');
				}
				else {
					add_text(crado_button, 'span', 'crado_button', '', 'Aucun monstre fatigué, gardez vos cradolines pour plus tard');
				}
			}
			else {
				add_text(crado_button, 'span', 'crado_button', '', 'Aucune cradoline, vous ne pouvez pas retirer la fatigue de vos monstres');
			}
		}
	});
}

// ajoute la ligne sur la cradoline
function add_line_crado() {
	var li = add_text(monsters_ul, 'li', 'crado', '', '');
	crado_nb_div = add_text(li, 'span', 'crado_nb_div', '', '');
	add_text(li, 'span', 'crado_separate', '', ' - ');
	crado_button = add_text(li, 'span', 'crado_button', '', '');
	wait_for_monsters(get_crado);
}

// indique si un monstre a un équipement
function verif_item(iid, items) {
	var equip = new RegExp(','+iid+',', '');
	return equip.test(items);
}

// retourne la valeur d'un bonus sur un monstre
function verif_bonus(iid, items) {
	if (!verif_item(iid, items)) return equipements.bonus[iid];
	return 0;
}

// affiche un contrat
function print_contrat(cid) {
	return contracts.table[cid].name + '/' + contracts.table[cid].city + ' (à ' + timezone2time(contracts.table[cid].timezone) + 'h)' + ' pour ' + contracts.table[cid].prize + '&nbsp;<img src="/gfx/forum/miniMoney.gif">';
}

// affiche un pourcentage d'infernal
function print_infernal(cid) {
	var txt = '';
	if (contracts.table[cid].infernal == -1) txt = '-';
	else {
		if (contracts.table[cid].infernal >= 90) txt = '<strong>' + contracts.table[cid].infernal + '&nbsp;%</strong>';
		else txt = contracts.table[cid].infernal + '&nbsp;%';
	}
	return txt;
}

// affiche une estimation
function print_estimation(cid, mid) {
	var txt = '';
	if (contracts.table[cid].estimation[mid].percent >= 100)
		txt = '<strong>100%</strong>';
	else {
		txt = contracts.table[cid].estimation[mid].percent + '%';
		if (contracts.table[cid].estimation[mid].percent_equip >= 100)
			txt = txt + ' <img src="gfx/forum/icon_arrow.gif" width="13px"> <strong>100%</strong>';
		else
			txt = txt + ' <img src="gfx/forum/icon_arrow.gif" width="13px"> '+ contracts.table[cid].estimation[mid].percent_equip + '%';
	}
	return txt;
}

// affiche une estimation pour les spéciaux
function print_estimation_special(cid, mid) {
	var txt = '';
	if (contracts.table[cid].estimation[mid].percent >= 100) txt = '<strong>100%</strong>';
	else {
		if (contracts.table[cid].estimation[mid].percent == contracts.table[cid].best_estimation)
			txt = '<strong>' + contracts.table[cid].estimation[mid].percent + '</strong>%';
		else
			txt = contracts.table[cid].estimation[mid].percent + '%';
		if (contracts.table[cid].estimation[mid].percent_equip == contracts.table[cid].best_estimation)
			txt += '<br>[<strong>' + contracts.table[cid].estimation[mid].percent_equip + '</strong>]%';
		else
			txt += '<br>[' + contracts.table[cid].estimation[mid].percent_equip + '%]';
	}
	return txt;
}

// affichage d'un monstre dans une petite boîte bien pratique
function add_tip_monster(element, mid) {
	var title;
	var txt;
	if (mid == -1) {
		title = 'Erreur';
		txt = 'Pas d\'infos sur ce monstre !';
	}
	else {
		title = monsters[mid].name;
		var reussite = 0;
		if (monsters[mid].successes + monsters[mid].failures > 0)
			reussite = Math.round(monsters[mid].successes / (monsters[mid].successes + monsters[mid].failures) * 10000) / 100;
		txt = '<div style=\\\'float:left;\\\'>' + monsters[mid].image + '</div><div style=\\\'float:left;\\\'><ul class=\\\'caracs\\\'><li class=\\\'sadism\\\'>' + monsters[mid].sadism + '</li><li class=\\\'ugliness\\\'>' + monsters[mid].ugliness + '</li><li class=\\\'power\\\'>' + monsters[mid].power + '</li><li class=\\\'greediness\\\'>' + monsters[mid].greediness + '</li></ul></div><div style=\\\'float:left;\\\'><ul class=\\\'caracs\\\'><li class=\\\'control\\\'>' + monsters[mid].control + '</li><li class=\\\'fight\\\'>' + monsters[mid].fight + '</li><li class=\\\'endurance\\\'>' + monsters[mid].endurance + '</li><li class=\\\'bounty\\\'>' + monsters[mid].bounty + '</li></ul></div><div style=\\\'clear:both;\\\'></div><div style=\\\'display:block;text-align:center;\\\'>Revente :&nbsp;' + monsters[mid].firePrize + '&nbsp;<img src=\\\'/gfx/forum/miniMoney.gif\\\' size=\\\'16px\\\'><br>Réussite :&nbsp;' + reussite + '&nbsp;%</div><div style=\\\'clear:both;\\\'></div>';
	}
	add_tip(element, title, txt);
}

// affichage d'un nombre sur 2 caractères
function print_2digits(nb) {
	if (nb < 10) return '0' + nb;
	else return '' + nb;
}

// affichage d'un nombre sur 3 caractères
function print_3digits(nb) {
	if (nb < 10) return '00' + nb;
	if (nb < 100) return '0' + nb;
	else return '' + nb;
}

// affichage d'un nombre UK... enfin, presque : avec des '.' entre chaque millier
function print_UK_number(nb) {
	var temp;
	var txt = '';
	while (nb > 1000) {
		temp = nb % 1000;
		txt = '.' + print_3digits(temp) + txt;
		nb = Math.floor(nb / 1000);
	}
	return '' + nb + txt;
}

// affichage d'un nombre avec des images
function print_nb_img(nb) {
	var str = nb.toString();
	var img = '<span class="imgNum"><img src="/gfx/font/gold/x.gif" alt="x">';
	var temp;

	for (var i = 0 ; i < str.length ; i++ ) {
		temp = str.charAt(i);
		img += '<img src="/gfx/font/gold/' + temp + '.gif" alt="' + temp + '">';
	}

	img += '</span>';
	return img;
}

// mise à jour des gains
function update_gains() {
	var cid = 0;
	var prix = 0;
	var prix_reel = 0;
	var nb = 0;
	var percent = 0;
	var pourcentage_tout = 1;
	var sum_esperance = 0;
	var sum = 0;
	var sum_esperance_reel = 0;
	var sum_reel = 0;
	for ( var i = 0 ; i < monsters.length ; i++ ) {
		if (!monsters[i].mbl) {
			cid = monsters[i].contrat.choix;
			if (cid != -1) {
				if (monsters[i].contrat.affect.checked) {
					nb++;
					if (monsters[i].contrat.equip.checked) {
						prix_reel = contracts.table[cid].prize - monsters[i].bounty - contracts.table[cid].estimation[i].equip_prix;
						percent = contracts.table[cid].estimation[i].percent_equip;
					}
					else {
						prix_reel = contracts.table[cid].prize - monsters[i].bounty;
						percent = contracts.table[cid].estimation[i].percent;
					}
					prix = contracts.table[cid].prize;
					sum += prix;
					sum_esperance += percent * prix / 100;
					// infos globales
					pourcentage_tout *= percent / 100;
					sum_esperance_reel += percent * prix_reel / 100;
					sum_reel += prix_reel;
				}
			}
		}
	}
	// mise à jour de l'affichage
	gains.nb.innerHTML = '<strong>' + nb + '</strong> monstre' + pluriel(nb);
	gains.percent.innerHTML = '<strong>' + Math.floor(pourcentage_tout.toFixed(2) * 100) + '</strong>%';
	gains.money.innerHTML = 'Totaux : <strong>' + sum + '</strong><br>Réels : ' + sum_reel;
	gains.esperance.innerHTML = 'Totale : <strong>' + Math.floor(sum_esperance) + '</strong><br>Réelle : ' + Math.floor(sum_esperance_reel);
	gains.table.style.display = 'block';
}

// affichage des contrats et calculs des gains
function print_choices_gains() {
	var cid = 0;
	var prix = 0;
	var prix_reel = 0;
	var nb = 0;
	var pourcentage_tout = 1;
	var sum_esperance = 0;
	var sum = 0;
	var sum_esperance_reel = 0;
	var sum_reel = 0;
	for ( var i = 0 ; i < monsters.length ; i++ ) {
		if (!monsters[i].mbl) {
			cid = monsters[i].contrat.choix;
			if (cid != -1) {
				prix = contracts.table[cid].prize;
				prix_reel = contracts.table[cid].prize - monsters[i].bounty - contracts.table[cid].estimation[i].equip_prix;
				// infos globales
				nb++;
				pourcentage_tout *= contracts.table[cid].estimation[i].percent_equip / 100;
				sum_esperance += contracts.table[cid].estimation[i].percent_equip * prix / 100;
				sum += prix;
				sum_esperance_reel += contracts.table[cid].estimation[i].percent_equip * prix_reel / 100;
				sum_reel += prix_reel;
				// infos du contrat pour le monstre						
				monsters[i].contrat.infernal.innerHTML = print_infernal(cid);
				monsters[i].contrat.credits.innerHTML = prix;
				monsters[i].contrat.reussite.innerHTML = print_link_estimation(cid, i, print_estimation(cid, i));
				if (contracts.table[cid].estimation[i].percent < 100) {
					monsters[i].contrat.equip.checked = true;
					monsters[i].contrat.equip.style.visibility = 'visible';
				}
				monsters[i].contrat.affect.checked = true;
				monsters[i].contrat.affect.style.visibility = 'visible';
			}
		}
	}
	// mise à jour de l'affichage
	if (nb > 0) {
		gains.nb.innerHTML = '<strong>' + nb + '</strong> monstre' + pluriel(nb);
		gains.percent.innerHTML = '<strong>' + Math.floor(pourcentage_tout.toFixed(2) * 100) + '</strong>%';
		gains.money.innerHTML = 'Totaux : <strong>' + sum + '</strong><br>Réels : ' + sum_reel;
		gains.esperance.innerHTML = 'Totale : <strong>' + Math.floor(sum_esperance) + '</strong><br>Réelle : ' + Math.floor(sum_esperance_reel);
		gains.table.style.display = 'block';
		// en cas de simulation, on n'affiche pas le bouton de validation
		if (!simulation.checked) valid_div_N.style.display = 'block';
	}
	else reinit_gains();

//~ help_div.innerHTML += '<br>' + debug_txt;

}

// lecture des news pour trouver le pourcentage et la date de la pleine lune
function get_moon() {
	moon.done = false;
	getDOC(GM_info.script.namespace + '/news', function (news_page) {
		if (fuseau(news_page)) {
			setTimeout(get_moon, 1000);
			return;
		}

		var moon_div = news_page.getElementsByClassName('nextMoon')[0];
		// date du bonus
		var date_pattern = /[0-9]{4}-[0-9]{2}-[0-9]{2}/;
		var date = date_pattern.exec(moon_div.innerHTML);
		compute_time();
		if (date == null) {
			// ici, la pleine lune est en cours
			moon.start = baseH + 1;
			moon.end = 23;
		}
		else {
			// date de la pleine lune
			date = date.toString();
			var moon_date = new Date(parseInt(date.substr(0,4), 10), (parseInt(date.substr(5,2), 10)-1), parseInt(date.substr(8,2), 10), 0, 0, 0, 0);
			// date de demain
			var today_pattern = /[0-9]{2} .* [0-9]{4}/;
			today_div = news_page.getElementsByClassName('date')[0];
			var today = today_pattern.exec(today_div.innerHTML);
			today = today.toString();
			var tomorrow = new Date(parseInt(today.slice(today.lastIndexOf(' ')+1), 10), replace_month_long(today.slice(today.indexOf(' ')+1, today.lastIndexOf(' '))), (parseInt(today.substr(0,2), 10) + 1), 0, 0, 0, 0);
			if (tomorrow.getTime() == moon_date.getTime()) {
				moon.start = 0;
				moon.end = baseH;
			}
			else {
				moon.start = 24;
				moon.end = 0;
			}
		}
		// valeur du bonus
		var bonus_pattern = /\+[0-9]+%/;
		var bonus = bonus_pattern.exec(moon_div.innerHTML);
		moon.bonus = parseInt(bonus, 10);
		// lecture OK
		moon.done = true;
	});
}

// estime une statistique
function estimate_stat(M_stat, C_stat, C_diff, C_time) {
	var percent;
	var bonus;
	if (C_time >= moon.start && C_time <= moon.end) bonus = moon.bonus;
	else bonus = 0;
	percent = 5 + bonus;
	if (M_stat > 0) percent = Math.min(100, Math.max(0, 100 + bonus + 5 * (M_stat - C_stat) - C_diff));
	return percent;
}

function estimate_statG(M_stat, C_stat, C_diff, C_time, M_control, C_kind) {
	var percent;
	// gourmandise normale
	if (C_stat > 0) percent = estimate_stat(M_stat, C_stat, C_diff, C_time);
	// pourcentage de pas miam (affichage inversé : miam = 100 - pourcentage)
	else {
		if (M_stat == 0) percent = 100;
		else {
			var multiple;
			var bonus;
			switch (C_kind) {
				case KIND_NORMAL: multiple = 1.5; break;
				case KIND_HARD: multiple = 2; break;
				default: multiple = 1;
			}
			if (C_time >= moon.start && C_time <= moon.end) bonus = moon.bonus;
			else bonus = 0;
			percent = Math.min(100, Math.max(0, 100 + bonus + 5 * (M_control - Math.round(multiple * M_stat))));
		}
	}
	return percent;
}

// estimation de la réussite d'un contrat
function estimate(cid) {
	if (contracts.table[cid].estimation == null) contracts.table[cid].estimation = new Array();
	// calculs avec et sans équipement temporaire
	var sadism = 0;
	var ugliness = 0;
	var power = 0;
	var greediness = 0;
	var temp = 0;
	for (var i = 0 ; i < monsters.length ; i++ ) {
		if (!monsters[i].mbl) {
			var C_time = timezone2time(contracts.table[cid].timezone);
			contracts.table[cid].estimation[i] = new Object();
			// pourcentage normal
			sadism = estimate_stat(monsters[i].sadism, contracts.table[cid].sadism, contracts.table[cid].difficulty, C_time);
			contracts.table[cid].estimation[i].percentS = sadism;
			ugliness = estimate_stat(monsters[i].ugliness, contracts.table[cid].ugliness, contracts.table[cid].difficulty, C_time);
			contracts.table[cid].estimation[i].percentU = ugliness;
			power = estimate_stat(monsters[i].power, contracts.table[cid].power, contracts.table[cid].difficulty, C_time);
			contracts.table[cid].estimation[i].percentP = power;
			greediness = estimate_statG(monsters[i].greediness, contracts.table[cid].greediness, contracts.table[cid].difficulty, C_time, monsters[i].control, contracts.table[cid].kind);
			contracts.table[cid].estimation[i].percentG = greediness;
			contracts.table[cid].estimation[i].percent = Math.floor((sadism * ugliness * power * greediness) / 1000000);
			// pourcentage avec équipement temporaire
			contracts.table[cid].estimation[i].equip = new Array();
			contracts.table[cid].estimation[i].equip_prix = 0;
			if (sadism < 100 && monsters[i].sadism_bonus > 0) {
				sadism = Math.min(100, sadism + 5 * monsters[i].sadism_bonus);
				contracts.table[cid].estimation[i].equip.push(0);
				contracts.table[cid].estimation[i].equip_prix += equipements.prix[0];
			}
			if (ugliness < 100 && monsters[i].ugliness_bonus > 0) {
				ugliness = Math.min(100, ugliness + 5 * monsters[i].ugliness_bonus);
				contracts.table[cid].estimation[i].equip.push(3);
				contracts.table[cid].estimation[i].equip_prix += equipements.prix[3];
			}
			if (power < 100 && monsters[i].power_bonus > 0) {
				power = Math.min(100, power + 5 * monsters[i].power_bonus);
				contracts.table[cid].estimation[i].equip.push(6);
				contracts.table[cid].estimation[i].equip_prix += equipements.prix[6];
			}
			// gourmandise normale
			if (contracts.table[cid].greediness > 0) {
				if (greediness < 100 && monsters[i].greediness_bonus > 0) {
					if (greediness < 90 && monsters[i].greediness_bonus == 3) {
						greediness = Math.min(100, greediness + 5 * monsters[i].greediness_bonus);
						contracts.table[cid].estimation[i].equip.push(9);
						contracts.table[cid].estimation[i].equip.push(10);
						contracts.table[cid].estimation[i].equip_prix += equipements.prix[9];
						contracts.table[cid].estimation[i].equip_prix += equipements.prix[10];
					}
					else {
						if (greediness >= 90 && greediness < 95 && monsters[i].greediness_bonus >= 2) {
							greediness = Math.min(100, greediness + 5 * monsters[i].greediness_bonus);
							contracts.table[cid].estimation[i].equip.push(10);
							contracts.table[cid].estimation[i].equip_prix += equipements.prix[10];
						}
						else {
							if (greediness >= 95 && greediness < 100 && monsters[i].greediness_bonus == 2) {
								greediness = Math.min(100, greediness + 5 * monsters[i].greediness_bonus);
								contracts.table[cid].estimation[i].equip.push(10);
								contracts.table[cid].estimation[i].equip_prix += equipements.prix[10];
							}
							else {
								if (greediness >= 95 && greediness < 100 && monsters[i].greediness_bonus >= 1) {
									greediness = Math.min(100, greediness + 5 * monsters[i].greediness_bonus);
									contracts.table[cid].estimation[i].equip.push(9);
									contracts.table[cid].estimation[i].equip_prix += equipements.prix[9];
								}
							}
						}
					}
				}
			}
			// pas miam
			else {
				if (greediness < 100 && monsters[i].control_bonus > 0) {
					greediness = Math.min(100, greediness + 5 * monsters[i].control_bonus);
					contracts.table[cid].estimation[i].equip.push(13);
					contracts.table[cid].estimation[i].equip_prix += equipements.prix[13];
				}
			}
			temp = Math.floor((sadism * ugliness * power * greediness) / 1000000);
			contracts.table[cid].estimation[i].percent_equipS = sadism;
			contracts.table[cid].estimation[i].percent_equipU = ugliness;
			contracts.table[cid].estimation[i].percent_equipP = power;
			contracts.table[cid].estimation[i].percent_equipG = greediness;
			contracts.table[cid].estimation[i].percent_equip = temp;
			var attente = timetowait(baseH, contracts.table[cid].timezone);
			if (!monsters[i].busy && monsters[i].fatigue < attente && temp > contracts.table[cid].best_estimation) contracts.table[cid].best_estimation = temp;
		}
	}
}

// récupération de la liste des contrats
function get_contracts() {
	contracts.OK = false;
	getDOC(GM_info.script.namespace + '/api/contracts.xml?name=' + API.name + ';pass=' + API.pass, function (contracts_API) {
		if (fuseau(contracts_API)) {
			setTimeout(get_contracts, 1000);
			return;
		}

		compute_time();

		// contrats
		var contracts_list = contracts_API.getElementsByTagName('contract');
		contracts.table.splice(0, contracts.table.length);
		contracts.table.length = contracts_list.length;
		if (contracts_list.length == 0) {
			contracts.Dmax.innerHTML = '0';
			contracts.Hmax.innerHTML = 'xx';
		}
		else {
			var distance_min = 25;
			var temp;
			for ( var i = 0 ; i < contracts_list.length ; i++ ) {
				contracts.table[i] = new Object();
				contracts.table[i].id = parseInt(contracts_list[i].getAttribute('id'), 10);
				contracts.table[i].name = contracts_list[i].getAttribute('name');
				contracts.table[i].age = contracts_list[i].getAttribute('age');
				contracts.table[i].city = contracts_list[i].getAttribute('city');
				contracts.table[i].country = contracts_list[i].getAttribute('country');
				contracts.table[i].kind = parseInt(contracts_list[i].getAttribute('kind'), 10);
				contracts.table[i].countdown = parseInt(contracts_list[i].getAttribute('countdown'), 10);
				contracts.table[i].timezone = parseInt(contracts_list[i].getAttribute('timezone'), 10);
				contracts.table[i].prize = parseInt(contracts_list[i].getAttribute('prize'), 10);
				contracts.table[i].accepted = (contracts_list[i].getAttribute('accepted') == "true");
				contracts.table[i].difficulty = parseInt(contracts_list[i].getAttribute('difficulty'), 10);
				contracts.table[i].sadism = parseInt(contracts_list[i].getAttribute('sadism'), 10);
				contracts.table[i].ugliness = parseInt(contracts_list[i].getAttribute('ugliness'), 10);
				contracts.table[i].power = parseInt(contracts_list[i].getAttribute('power'), 10);
				contracts.table[i].greediness = parseInt(contracts_list[i].getAttribute('greediness'), 10);
				contracts.table[i].infernal = -1;
				contracts.table[i].best_estimation = -1;
				temp = timedistance(baseH, contracts.table[i].timezone);
				if (temp < distance_min) distance_min = temp;
			}
			contracts.Dmax.innerHTML = 24 - distance_min;
			if (baseH - distance_min >= 0) {
				if (jetlag == 0) contracts.Hmax.innerHTML = baseH - distance_min;
				else {
					contracts.Hmax.innerHTML = (baseH - distance_min) + 'h pour CM et en heure locale à ' + (baseH - distance_min - jetlag);
				}
			}
			else {
				if (jetlag == 0) contracts.Hmax.innerHTML = baseH + 24 - distance_min;
				else {
					contracts.Hmax.innerHTML = (baseH + 24 - distance_min) + 'h pour CM et en heure locale à ' + (baseH + 24 - distance_min - jetlag);
				}
			}
		}
		contracts.nb.innerHTML = contracts_list.length;

		// paradoxe
		contracts.paradox = false;
		contracts.paradox_date = null;
		var paradox = contracts_API.getElementsByTagName('paradox');
		if (paradox.length > 0) {
			var local_date = new Date();
			local_date.setHours(local_date.getHours() + jetlag);
			var date_txt = paradox[0].getAttribute('next');
			contracts.paradox_date = new Date(parseInt(date_txt.substr(0, 4), 10), (parseInt(date_txt.substr(5, 2), 10)-1), parseInt(date_txt.substr(8, 2), 10), parseInt(date_txt.substr(11, 2), 10), parseInt(date_txt.substr(14, 2), 10), parseInt(date_txt.substr(17, 2), 10));
			if (local_date >= contracts.paradox_date) contracts.paradox = true;
		}

		// fin de lecture
		contracts.OK = true;
	});
}

// recherche un contrat par son id CM
function search_contract(cid) {
	var i = 0;
	while(i < contracts.table.length && contracts.table[i].id != cid) i++;
	if (i == contracts.table.length) return -1;
	else return i;
}

// recherche quel monstre s'occupe d'un contrat
function search_monster(cid) {
	var i;
	for ( i = monsters.length-1 ; i >= 0 ; i-- ) {
		if (monsters[i].busy && monsters[i].busy_type == BUSY_CONTRAT)
			if (monsters[i].busy_value == cid) break;
	}
	return i;
}

// génération des contrats
function generate_contracts () {
	if (params.confirm_option_contracts) if (!confirm('Voulez-vous vraiment générer la liste des contrats ?')) return;
	getDOC(GM_info.script.namespace + '/contract', function (contracts) {
		if (fuseau(contracts)) {
			setTimeout(generate_contracts, 1000);
			return;
		}
		go();
	});
}

// fonctions de conversions entre les heures et les timezones
function timezone2time(z) {
	if (z>0) return 24 - z;
	else return -z;
}
function time2timezone(h) {
	if (h>12) return 24 - h;
	else return -h;
}
function timedistance(h, z) {
	x = timezone2time(z);
	if (x <= h) return h - x;
	else return h + 24 - x;
}
function timetowait(h, z) { return 24 - timedistance(h, z); }

// ajoute la ligne concernant les contrats
function add_line_contracts() {
	var li = add_text(monsters_ul, 'li', 'contrats', '', '');

	add_text(li, 'span', 'contracts1', '', 'Contrats : ');
	contracts.nb = add_text(li, 'span', 'contracts_nb', '', '');
	add_text(li, 'span', 'contracts2', '', ' / ');
	contracts.max = add_text(li, 'span', 'contracts_max', '', '');
	add_text(li, 'span', 'contracts3', '', ' (générés sur ');
	contracts.Dmax = add_text(li, 'span', 'contracts_Dmax', '', '');
	add_text(li, 'span', 'contracts4', '', 'h, fin à ');
	contracts.Hmax = add_text(li, 'span', 'contracts_Hmax', '', '');
	add_text(li, 'span', 'contracts4', '', 'h) - ');
	var a = add_func_button(li, 'generation', '<img src="/gfx/icons/stat_miam.gif" size="17px"> Générer les contrats', generate_contracts, 'Génère les contrats, attention en période de contrats spéciaux, il faut les générer le moins souvent possible');

	contracts.max.innerHTML = agency.cities;

	get_contracts();
}

// ajoute la ligne du filtre horaire
function add_line_filter() {
	filter_start = document.createElement('select');
	var opt = document.createElement('option')
	opt.setAttribute('value', '0');
	opt.innerHTML = 'maintenant';
	filter_start.appendChild(opt);
	for ( var i = 1 ; i < 24 ; i++ ) {
		opt = document.createElement('option')
		opt.setAttribute('value', '' + i);
		opt.innerHTML = '+' + i + 'h (après ' + ((baseH+i)%24) + 'h)';
		filter_start.appendChild(opt);
	}
	filter_end = document.createElement('select');
	for ( var i = 1 ; i <= 24 ; i++ ) {
		opt = document.createElement('option')
		opt.setAttribute('value', '' + i);
		opt.innerHTML = '+' + i + 'h (à ' + ((baseH+i)%24) + 'h)';
		filter_end.appendChild(opt);
	}
	var li = add_text(monsters_ul, 'li', 'filter1', '', 'Filtre horaire de ');
	li.appendChild(filter_start);
	add_text(li, 'span', 'filter2', '', ' à ');
	li.appendChild(filter_end);
	nb_filtered_div = add_text(li, 'span', 'nb_filtered', '', '');
}

// ajoute une ligne pour éviter de prendre en compte la fatigue afin de pouvoir simuler une optimisation
function add_line_simulation() {
	var li = add_text(monsters_ul, 'li', 'simulation', '', 'Simulation (ignore la fatigue et les occupations) ');
	simulation = add_input_checkbox(li, 'simulation', false);
}

// rechercher la variable "sec" pour lancer un contrat
// inutile ?
function get_sec () {
	sec_OK = false;
	sec_var = 0;
	getDOC(GM_info.script.namespace + '/monster/monstromatic', function (sec) {
		if (fuseau(sec)) {
			setTimeout(get_sec, 1000);
			return;
		}
		crado_link = sec.getElementById('monstromatic').getElementsByTagName('a')[0].href;
		var pattern = /sec=.*;/;
		sec_var = pattern.exec(crado_link);
		sec_OK = true;
	});
}

// Équiper un monstre
function equip_item_direct() {
	equipement_done = false;
	if (retour_fonction_equipement == OK && params_fonction_equipement.monster_index < params_fonction_equipement.raf.length) {
		var mid = params_fonction_equipement.raf[params_fonction_equipement.monster_index].mid;
		var iid = params_fonction_equipement.raf[params_fonction_equipement.monster_index].liste[params_fonction_equipement.item_index];
		getDOC(GM_info.script.namespace + '/labo/useItem?iid=' + iid + ';mid=' + mid, function (result) {
			if (fuseau(result)) {
				setTimeout(equip_item_direct, 1000);
				return;
			}

			var erreur = result.getElementById('refreshWarning');
			if (erreur != null) {
				retour_fonction_equipement = KO;
				full_print_add(TYPE_ERREUR, '<strong>' + monsters[reverse_monsters[params_fonction_equipement.raf[params_fonction_equipement.monster_index].mid]].name + '</strong> : impossible de l\'équiper avec ' + equipements.nom[params_fonction_equipement.raf[params_fonction_equipement.monster_index].liste[params_fonction_equipement.item_index]] + ', vérifiez vos stocks');
			}
			params_fonction_equipement.item_index++;
			if (params_fonction_equipement.item_index >= params_fonction_equipement.raf[params_fonction_equipement.monster_index].liste.length) {
				full_print_add(TYPE_EQUIP, '<strong>' + monsters[reverse_monsters[params_fonction_equipement.raf[params_fonction_equipement.monster_index].mid]].name + '</strong> équipé');
				params_fonction_equipement.item_index = 0;
				params_fonction_equipement.monster_index++;
			}
			equip_item_direct();
		});
	}
	else equipement_done = true;
}
function equip_monster_direct(liste) {
	retour_fonction_equipement = OK;
	params_fonction_equipement.raf = liste;
	params_fonction_equipement.monster_index = 0;
	params_fonction_equipement.item_index = 0;
	equip_item_direct();
}
function equip_item_interactif() {
	if (params_fonction_equipement.id < params_fonction_equipement.raf.length) {
		getDOC(GM_info.script.namespace + '/labo/useItem?iid=' + params_fonction_equipement.raf[params_fonction_equipement.id] + ';mid=' + params_fonction_equipement.mid, function (result) {
			if (fuseau(result)) {
				setTimeout(equip_item_interactif, 1000);
				return;
			}

			var texte = '';
			var erreur = result.getElementById('refreshWarning');
			if (erreur != null) texte = 'Erreur';
			else {
				var retour = result.getElementById('validationBox');
				if (retour == null) texte = 'OK';
				else {
					if (params_fonction_equipement.raf[params_fonction_equipement.id] != 27) texte = 'OK';
					else {
						var contenu = retour.getElementsByTagName('div')[3].innerHTML;
						var pattern = /<strong>.*<\/strong>/;
						texte = contenu.match(pattern) + ' +1';
					}
				}
			}
			full_print_add(TYPE_RIEN, equipements.nom[params_fonction_equipement.raf[params_fonction_equipement.id]] + ' : ' + texte);
			params_fonction_equipement.id++;
			equip_item_interactif();
		});
	}
	else {
		full_print_end('Continuer');
		go();
	}
}
function equip_monster_interactif(mid, liste, liste_txt) {
	if (liste.length > 0)  {
		if (params.confirm_option_equip) {
			var s_char = pluriel(liste.length);
			var texte = 'Équipement' + s_char + ' manquant' + s_char + ' :';
			texte = texte + ' ' + liste_txt + '.\nConfirmez-vous l\'équipement de ' + monsters[mid].name + ' ?';
			if (!confirm(texte)) return;
		}

		full_print_start(OK, 'Équipement de ' + monsters[mid].name);
		monsters[mid].permanent_zone.innerHTML = '';
		params_fonction_equipement.mid = monsters[mid].id;
		params_fonction_equipement.id = 0;
		params_fonction_equipement.raf = liste;
		equip_item_interactif();
	}
}
function equip_monster_mbl(mid, liste, liste_txt) {
	if (liste.length > 0)  {
		if (params.confirm_option_mbl) {
			var s_char = pluriel(liste.length);
			var texte = 'Équipement' + s_char + ' manquant' + s_char + ' :';
			texte = texte + ' ' + liste_txt + '.\nConfirmez-vous l\'équipement de ' + monsters[mid].name + ' ?';
			if (!confirm(texte)) return;
		}

		full_print_start(OK, 'Équipement de ' + monsters[mid].name);
		params_fonction_equipement.mid = monsters[mid].id;
		params_fonction_equipement.id = 0;
		params_fonction_equipement.raf = liste;
		equip_item_interactif();
	}
}

// affecter un monstre
function affecter_un_monstre() {
	affect = false;
	if (retour_fonction_affectation == OK && params_fonction_affectation.id < params_fonction_affectation.raf.length) {
		var mid = params_fonction_affectation.raf[params_fonction_affectation.id];
		var cid = contracts.table[monsters[reverse_monsters[mid]].contrat.choix].id;
		getDOC(GM_info.script.namespace + '/contract/' + cid + '/accept?mid=' + mid + ';' + sec_var, function (result) {
			if (fuseau(result)) {
				setTimeout(affecter_un_monstre, 1000);
				return;
			}

			mid = reverse_monsters[params_fonction_affectation.raf[params_fonction_affectation.id]];
			var erreur = result.getElementById('refreshWarning');
			if (erreur != null) {
				retour_fonction_affectation = KO;
				full_print_add(TYPE_ERREUR, '<strong>' + monsters[mid].name + '</strong> : problème lors de l\'affectation au contrat');
			}
			else {
				full_print_add(TYPE_AFFECT, '<strong>' + monsters[mid].name + '</strong> affecté à ' + print_contrat(monsters[mid].contrat.choix));
			}
			params_fonction_affectation.id++;
			affecter_un_monstre();
		});
	}
	else affect = true;
}
// affecter une liste de monstres
function affecter_monstres(liste) {
	retour_fonction_affectation = OK;
	params_fonction_affectation.id = 0;
	params_fonction_affectation.raf = liste;
	affecter_un_monstre();
}

// vérification des dispos des monstres et des contrats
function get_verif_monsters() {
	monsters_OK = false;
	getDOC(GM_info.script.namespace + '/api/monsters.xml?name=' + API.name + ';pass=' + API.pass, function (monsters_API) {
		if (fuseau(monsters_API)) {
			setTimeout(get_verif_monsters, 1000);
			return;
		}

		var monsters_list = monsters_API.getElementsByTagName('monster');
		dispo.monsters.splice(0, dispo.monsters.length);
		dispo.monsters.length = monsters_list.length;
		var mid;
		var temp;
		for ( var i = 0 ; i < monsters_list.length ; i++ ) {
			dispo.monsters[i] = new Object();
			dispo.monsters[i].id = parseInt(monsters_list[i].getAttribute('id'), 10);
			dispo.monsters[i].fatigue = parseInt(monsters_list[i].getAttribute('fatigue'), 10);
			temp = monsters_list[i].getAttribute('isMblMonster');
			if (temp != null && !HAIDAO) dispo.monsters[i].OK = false;
			else {
				temp = monsters_list[i].getAttribute('contract');
				if (temp != null) dispo.monsters[i].OK = false;
				else {
					temp = monsters_list[i].getAttribute('escort');
					if (temp != null) dispo.monsters[i].OK = false;
					else {
						temp = monsters_list[i].getAttribute('attack');
						if (temp != null) dispo.monsters[i].OK = false;
						else {
							temp = monsters_list[i].getAttribute('racket');
							if (temp != null) dispo.monsters[i].OK = false;
							else {
								temp = monsters_list[i].getAttribute('propaganda');
								if (temp != null) dispo.monsters[i].OK = false;
								else {
									temp = monsters_list[i].getAttribute('match');
									if (temp != null) dispo.monsters[i].OK = false;
									else dispo.monsters[i].OK = true;
								}
							}
						}
					}
				}
			}
		}
		monsters_OK = true;
	});
}
function get_verif_contracts() {
	contracts.OK = false;
	getDOC(GM_info.script.namespace + '/api/contracts.xml?name=' + API.name + ';pass=' + API.pass, function (contracts_API) {
		if (fuseau(contracts_API)) {
			setTimeout(get_verif_contracts, 1000);
			return;
		}

		compute_time();

		var contracts_list = contracts_API.getElementsByTagName('contract');
		dispo.contracts.splice(0, dispo.contracts.length);
		dispo.contracts.length = contracts_list.length;
		var cid;
		var temp;
		for ( var i = 0 ; i < contracts_list.length ; i++ ) {
			dispo.contracts[i] = new Object();
			dispo.contracts[i].id = parseInt(contracts_list[i].getAttribute('id'), 10);
			dispo.contracts[i].countdown = parseInt(contracts_list[i].getAttribute('countdown'), 10);
			dispo.contracts[i].OK = (contracts_list[i].getAttribute('accepted') == "false");
		}
		contracts.OK = true;
	});
}
function get_verif() {
	verif = false;
	get_verif_monsters();
	get_verif_contracts();
	wait_for_contracts_and_monsters(function () { verif = true; });
}
function verif_dispo(mid, cid) {
	var midV = -1;
	var cidV = -1;
	var i = 0;
	// recherche de l'index du monstre dans le tableau de verif
	for ( i = 0 ; i < dispo.monsters.length ; i++ ) {
		if (dispo.monsters[i].id == monsters[mid].id) {
			midV = i;
			break;
		}
	}
	// monstre plus dans les rangs
	if (midV == -1) {
		full_print_add(TYPE_ERREUR, 'Erreur : ' + monsters[mid].name + ' ne fait plus partie de vos monstres');
		return false;
	}
	// monstre pas dispo
	if (!dispo.monsters[midV].OK) {
		full_print_add(TYPE_ERREUR, 'Erreur : ' + monsters[mid].name + ' n\'est plus disponible');
		return false;
	}
	// si on demande le paradoxe, pas besoin de vérifier sa dispo
	if (cid != -1) {
		// recherche de l'index du contrat dans le tableau de verif
		for ( i = 0 ; i < dispo.contracts.length ; i++ ) {
			if (dispo.contracts[i].id == contracts.table[cid].id) {
				cidV = i;
				break;
			}
		}
		// contrat qui n'existe plus
		if (cidV == -1) {
			full_print_add(TYPE_ERREUR, 'Erreur : le contrat ' + print_contrat(cid) + ' n\'existe plus');
			return false;
		}
		// contrat pas dispo
		if (!dispo.contracts[cidV].OK) {
			// mais si le contrat n'est réellement pas affecté on laissera faire
			if (search_monster(cidV) != -1) {
				full_print_add(TYPE_ERREUR, 'Erreur : le contrat ' + print_contrat(cid) + ' est déjà affecté à un monstre');
				return false;
			}
		}
		// contrat dans moins de 15 minutes
		if (dispo.contracts[cidV].countdown < LIMIT_15) {
			full_print_add(TYPE_ERREUR, 'Erreur : le contrat ' + print_contrat(cid) + ' n\'est plus disponible (moins de 15 minutes avant son horaire)');
			return false;
		}
		// monstre fatigué pour le contrat 
		if (!simulation.checked && dispo.contracts[cidV].countdown < (dispo.monsters[midV].fatigue * 3600)) {
			full_print_add(TYPE_ERREUR, 'Erreur : ' + monsters[mid].name + ' ne sera pas reposé pour le contrat ' + print_contrat(cid));
			return false;
		}
		// pas de soucis
	}
	return true;
}

// construit une liste des contrats filtrés
function filtre () {
	wait_for_PDM(function () {
		var start = parseInt(filter_start.value, 10);
		var end = parseInt(filter_end.value, 10);
		var kind = new Array(params.kind_0, params.kind_1, params.kind_2, params.kind_3, params.kind_4, params.kind_5);
		var nb = 0;
		var attente = 0;
		var j = 0;
		var special_in_filter = 0;
		// retour à zéro de la liste de choix
		for ( j = 0 ; j < monsters.length ; j++ ) monsters[j].filtered.splice(0, monsters[j].filtered.length);
		// heure maintenant (pour initialiser nextH et baseS pour les tests à venir)
		compute_time();
		var nextH = (baseH + 1) % 24;
		var next_timezone = time2timezone(nextH);
		// construction de la liste des contrats possibles
		for (var i = 0 ; i < contracts.table.length ; i++ ) {
			// contrat dispo
			if (!contracts.table[i].accepted) {
				// horaire
				attente = timetowait(baseH, contracts.table[i].timezone);
				if (attente > start && attente <= end) {
					// impossible d'affecter un contrat 15 minutes avant l'heure pile si le niveau est supérieur à 9
					if (agency.level < 10 || (contracts.table[i].timezone != next_timezone || (contracts.table[i].timezone == next_timezone && (baseM * 60 + baseS) < LIMIT_45))) {
						// si ce contrat est un spécial, je le signale (seuls les tests d'horaire sont conservés)
						if (special_table.indexOf(i) != -1) special_in_filter++;
						// ville avec PDM ignorées si besoin
						if (!params.intox || check_PDM(i) == NO) {
							// type de contrat
							if (kind[contracts.table[i].kind]) {
								// ici on a un contrat filtré avec tous les critères
								nb++;
								estimate(i);
								for ( j = 0 ; j < monsters.length ; j++ ) {
									// pas monstre MBL
									if (!monsters[j].mbl) {
										// pas ignoré
										if (!monsters[j].contrat.ignore.checked) {
											// en simulation ou pas fatigué et pas occupé
											if (simulation.checked || (attente > monsters[j].fatigue && !monsters[j].busy)) {
												// filtre sur réussite
												if (contracts.table[i].estimation[j].percent_equip >= params.percent) {
													monsters[j].filtered.push(i);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		nb_filtered_div.innerHTML = '&nbsp;<i>(' + nb + ' contrats filtrés, <strong>' + special_in_filter + ' spéci' + pluriel_al(special_in_filter) + ' sur cet horaire</strong>)</i>';
	});
}

// mise à jour des contrats par les pourcentages d'infernal des villes
function update_inferno() {
	var i = 0;
	var j = 0;
	var k = 0;
	var cid = 0;
	for ( i = 0 ; i < monsters.length ; i++ )
		// pas MBL
		if (!monsters[i].mbl)
			// lecture des estimations OK
			for ( j = 0 ; j < monsters[i].filtered.length ; j++) {
				cid = monsters[i].filtered[j];
				// si le contrat n'est pas à jour alors update
				if (contracts.table[cid].infernal < 0) {
					// chercher la ville (il existe des doublons dans les noms => test sur couple ville/pays)
					k = 0;
					do { k++; }
					while (k < cities.infernal.length && (cities.infernal[k].city != contracts.table[cid].city || cities.infernal[k].country != contracts.table[cid].country));
					// mise à jour du pourcentage
					if (k < cities.infernal.length) contracts.table[cid].infernal = cities.infernal[k].percent;
				}
			}
}

// optimisation de la ressource souhaitée
function optimization_choices(criteria) {
	var i;
	var j;
	var temp;
	var nb;
	var cid = 0;
	var money = 0;

	// création de la liste d'adjacence des monstres
	var M_adj = new Array();
	M_adj.length = monsters.length;
	for ( i = 0 ; i < monsters.length ; i++ ) {
		M_adj[i] = new Array();
		M_adj[i].length = Math.max(0, monsters[i].filtered.length);
		for ( j = 0 ; j < monsters[i].filtered.length ; j++ ) {
			M_adj[i][j] = new Object();
			cid = monsters[i].filtered[j];
			M_adj[i][j].cid = cid;
			money = Math.floor(contracts.table[cid].prize * contracts.table[cid].estimation[i].percent_equip / 100) - contracts.table[cid].estimation[i].equip_prix - monsters[i].bounty;
			// ici différence sur le critère
			if (criteria == MONEY) M_adj[i][j].value = money;
			else M_adj[i][j].value = contracts.table[cid].infernal * SCORE_V + money;
		}
	}

	// tri des contrats par rapport aux valeurs en ordre décroissant
	for ( i = 0 ; i < M_adj.length ; i++ )
		if (M_adj[i].length > 1) M_adj[i].sort(sortfunc_value);

	// compter le nombre de monstres qui ont des contrats filtrés et initialiser le tableau des monstres utilisés
	var M_available = new Array();
	M_available.length = monsters.length;
	for ( i = 0 ; i < monsters.length ; i++ ) M_available[i] = false;
	var nb_monsters = 0;
	for ( i = 0 ; i < monsters.length ; i++ )
		if (!monsters[i].mbl) {
			if (M_adj[i].length > 0) {
				nb_monsters++;
				M_available[i] = true;
			}
			else M_available[i] = false;
		}
		else M_available[i] = false;

	// conservation des n meilleurs si n monstres avec des contrats
	for ( i = 0 ; i < M_adj.length ; i++ ) M_adj[i].splice(nb_monsters, Math.max(0, M_adj[i].length - nb_monsters));

	// construire et initialiser le tableau des adjacences des contrats et les dispos
	var nb_contrats = 0;
	var C_bindings = new Array();
	var C_reverse = new Array();
	var C_available = new Array();
	var C_maxvalue_estimation = new Array();
	var C_adj = new Array();
	for ( i = 0 ; i < contracts.table.length ; i++ ) C_bindings.push(-1);
	for ( i = 0 ; i < M_adj.length ; i++ ) {
		for ( j = 0 ; j < M_adj[i].length ; j++ ) {
			cid = M_adj[i][j].cid;
			if (C_bindings[cid] == -1) {
				C_bindings[cid] = nb_contrats;
				C_reverse.push(cid);
				C_available.push(true);
				C_maxvalue_estimation.push(0);
				C_adj.push(new Array());
				nb_contrats++;
			}
			temp = C_bindings[cid];
			nb = C_adj[temp].length;
			C_adj[temp].push(new Object());
			C_adj[temp][nb].mid = i;
			C_adj[temp][nb].madj = j;
		}
	}

	// initialisation des compteurs d'adjacence
	var M_adj_nb = new Array();
	M_adj_nb.length = M_adj.length;
	for ( i = 0 ; i < M_adj.length ; i++ ) M_adj_nb[i] = M_adj[i].length;
	var C_adj_nb = new Array();
	C_adj_nb.length = C_adj.length;
	for ( i = 0 ; i < C_adj.length ; i++ ) C_adj_nb[i] = C_adj[i].length
	// tree = arbre de recherche
	var tree = new Object();
	tree.branch = new Array();
	tree.branch_width = new Array();
	tree.branch_current = new Array();
	for ( i = 0 ; i < nb_monsters ; i++ ) {
		tree.branch.push(new Array());
		// il faut pouvoir mettre tous les contrats et la possibilité de ne pas prendre de contrat
		for ( j = 0 ; j <= nb_contrats ; j++ ) {
			tree.branch[i].push(new Object());
			tree.branch[i][j].mid = -1;
			tree.branch[i][j].cid = -1;
			tree.branch[i][j].cadj = -1;
			tree.branch[i][j].cbind = -1;
		}
		tree.branch_width.push(0);
		tree.branch_current.push(0);
	}
	var current_level = 0;
	// tableau utile pour le calcul de l'heuristique
	var score = new Array();
	score.length = nb_monsters;
	// valeur de la fonction objectif
	var current_value = 0;
	var best_value = 0;
	var useful = false;
	var backtrack = false;
	var BT_level = 0;
	var cadj;
	var cbind;

	// exploration des solutions
	for (;;) {
		// réduction de l'espace de recherche : si un contrat n'est que sur un monstre et en premier : le choisir
		useful = true;
		while (useful) {
			useful = false;
			for ( i = 0 ; i < C_adj.length ; i++ ) {
				// un contrat dans une seule liste de monstres
				if (C_available[i] && C_adj_nb[i] == 1) {
					// trouver le monstre dispo
					temp = 0;
					while (temp < C_adj[i].length && !M_available[C_adj[i][temp].mid]) temp++;
					// monstre dispo trouvé
					if (temp < C_adj[i].length) {
						mid = C_adj[i][temp].mid;
						// trouver le meilleur contrat disponible de ce monstre
						cadj = 0;
						while (!C_available[C_bindings[M_adj[mid][cadj].cid]]) cadj++;
						// si égalité entre le contrat trouvé et le meilleur contrat du monstre, alors...
						if (C_bindings[M_adj[mid][cadj].cid] == i) {
							// possibilité de vérifier encore si une autre propagation à faire
							useful = true;
							// on associe le contrat et le monstre dans l'arbre de recherche
							// monstre et contrat utilisés
							M_available[mid] = false;
							C_available[i] = false;
							// mise à jour de la borne
							current_value += M_adj[mid][cadj].value;
							// mise à jour des listes d'adjacence
							for ( j = 0 ; j < C_adj[i].length ; j++ ) M_adj_nb[C_adj[i][j].mid]--;
							for ( j = 0 ; j < M_adj[mid].length ; j++ ) {
								temp = C_bindings[M_adj[mid][j].cid];
								C_adj_nb[temp]--;
							}
							// ajout dans l'arbre
							tree.branch_current[current_level] = 0;
							tree.branch_width[current_level] = 1;
							tree.branch[current_level][0].mid = mid;
							tree.branch[current_level][0].cid = C_reverse[i];
							tree.branch[current_level][0].cadj = cadj;
							tree.branch[current_level][0].cbind = i;
							// passage au niveau suivant
							current_level++;
							// mise à zéro du niveau suivant
							tree.branch_width[current_level] = 0;
							tree.branch_current[current_level] = 0;
						}
					}
				}
			}
		}

		// 2 cas de backtrack possibles
		backtrack = false;

		// feuille atteinte ?
		temp = 0;
		for ( i = 0 ; i < M_adj.length ; i++ ) if (!M_available[i] || M_adj_nb[i] < 1) temp++;
		// dans une feuille
		if (temp == M_adj.length) {
			backtrack = true;
			// meilleure solution actuelle ?
			if (current_value > best_value) {
				best_value = current_value;
				// copie de la branche courante
//~ debug_txt += '----- NEW BEST value : ' + best_value + ' -----<br>';
				for ( i = 0 ; i < monsters.length ; i++ ) monsters[i].contrat.choix = -1;
				for ( i = 0 ; i < current_level ; i++ ) {
					monsters[tree.branch[i][tree.branch_current[i]].mid].contrat.choix = tree.branch[i][tree.branch_current[i]].cid;
//~ debug_txt += monsters[tree.branch[i][tree.branch_current[i]].mid].name + ' / ' + print_contrat(tree.branch[i][tree.branch_current[i]].cid) + ' : ' + M_adj[tree.branch[i][tree.branch_current[i]].mid][tree.branch[i][tree.branch_current[i]].cadj].value + '(' + contracts.table[tree.branch[i][tree.branch_current[i]].cid].estimation[tree.branch[i][tree.branch_current[i]].mid].equip_prix + ')<br>';
				}
			}
		}

		if (!backtrack) {
			// estimation de la meilleure borne possible
			// nb, le nombre de monstres dispos
			nb = 0;
			for ( i = 0 ; i < M_adj.length ; i++ ) if (M_available[i] && M_adj_nb[i] > 1) nb++;

			// trouver les meilleures valeurs possibles pour les contrats
			for ( i = 0 ; i < C_adj.length ; i++ ) {
				C_maxvalue_estimation[i] = 0;
				if (C_available[i]) {
					for ( j = 0 ; j < C_adj[i].length ; j++ ) {
						mid = C_adj[i][j].mid;
						if (M_available[mid]) {
							temp = M_adj[mid][C_adj[i][j].madj].value;
							if (temp > C_maxvalue_estimation[i]) C_maxvalue_estimation[i] = temp;
						}
					}
				}
			}

			// somme des nb meilleurs contrats possibles
			C_maxvalue_estimation.sort(sortfunc);
			if (nb > C_maxvalue_estimation.length) nb = C_maxvalue_estimation.length;
			temp = 0;
			for ( i = 0 ; i < nb ; i++ ) temp += C_maxvalue_estimation[i];

			// si l'estimation n'est pas meilleure que la meilleure réponse alors backtrack (coupe)
			if (best_value > (current_value + temp)) {
//~ debug_txt += '----- COUPE car estimation : ' + (current_value + temp) + ' -----<br>';
				backtrack = true;
			}
		}

		// backtrack ?
		if (backtrack) {
			// niveau de backtrack
			current_level--;
			BT_level = current_level;
			while (BT_level >= 0 && (tree.branch_current[BT_level]+1) >= tree.branch_width[BT_level]) BT_level--;
			// fin de l'exploration ?
			if (BT_level < 0) {
				break;
			}

			// mise à jour des tableaux
			while (current_level > BT_level) {
				// remise en jeu du contrat et du monstre (niveau plus haut)
				mid = tree.branch[current_level][tree.branch_current[current_level]].mid;
				cid = tree.branch[current_level][tree.branch_current[current_level]].cid;
				cbind = tree.branch[current_level][tree.branch_current[current_level]].cbind;
				cadj = tree.branch[current_level][tree.branch_current[current_level]].cadj;
				// si un contrat était choisi
				if (cid != -1) {
					// mise à jour de la borne
					current_value -= M_adj[mid][cadj].value;
					// monstre et contrat disponibles
					M_available[mid] = true;
					C_available[cbind] = true;
					// mise à jour des listes d'adjacence
					for ( j = 0 ; j < C_adj[cbind].length ; j++ ) M_adj_nb[C_adj[cbind][j].mid]++;
					for ( j = 0 ; j < M_adj[mid].length ; j++ ) {
						temp = C_bindings[M_adj[mid][j].cid];
						C_adj_nb[temp]++;
					}
				}
				// si aucun contrat n'était choisi pour ce monstre
				else {
					// monstre disponible
					M_available[mid] = true;
					// mise à jour des listes d'adjacence
					for ( j = 0 ; j < M_adj[mid].length ; j++ ) {
						temp = C_bindings[M_adj[mid][j].cid];
						C_adj_nb[temp]++;
					}
				}
				// remise à zéro du niveau de l'arbre
				tree.branch_width[current_level] = 0;
				tree.branch_current[current_level] = 0;
				current_level--;
			}

			// remise en jeu du contrat (même niveau)
			mid = tree.branch[current_level][tree.branch_current[current_level]].mid;
			cid = tree.branch[current_level][tree.branch_current[current_level]].cid;
			cbind = tree.branch[current_level][tree.branch_current[current_level]].cbind;
			cadj = tree.branch[current_level][tree.branch_current[current_level]].cadj;
			if (cid != -1) {
				// mise à jour de la borne
				current_value -= M_adj[mid][cadj].value;
				// contrat disponible
				C_available[cbind] = true;
				// mise à jour de la liste d'adjacence du contrat
				for ( j = 0 ; j < C_adj[cbind].length ; j++ ) M_adj_nb[C_adj[cbind][j].mid]++;
			}

			// choix du contrat suivant
			tree.branch_current[current_level]++;
			mid = tree.branch[current_level][tree.branch_current[current_level]].mid;
			cid = tree.branch[current_level][tree.branch_current[current_level]].cid;
			cbind = tree.branch[current_level][tree.branch_current[current_level]].cbind;
			cadj = tree.branch[current_level][tree.branch_current[current_level]].cadj;
			if (cid != -1) {
				// mise à jour de la borne
				current_value += M_adj[mid][cadj].value;
				// contrat utilisé
				C_available[cbind] = false;
				// mise à jour de la liste d'adjacence du contrat
				for ( j = 0 ; j < C_adj[cbind].length ; j++ ) M_adj_nb[C_adj[cbind][j].mid]--;
			}

			// passage au niveau suivant
			current_level++;
			// mise à zéro du niveau suivant
			tree.branch_width[current_level] = 0;
			tree.branch_current[current_level] = 0;
		}
		else {
			// heuristique de branchement : on choisit le monstre qui a le moins de contrats dispos
			// calcul des scores
			for ( i = 0 ; i < M_adj.length ; i++ ) {
				// si monstre dispo
				if (M_available[i]) {
					// meilleur score : longueur min avec plus grosse somme de valeur
					temp = 0;
					nb = 0;
					for ( j = 0 ; j < M_adj[i].length ; j++) {
						if (C_available[C_bindings[M_adj[i][j].cid]]) {
							nb++;
							temp += M_adj[i][j].value;
						}
					}
					if (nb == 0) score[i] = 0;
					else score[i] = (nb_contrats - nb) * SCORE_H + temp;
				}
				else score[i] = 0;
			}
			// choix du monstre : meilleur score
			temp = 0;
			mid = -1;
			for ( i = 0 ; i < score.length ; i++ )
				if (score[i] > temp) {
					temp = score[i];
					mid = i;
				}

			// ajout des contrats dispos dans l'arbre de recherche
			tree.branch_current[current_level] = 0;
			tree.branch_width[current_level] = 0;
			for ( i = 0 ; i < M_adj[mid].length ; i++ ) {
				// si le contrat est dispo
				cid = M_adj[mid][i].cid;
				cbind = C_bindings[cid];
				if (C_available[cbind]) {
					tree.branch[current_level][tree.branch_width[current_level]].mid = mid;
					tree.branch[current_level][tree.branch_width[current_level]].cid = cid;
					tree.branch[current_level][tree.branch_width[current_level]].cbind = cbind;
					tree.branch[current_level][tree.branch_width[current_level]].cadj = i;
					tree.branch_width[current_level]++;
				}
			}
			// ajout du nœud sans choix pour ce monstre
			tree.branch[current_level][tree.branch_width[current_level]].mid = mid;
			tree.branch[current_level][tree.branch_width[current_level]].cid = -1;
			tree.branch[current_level][tree.branch_width[current_level]].cbind = -1;
			tree.branch[current_level][tree.branch_width[current_level]].cadj = -1;
			tree.branch_width[current_level]++;
			// affectation du premier contrat de la liste
			// monstre et contrat utilisés
			M_available[mid] = false;
			cid = tree.branch[current_level][tree.branch_current[current_level]].cid;
			cbind = tree.branch[current_level][tree.branch_current[current_level]].cbind;
			cadj = tree.branch[current_level][tree.branch_current[current_level]].cadj;
			C_available[cbind] = false;
			// mise à jour de la borne
			current_value += M_adj[mid][cadj].value;
			// mise à jour des listes d'adjacence
			for ( j = 0 ; j < C_adj[cbind].length ; j++ ) M_adj_nb[C_adj[cbind][j].mid]--;
			for ( j = 0 ; j < M_adj[mid].length ; j++ ) {
				temp = C_bindings[M_adj[mid][j].cid];
				C_adj_nb[temp]--;
			}
			// passage au niveau suivant
			current_level++;
			// mise à zéro du niveau suivant
			tree.branch_width[current_level] = 0;
			tree.branch_current[current_level] = 0;
		}
	}
//~ var txt ='Arbre\n';
//~ for ( i = 0 ; i < current_level ; i++ ) {
	//~ txt = txt + i + ': |';
	//~ for ( j = 0 ; j < tree.branch_width[i] ; j++ ) {
		//~ if (j == tree.branch_current[i]) txt = txt + ' [' + tree.branch[i][j].mid + '/' + tree.branch[i][j].cid + ']';
		//~ else txt = txt + ' ' + tree.branch[i][j].mid + '/' + tree.branch[i][j].cid;
	//~ }
	//~ txt = txt + ' |\n';
//~ }
//~ alert(txt);
}

function get_cities() {
	cities_OK = false;
	getDOC(GM_info.script.namespace + '/api/cities.xml?name=' + API.name + ';pass=' + API.pass, function (cities_API) {
		if (fuseau(cities_API)) {
			setTimeout(get_cities, 1000);
			return;
		}

		var cities_list = cities_API.getElementsByTagName('city');
		cities.infernal.length = cities_list.length;
		for ( var i = 0 ; i < cities_list.length ; i++ ) {
			cities.infernal[i] = new Object();
			cities.infernal[i].city = cities_list[i].getAttribute('name');
			cities.infernal[i].country = cities_list[i].getAttribute('country');
			cities.infernal[i].percent = parseInt(cities_list[i].getAttribute('percent'), 10);
		}
		cities_OK = true;
	});
}

// optimisation du choix des contrats
function optimize (criteria) {
	wait_for_contracts(function () {
		wait_for_moon(function () {
			wait_for_special(function () {
				// reinit de l'affichage
				reinit_gains();
				valid_div_N.style.display = 'none';
				for ( var i = 0 ; i < monsters.length ; i++ ) {
					monsters[i].contrat.choix = -1;
					if (!monsters[i].mbl) monsters[i].contrat.nb.innerHTML = 0;
					else monsters[i].contrat.nb.innerHTML = '';
					monsters[i].contrat.infernal.innerHTML = '';
					monsters[i].contrat.credits.innerHTML = '';
					monsters[i].contrat.reussite.innerHTML = '';
					monsters[i].contrat.equip.checked = false;
					monsters[i].contrat.equip.style.visibility = 'hidden';
					monsters[i].contrat.affect.checked = false;
					monsters[i].contrat.affect.style.visibility = 'hidden';
				}
				// filtre
				filtre();
				// affichage du nombre de contrats filtrés
				for (var i = 0 ; i < monsters.length ; i++ )
					if (!monsters[i].mbl) monsters[i].contrat.nb.innerHTML = monsters[i].filtered.length;

				wait_for_cities(function () {
					// mise à jour des infernaux car besoin ou pour info
					update_inferno();
					// optimisation réelle
					optimization_choices(criteria);
					// affichage des contrats choisis
					print_choices_gains();
				});
			});
		});
	});
}

// cache les 2 zones de validation
function hide_all_valid() {
	valid_div_N.display = 'none';
	valid_div_S.style.visibility = 'hidden';
}

// validation des choix de contrats normaux
function valider_N() {
	// vérification si il est utile de faire cette fonction
	affect = false;
	for ( var i = 0 ; i < monsters.length ; i++ )
		if (monsters[i].contrat.affect.checked) {
			affect = true;
			break;
		}
	if (!affect) return;

	// confirmation
	if (params.confirm_option_valid_N) if (!confirm('Validez-vous les équipements et les affectations ?')) return;

	// initialisation
	hide_all_valid();

	// affichage
	full_print_start(OK, 'Résultat des équipements et affectations');

	// vérification des dispos
	get_verif()
	wait_for_verif(function () {
		get_sec();
		wait_for_sec(function () {
			// préparer la liste des équipements + vérification de la dispo
			var i = 0;
			var liste = new Array();
			var nb = 0;
			for ( i = 0 ; i < monsters.length ; i++ ) {
				if (!affect) break;
				if (monsters[i].contrat.affect.checked) {
					if (verif_dispo(i, monsters[i].contrat.choix)) {
						if (monsters[i].contrat.equip.checked) {
							liste.push(new Object());
							liste[nb].mid = monsters[i].id;
							liste[nb].liste = contracts.table[monsters[i].contrat.choix].estimation[i].equip;
							nb++;
						}
					}
					else affect = false;
				}
			}

			// erreur bloquante
			if (!affect) {
				full_print_add(TYPE_INFO, '<strong>Aucun contrat n\'a été affecté !</strong>');
				full_print_end('Fermer');
				go();
				return;
			}

			// équiper
			equip_monster_direct(liste);

			wait_for_equipement(function () {
				if (retour_fonction_equipement == KO) {
					full_print_add(TYPE_INFO, '<strong>Aucun contrat n\'a été affecté !</strong>');
					full_print_end('Fermer');
					go();
					return;
				}

				// liste des affectations
				var liste = new Array();
				for ( i = 0 ; i < monsters.length ; i++ )
					if (monsters[i].contrat.affect.checked) liste.push(monsters[i].id);

				// affectations
				affecter_monstres(liste);

				// retour
				wait_for_affectation(function () {
					if (retour_fonction_affectation == KO) {
						full_print_add(TYPE_INFO, '<strong>Processus stoppé, le reste des contrats n\'est pas affecté !</strong>');
						full_print_end('Fermer');
						go();
					}
					else {
						full_print_add(TYPE_RIEN, '');
						full_print_add(TYPE_INFO, '<strong>J\'adore quand un plan se déroule sans accroc !</strong>');
						full_print_end('Fermer');
						go();
					}
				});
			});
		});
	});
}

// affichages dans le tableau
function print_permanent(id) {
	if (monsters[id].mbl) monsters[id].permanent_zone.innerHTML = '-';
	else {
		var liste_txt = '';
		var liste_tip = '<strong>Liste des objets à équiper :</strong><br>';
		var liste = new Array();
		for ( i = 1 ; i < params.equip_watched.length ; i++ ) {
			if (params.equip_watched[i]) {
				if (!verif_item(i, monsters[id].permanentItems)) {
					if (liste.length == 0) liste_txt = equipements.nom[i];
					else liste_txt += ', ' + equipements.nom[i];
					liste_tip += equipements.nom[i] + '<br>';
					liste.push(i);
				}
			}
		}
		if (liste.length == 0) monsters[id].permanent_zone.innerHTML = '';
		else {
			liste_tip += '<br>Ajout de <strong>' + liste.length + '</strong> objet' + pluriel(liste.length);
			var my_span = add_span_func(monsters[id].permanent_zone, 'permanent_monster_' + id, '', '<img src="/gfx/icons/equip.gif" width="16px">', function () {equip_monster_interactif(id, liste, liste_txt);});
			add_tip(my_span, 'Objets permanents manquants', liste_tip);
		}
	}
}
function print_mbl(id) {
	var i;
	var temp;
	// tester si il est utile de proposer un lien
	var liste_monster = monsters[id].permanentItems + monsters[id].contractItems;
	if ((!monsters[id].busy || monsters[id].busy_type != BUSY_MBL) && (!verif_item(20, liste_monster) || !verif_item(21, liste_monster) || !verif_item(22, liste_monster) || !verif_item(31, liste_monster))) {
		// trouver les meilleurs bonus sur chaque caractéristique
		var maxs = new Array(6);
		for ( i = 0 ; i < 6 ; i++ ) {
			maxs[i] = new Object();
			maxs[i].value = 0;
			maxs[i].cost = 0;
			maxs[i].list = new Array();
		}
		var iid = 0;
		var carac_id = 0;
		// sadisme
		maxs[carac_id].value = monsters[id].sadism;
		while (iid < 3) {
			temp = verif_bonus(iid, liste_monster);
			if (temp > 0) {
				maxs[carac_id].value += temp;
				maxs[carac_id].list.push(iid);
				maxs[carac_id].cost += equipements.prix[iid];
			}
			iid++;
		}
		carac_id++;
		// laideur
		maxs[carac_id].value = monsters[id].ugliness;
		while (iid < 6) {
			temp = verif_bonus(iid, liste_monster);
			if (temp > 0) {
				maxs[carac_id].value += temp;
				maxs[carac_id].list.push(iid);
				maxs[carac_id].cost += equipements.prix[iid];
			}
			iid++;
		}
		carac_id++;
		// force
		maxs[carac_id].value = monsters[id].power;
		while (iid < 9) {
			temp = verif_bonus(iid, liste_monster);
			if (temp > 0) {
				maxs[carac_id].value += temp;
				maxs[carac_id].list.push(iid);
				maxs[carac_id].cost += equipements.prix[iid];
			}
			iid++;
		}
		carac_id++;
		// gourmandise
		maxs[carac_id].value = monsters[id].greediness;
		while (iid < 13) {
			temp = verif_bonus(iid, liste_monster);
			if (temp > 0) {
				maxs[carac_id].value += temp;
				maxs[carac_id].list.push(iid);
				maxs[carac_id].cost += equipements.prix[iid];
			}
			iid++;
		}
		carac_id++;
		// contrôle
		maxs[carac_id].value = monsters[id].control;
		while (iid < 16) {
			temp = verif_bonus(iid, liste_monster);
			if (temp > 0) {
				maxs[carac_id].value += temp;
				maxs[carac_id].list.push(iid);
				maxs[carac_id].cost += equipements.prix[iid];
			}
			iid++;
		}
		carac_id++;
		// endurance (je ne me permets pas de mettre des objets d'endurance) / endurance, carac_id == 5
		maxs[carac_id].value = monsters[id].endurance;

		// trouver la meilleure combinaison au meilleur prix
		var j;
		var fight = monsters[id].fight;
		var dopage = 0;
		var cost = 0;
		var best = new Object();
		best.value = fight;
		best.cost = 0;
		best.id1 = -1;
		best.id2 = -1;
		for ( i = 0 ; i < 6 ; i++ ) {
			for ( j = i+1 ; j < 6 ; j++ ) {
				// impossible de doper si fight fait partie des deux meilleures caracs
				// bug dans CM : si fight et endurance sont égaux alors le dopage est impossible même si une troisième carac est meilleure ("fight déjà dans les deux meilleures caracs")
				if ((i != 5 && j != 5 && maxs[i].value >= fight && maxs[j].value >= fight) || (i == 5 && maxs[i].value > fight && maxs[j].value >= fight) || (maxs[i].value >= fight && j == 5 && maxs[j].value > fight)) {
					dopage = Math.round((fight + maxs[i].value + maxs[j].value) / 3);
					cost = maxs[i].cost + maxs[j].cost;
					if (dopage > best.value || (dopage == best.value && cost < best.cost)) {
						best.value = dopage;
						best.cost = cost;
						best.id1 = i;
						best.id2 = j;
					}
				}
			}
		}

		// créer la liste des items à équiper
		var liste_tip = '<strong>Liste des objets à équiper :</strong><br>';
		var liste = new Array();
		var liste_txt = '';
		if (best.id1 != -1 && best.id2 != -1) {
			var nom;
			for ( i = 0 ; i < maxs[best.id1].list.length ; i++ ) {
				nom = equipements.nom[maxs[best.id1].list[i]];
				if (liste.length == 0) liste_txt = nom;
				else liste_txt += ', ' + nom;
				liste_tip += nom + '<br>';
				liste.push(maxs[best.id1].list[i]);
			}
			for ( i = 0 ; i < maxs[best.id2].list.length ; i++ ) {
				nom = equipements.nom[maxs[best.id2].list[i]];
				if (liste.length == 0) liste_txt = nom;
				else liste_txt += ', ' + nom;
				liste_tip += nom + '<br>';
				liste.push(maxs[best.id2].list[i]);
			}
			// dopage si nécessaire
			if (liste.length > 0) {
				liste_tip += equipements.nom[31] + '<br>';
				liste_txt += ', ' + equipements.nom[31];
				liste.push(31);
				best.cost += equipements.prix[31];
			}
			// objets de combat
			for ( i = 20 ; i < 23 ; i++ ) {
				temp = verif_bonus(i, liste_monster);
				if (temp > 0) {
					best.value += temp;
					if (liste.length == 0) liste_txt = equipements.nom[i];
					else liste_txt += ', ' + equipements.nom[i];
					liste_tip += equipements.nom[i] + '<br>';
					liste.push(i);
					best.cost += equipements.prix[i];
				}
			}

			// affichage du résultat
			liste_tip += '<br>Valeur possible de <img src=\\\'/gfx/icons/fight.gif\\\' width=\\\'13\\\'> : <strong>' + best.value + '</strong> (+' + (best.value - fight) + ')';
			liste_tip += '<br>Coût : ' + best.cost + ' <img src=\\\'/gfx/forum/miniMoney.gif\\\' width=\\\'13\\\'>';
			liste_tip += '<br>Valeur actuelle de <img src=\\\'/gfx/icons/endurance.gif\\\' width=\\\'13\\\'> : <strong>' + monsters[id].endurance + '</strong>';
		}
		else {
			// objets de combat
			for ( i = 20 ; i < 23 ; i++ ) {
				temp = verif_bonus(i, liste_monster);
				if (temp > 0) {
					best.value += temp;
					if (liste.length == 0) liste_txt = equipements.nom[i];
					else liste_txt += ', ' + equipements.nom[i];
					liste_tip += equipements.nom[i] + '<br>';
					liste.push(i);
					best.cost += equipements.prix[i];
				}
			}

			// affichage du résultat
			liste_tip += '<br>Valeur possible de <img src=\\\'/gfx/icons/fight.gif\\\' width=\\\'13\\\'> : <strong>' + best.value + '</strong> (+' + (best.value - fight) + ')';
			liste_tip += '<br>Coût : ' + best.cost + ' <img src=\\\'/gfx/forum/miniMoney.gif\\\' width=\\\'13\\\'>';
			liste_tip += '<br>Valeur actuelle de <img src=\\\'/gfx/icons/endurance.gif\\\' width=\\\'13\\\'> : <strong>' + monsters[id].endurance + '</strong>';
		}

		// ajout pour l'endurance mais pas fait automatiquement
		endurance = 0;
		temp = verif_bonus(16, liste_monster);
		if (temp > 0 && stock[16] > 0) endurance += temp;
		temp = verif_bonus(17, liste_monster);
		if (temp > 0 && stock[17] > 0) endurance += temp;
		if (endurance > 0)
			liste_tip += '<br><br><strong>Pour information</strong><br><img src=\\\'/gfx/icons/endurance.gif\\\' width=\\\'13\\\'> +' + endurance + ' : faites-le manuellement !';

		// ajout du bouton
		var my_span = add_span_func(monsters[id].mbl_zone, 'mbl_monster_' + id, '', '<img src="/gfx/forum/fight.gif" width="12px">', function () { equip_monster_mbl(id, liste, liste_txt); });
		add_tip(my_span, 'Amélioration de <img src=\\\'/gfx/icons/fight.gif\\\' width=\\\'13\\\'>', liste_tip);
	}
	else monsters[id].mbl_zone.innerHTML = '';
}
function print_fatigue(id) {
	var hdispo = (baseH + monsters[id].fatigue + 1) % 24;
	var jdispo = Math.floor(monsters[id].fatigue / 24);
	var jdispo_txt = '';
	if (jdispo > 0) jdispo_txt = ' (+' + jdispo + ' jour' + pluriel(jdispo) + ')';
	if (monsters[id].fatigue != 0) monsters[id].fatigue_zone.innerHTML = '<span title="' + monsters[id].name + ' sera disponible pour le fuseau de ' + hdispo + 'h' + jdispo_txt + '">' + monsters[id].fatigue + 'h</span>';
}
function print_busy(id) {
	var texte = '';

	if (monsters[id].busy) {
		switch (monsters[id].busy_type) {
			case BUSY_CONTRAT:
				texte = print_link_accepted(search_contract(monsters[id].busy_value), id, 'Contrat');
				break;
			case BUSY_ESCORTE:
				texte = '<a href="' + GM_info.script.namespace + '/syndicate/' + agency.syndicateId + '/contracts">Escorte</a>';
				texte = 'Escorte';
				break;
			case BUSY_ATTAQUE:
				//~ texte = '<a href="' + GM_info.script.namespace + '/mbl/' + monsters[id].busy_value + '">Attaque</a>';
				texte = 'Attaque';
				break;
			case BUSY_RACKET:
				//~ texte = '<a href="' + GM_info.script.namespace + '/??/' + monsters[id].busy_value + '">Racket</a>';
				texte = 'Racket';
				break;
			case BUSY_PROPAGANDE:
				//~ texte = '<a href="' + GM_info.script.namespace + '/??/' + monsters[id].busy_value + '">Propagande</a>';
				texte = 'Propagande';
				break;
			case BUSY_MBL:
				texte = '<a href="' + GM_info.script.namespace + '/mbl/' + monsters[id].busy_value + '">Match MBL</a>';
				break;
			default: texte = 'Occupé';
		}
	}
	else if (monsters[id].mbl) texte = 'Monstre MBL';
	monsters[id].busy_zone.innerHTML = texte;
}

// ajoute une ligne pour un monstre
function add_monster_line(id) {
	var my_tr = add_text(monsters_body, 'tr', 'monster_' + id, '', '');
	var odd = id % 2;
	if (odd) my_tr.setAttribute('class', 'false');
	else my_tr.setAttribute('class', 'true');
	// nom
	monsters[id].name_zone = add_text(my_tr, 'td', 'name_monster_' + id, '', '');
	// équipement permanent
	monsters[id].permanent_zone = add_text(my_tr, 'td', 'permanent_monster_' + id, '', '');
	// équipement MBL
	monsters[id].mbl_zone = add_text(my_tr, 'td', 'mbl_monster_' + id, '', '');
	// occupation
	monsters[id].busy_zone = add_text(my_tr, 'td', 'dispo_monster_' + id, '', '');
	// astreinte
	monsters[id].astreinte_zone = add_text(my_tr, 'td', 'astreinte_monster_' + id, '', '');
	// fatigue
	monsters[id].fatigue_zone = add_text(my_tr, 'td', 'fatigue_monster_' + id, '', '');
	// contrat
	var my_td = add_text(my_tr, 'td', 'ignorer_monster_' + id, '', '');
	monsters[id].contrat.ignore = add_input_checkbox(my_td, 'ignorer_monster' + id, false);
	if (monsters[id].mbl) monsters[id].contrat.ignore.style.visibility = 'hidden';
	monsters[id].contrat.nb = add_text(my_tr, 'td', 'nb_filtered_monster_' + id, '', '');
	monsters[id].contrat.infernal = add_text(my_tr, 'td', 'infernal_monster_' + id, '', '');
	monsters[id].contrat.credits = add_text(my_tr, 'td', 'credits_monster_' + id, '', '');
	monsters[id].contrat.reussite = add_text(my_tr, 'td', 'reussite_monster_' + id, '', '');
	my_td = add_text(my_tr, 'td', 'equiper_monster_' + id, '', '');
	monsters[id].contrat.equip = add_input_checkbox(my_td, 'equiper_monster_' + id, false);
	monsters[id].contrat.equip.style.visibility = 'hidden';
	my_td = add_text(my_tr, 'td', 'affecter_monster' + id, '', '');
	monsters[id].contrat.affect = add_input_checkbox(my_td, 'affecter_monster' + id, false);
	monsters[id].contrat.affect.style.visibility = 'hidden';
}

// remplit une ligne par les infos d'un monstre
function fill_monster(id) {
	// nom et lien
	var my_link = add_link(monsters[id].name_zone, 'monster_' + id, monsters[id].name, GM_info.script.namespace + '/monster/' + monsters[id].id);
	add_tip_monster(my_link, id);
	// équipement permanent
	print_permanent(id);
	// équipement pour la MBL
	print_mbl(id);
	// occupation
	print_busy(id);
	// astreinte
	if (monsters[id].watchSpas >= 0) monsters[id].astreinte_zone.innerHTML = monsters[id].watchSpas + ' <img src="/gfx/icons/spa_traumal.gif" alt="spa" width="16px">';
	// fatigue
	print_fatigue(id);
}

// rend un monstre occupé
function make_busy(id, type, value) {
	monsters[id].busy = true;
	monsters[id].busy_type = type;
	monsters[id].busy_value = parseInt(value, 10);
}

// réinitialise le tableau des gains
function reinit_gains() {
	gains.nb.innerHTML = '';
	gains.percent.innerHTML = '';
	gains.money.innerHTML = '';
	gains.esperance.innerHTML = '';
	gains.table.style.display = 'none';
}

// ajoute la table des monstres et des contrats normaux
function add_monster_table() {
	// création du tableau
	var t = document.createElement('table');
	t.setAttribute('id', GM_info.script.name + '_table_monsters');
	t.setAttribute('style', 'width:900px;');
	var c = t.createCaption();
	c.innerHTML = 'État des monstres et affectations des contrats normaux';
	var t_head = add_text(t, 'thead', 'monsters', '', '');
	var my_tr = add_text(t_head, 'tr', 'monsters', '', '');
	add_text(my_tr, 'th', 'monsters_nom', '', 'Monstre');
	add_text(my_tr, 'th', 'monsters_equip_permanent', '', '<img src="/gfx/forum/icon_chart.gif" style="margin-top:4px;" title="Équipement" alt="Équipement" width="16px">');
	add_text(my_tr, 'th', 'monsters_equip_mbl', '', 'MBL');
	add_text(my_tr, 'th', 'monsters_dispo', '', 'Occupation');
	add_text(my_tr, 'th', 'monsters_astreinte', '', '<img src="/gfx/icons/reveil.gif" style="margin-top:4px;" title="Astreinte" alt="Astreinte" width="16px">');
	add_text(my_tr, 'th', 'monsters_fatigue', '', '<img src="/gfx/forum/time.gif" style="margin-top:4px;" title="Fatigue" alt="Fatigue" width="16px">');
	add_text(my_tr, 'th', 'monsters_ignore', '', '<img src="/gfx/forum/icon_cross.gif" style="margin-top:4px;" title="Ignorer un monstre - l\'optimisation doit être relancée" alt="Ignorer" width="16px">');
	add_text(my_tr, 'th', 'monsters_nb_filtered', '', '#');
	add_text(my_tr, 'th', 'monsters_pourcentage_infernal', '', '% inf.');
	add_text(my_tr, 'th', 'monsters_prix_contrat', '', '<img src="/gfx/forum/miniMoney.gif" style="margin-top:4px;" title="Récompense" alt="Récompense" width="16px">');
	add_text(my_tr, 'th', 'monsters_reussite', '', 'Réussite et infos');
	add_text(my_tr, 'th', 'monsters_equiper', '', '<img src="/gfx/icons/equip.gif" style="margin-top:4px;" title="Équiper" alt="Équiper" width="16px">');
	add_text(my_tr, 'th', 'monsters_affecter', '', '<img src="/gfx/icons/ok.gif" style="margin-top:4px;" title="Affecter" alt="Affecter" width="16px">');
	monsters_body = add_text(t, 'tbody', 'body_monsters', '', '');
	monsters_body.setAttribute('style', 'text-align:center;');
	// création et remplissage des lignes
	wait_for_contracts_and_monsters(function () {
		wait_for_stock(function () {
			for ( var i = 0 ; i < monsters.length ; i++ ) add_monster_line(i);
			for ( var i = 0 ; i < monsters.length ; i++ ) fill_monster(i);
			// ajout du tableau
			contracts_div.appendChild(t);
			// création du tableau des gains
			gains.table = document.createElement('table');
			gains.table.setAttribute('id', GM_info.script.name + '_table_gains');
			gains.table.setAttribute('style', 'width:480px;margin:0px 0px 0px 210px');
			var c = gains.table.createCaption();
			c.innerHTML = 'Gains des contrats proposés';
			add_func_button(c, 'update_gains', 'Mise à jour', update_gains, 'Mise à jour des gains en fonction des équipements et des contrats cochés');
			var t_head = add_text(gains.table, 'thead', 'gains', '', '');
			var my_tr = add_text(t_head, 'tr', 'gains_head', '', '');
			var my_th = add_text(my_tr, 'th', 'gains_nb', '', 'Monstres affectés');
			my_th.setAttribute('style', 'width:100px;');
			my_th = add_text(my_tr, 'th', 'gains_percent', '', '% Réussite');
			my_th.setAttribute('style', 'width:80px;');
			my_th = add_text(my_tr, 'th', 'gains_money', '', 'Gains <img src="/gfx/forum/miniMoney.gif" title="Gains" alt="Gains" width="16px">');
			my_th.setAttribute('style', 'width:150px;');
			my_th = add_text(my_tr, 'th', 'gains_eperance', '', 'Espérance <img src="/gfx/forum/miniMoney.gif" title="Gains" alt="Gains" width="16px">');
			my_th.setAttribute('style', 'width:150px;');
			var t_body = add_text(gains.table, 'tbody', 'gains_body', '', '');
			t_body.setAttribute('style', 'text-align:center;');
			my_tr = add_text(t_body, 'tr', 'gains_infos', '', '');
			my_tr.setAttribute('class', 'false');
			gains.nb = add_text(my_tr, 'td', 'gains_nb', '', '');
			gains.percent = add_text(my_tr, 'td', 'gains_percent', '', '');
			gains.money = add_text(my_tr, 'td', 'gains_money', '', '');
			gains.esperance = add_text(my_tr, 'td', 'gains_eperance', '', '');
			gains.table.style.display = 'none';
			contracts_div.appendChild(gains.table);
			// bouton de validation des contrats
			valid_div_N = add_text(contracts_div, 'div', 'valid', '', '');
			valid_div_N.setAttribute('style', 'text-align:center;');
			add_func_button(valid_div_N, 'valid', 'Valider les équipements et les contrats cochés !', valider_N, 'Les monstres à équiper ne seront équipés que si ils sont affectés à un contrat');
			valid_div_N.style.display = 'none';
			add_text(contracts_div, 'div', 'space', '', '<br>');
		});
	});
}

// récupère la liste des monstres
function get_monsters() {
	monsters_OK = false;
	getDOC(GM_info.script.namespace + '/api/monsters.xml?name=' + API.name + ';pass=' + API.pass, function (monsters_API) {
		if (fuseau(monsters_API)) {
			setTimeout(get_monsters, 1000);
			return;
		}

		var monsters_list = monsters_API.getElementsByTagName('monster');
		var temp;
		nb_tired = 0;
		var i;
		for ( i = 0 ; i < monsters_list.length ; i++ ) {
			if (monsters.length <= i) {
				monsters[i] = new Object();
				monsters[i].filtered = new Array();
				monsters[i].contrat = new Object();
			}
			// infos
			monsters[i].id = monsters_list[i].getAttribute('id');
			reverse_monsters[monsters[i].id] = i;
			monsters[i].name = monsters_list[i].getAttribute('name');
			monsters[i].bounty = parseInt(monsters_list[i].getAttribute('bounty'), 10);
			monsters[i].fatigue = parseInt(monsters_list[i].getAttribute('fatigue'), 10);
			if (monsters[i].fatigue > 0) nb_tired++;
			monsters[i].permanentItems = ','+monsters_list[i].getAttribute('permanentItems')+',';
			monsters[i].contractItems = ','+monsters_list[i].getAttribute('contractItems')+',';
			monsters[i].sadism = parseInt(monsters_list[i].getAttribute('sadism'), 10);
			monsters[i].ugliness = parseInt(monsters_list[i].getAttribute('ugliness'), 10);
			monsters[i].power = parseInt(monsters_list[i].getAttribute('power'), 10);
			monsters[i].greediness = parseInt(monsters_list[i].getAttribute('greediness'), 10);
			monsters[i].control = parseInt(monsters_list[i].getAttribute('control'), 10);
			monsters[i].fight = parseInt(monsters_list[i].getAttribute('fight'), 10);
			monsters[i].endurance = parseInt(monsters_list[i].getAttribute('endurance'), 10);
			monsters[i].successes = parseInt(monsters_list[i].getAttribute('successes'), 10);
			monsters[i].failures = parseInt(monsters_list[i].getAttribute('failures'), 10);
			monsters[i].firePrize = parseInt(monsters_list[i].getAttribute('firePrize'), 10);
			// bonus
			monsters[i].sadism_bonus = verif_bonus(0, monsters[i].contractItems);
			monsters[i].ugliness_bonus = verif_bonus(3, monsters[i].contractItems);
			monsters[i].power_bonus = verif_bonus(6, monsters[i].contractItems);
			monsters[i].greediness_bonus = verif_bonus(9, monsters[i].contractItems);
			monsters[i].greediness_bonus += verif_bonus(10, monsters[i].contractItems);
			monsters[i].control_bonus = verif_bonus(13, monsters[i].contractItems);
			// astreinte
			monsters[i].watchSpas = monsters_list[i].getAttribute('watchSpas');
			if (monsters[i].watchSpas == null) monsters[i].watchSpas = -1;
			else monsters[i].watchSpas = parseInt(monsters[i].watchSpas, 10);
			// MBL
			temp = monsters_list[i].getAttribute('isMblMonster');
			if (temp != null) monsters[i].mbl = true;
			else monsters[i].mbl = false;
			// occupation
			temp = monsters_list[i].getAttribute('contract');
			if (temp != null) make_busy(i, BUSY_CONTRAT, temp);
			else {
				temp = monsters_list[i].getAttribute('escort');
				if (temp != null) make_busy(i, BUSY_ESCORTE, temp);
				else {
					temp = monsters_list[i].getAttribute('attack');
					if (temp != null) make_busy(i, BUSY_ATTAQUE, temp);
					else {
						temp = monsters_list[i].getAttribute('racket');
						if (temp != null) make_busy(i, BUSY_RACKET, temp)
						else {
							temp = monsters_list[i].getAttribute('propaganda');
							if (temp != null) make_busy(i, BUSY_PROPAGANDE, temp);
							else {
								temp = monsters_list[i].getAttribute('match');
								if (temp != null) make_busy(i, BUSY_MBL, temp);
								else {
									monsters[i].busy = false;
									monsters[i].busy_type = BUSY_NONE;
									monsters[i].busy_value = 0;
								}
							}
						}
					}
				}
			}
		}
		// on enlève les monstres en trop
		for ( ; i < monsters.length ; i++ ) monsters.pop();
		// verrou libéré dans la fonction dessous ! Ne pas déplacer !
		get_images();
	});
}

// fonction qui va chercher les images des monstres
function get_images() {
	getDOC(GM_info.script.namespace + '/monster', function (monsters_page) {
		if (fuseau(monsters_page)) {
			setTimeout(get_images, 1000);
			return;
		}

		var temp;
		var pattern = /\'mface\', \'.*\'/;
		var numbers = /[0-9].*[0-9]/;
		for (var i = 0 ; i < monsters.length ; i++ ) {
			temp = monsters_page.getElementById('monster' + monsters[i].id);
			if (temp != null) {
				temp = temp.innerHTML;
				temp = pattern.exec(temp);
				temp = numbers.exec(temp);
				monsters[i].image = '<embed id=\\\'myMonster' + monsters[i].id + '\\\' width=\\\'150\\\' height=\\\'170\\\' flashvars=\\\'mface=' + temp + '&size=1\\\' menu=\\\'false\\\' wmode=\\\'transparent\\\' quality=\\\'high\\\' bgcolor=\\\'F9F7D2\\\' name=\\\'myMonster' + monsters[i].id + '\\\' style=\\\'undefined\\\' src=\\\'http://data.croquemonster.com/swf/generator.swf?v=18\\\' type=\\\'application/x-shockwave-flash\\\'>';
			}
		}

		monsters_OK = true;
	});
}

// recherche des villes sur lesquelles une boîte à pets est posée
function get_prout() {
	prout_OK = false;
	getDOC(GM_info.script.namespace + '/api/syndicate.xml?id=' + agency.syndicateId + ';user=' + API.name + ';pass=' + API.pass, function (prout_API) {
		if (fuseau(prout_API)) {
			setTimeout(get_prout, 1000);
			return;
		}

		var agences_list = prout_API.getElementsByTagName('agency');
		cities.prout.splice(0, cities.prout.length);
		var nb_prouts = 0;
		var j = 0;
		var box;
		for ( var i = 0 ; i < agences_list.length ; i++ ) {
			box = agences_list[i].getAttribute('fartBox');
			if (box != null) {
				j = 0;
				while (j < cities.prout.length && cities.prout[j].city != box) j++;
				if (j == cities.prout.length) {
					cities.prout.push(new Object());
					cities.prout[nb_prouts].agency = '<strong>' + agences_list[i].getAttribute('name') + '</strong>';
					cities.prout[nb_prouts].city = box;
					if (box == 'Birmingham') {
						var USA = 0;
						var UK = 0;
						for ( j = 0 ; j < cities.PDM.length ; j++ ) {
							if (cities.PDM[j].city == 'Birmingham' && cities.PDM[j].country == 'Royaume-Uni') UK = cities.PDM[j].points;
							if (cities.PDM[j].city == 'Birmingham' && cities.PDM[j].country == 'Etats-Unis') USA = cities.PDM[j].points;
						}
						cities.prout[nb_prouts].PDM = 'Cas spécial de Birmingham ! Vérifiez le pays : ' + USA + ' aux USA et ' + UK + ' au RU';
					}
					else {
						cities.prout[nb_prouts].PDM = 0;
						for ( j = 0 ; j < cities.PDM.length ; j++ )
							if (cities.prout[nb_prouts].city == cities.PDM[j].city) cities.prout[nb_prouts].PDM = cities.PDM[j].points;
					}
					nb_prouts++;
				}
				else cities.prout[j].agency += ', <strong>' + agences_list[i].getAttribute('name') + '</strong>';
			}
		}
		prout_OK = true;
	});
}

// vérifie si un contrat est dans le tableau des villes à boîtes à pets
function check_prout(cid) {
	for (var i = 0 ; i < cities.prout.length ; i++ ) if (cities.prout[i].city == contracts.table[cid].city) return i;
	return -1;
}

// recherche des villes sur lesquelles des PDM sont mis
function get_PDM() {
	PDM.OK = false;
	getDOC(GM_info.script.namespace + '/syndicate/' + agency.syndicateId + '/cities?page=' + PDM.page, function (PDM_page_lue) {
		if (fuseau(PDM_page_lue)) {
			setTimeout(get_PDM, 1000);
			return;
		}

		var div = PDM_page_lue.getElementById('content');
		var trs = div.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
		if (PDM.page == 1) cities.PDM.splice(0, cities.PDM.length);
		var j = cities.PDM.length;
		for ( var i = 0 ; i < trs.length ; i++ ) {
			tds = trs[i].getElementsByTagName('td');
			cities.PDM.push(new Object());
			cities.PDM[j].city = tds[2].getElementsByTagName('a')[0].innerHTML;
			cities.PDM[j].country = tds[3].innerHTML;
			cities.PDM[j].points = tds[6].innerHTML;
			j++;
		}

		// dernière page des PDM : stop, sinon on continue la lecture
		var temp = div.getElementsByClassName('next_disable');
		if (temp.length > 0) PDM.OK = true;
		else {
			PDM.page++;
			get_PDM();
		}
	});
}

// vérifie si un contrat est dans le tableau des villes à PDM
function check_PDM(cid) {
	for (var i = 0 ; i < cities.PDM.length ; i++ )
		if (cities.PDM[i].city == contracts.table[cid].city && cities.PDM[i].country == contracts.table[cid].country)
			return i;
	return NO;
}

// validation des choix de contrats spéciaux
function valider_S() {
	// vérification si il est utile de faire cette fonction
	affect = false;
	var i = 0;
	var j = 0;
	for ( i = 0 ; i < monsters.length ; i++ ) {
		if (affect) break;
		for ( j = 0 ; j < specials.length ; j++ )
			if (monsters[i].special[j].checked) {
				affect = true;
				break;
			}
	}
	if (!affect) return;

	// confirmation
	if (params.confirm_option_valid_S)
		if (!confirm('Validez-vous les équipements et les affectations aux contrats spéciaux ?')) return;

	// initialisation
	hide_all_valid();

	// affichage
	full_print_start(OK, 'Résultat des équipements et affectations des contrats spéciaux');

	var nb = 0;
	// test sur les contrats
	for ( i = 0 ; i < specials.length ; i++ ) {
		if (!affect) break;
		nb = 0;
		for ( j = 0 ; j < monsters.length ; j++ ) if (monsters[j].special[i].checked) nb++;
		// trop de monstres sur un contrat
		if (nb > 1) affect = false;
	}
	if (affect) {
		// test sur les monstres
		for ( i = 0 ; i < monsters.length ; i++ ) {
			if (!affect) break;
			nb = 0;
			for ( j = 0 ; j < specials.length ; j++ ) if (monsters[i].special[j].checked) nb++;
			// trop de contrats pour un monstre
			if (nb > 1) affect = false;
		}
		if (affect) {
			// init des valeurs car tout semble OK
			paradox.chosen = 0;
			for ( i = 0 ; i < monsters.length ; i++ ) {
				monsters[i].special_checked = false;
				monsters[i].contrat.special = false;
				for ( j = 0 ; j < specials.length ; j++ ) {
					if (monsters[i].special[j].checked) {
						monsters[i].special_checked = true;
						// cas du paradoxe
						if (specials[j] == 'paradox') {
							paradox.chosen = monsters[i].id;
							monsters[i].contrat.choix = -1;
						}
						else monsters[i].contrat.choix = specials[j];
						break;
					}
				}
			}
		}
	}

	// erreur bloquante
	if (!affect) {
		full_print_add(TYPE_ERREUR, 'Attention : un monstre maximum par contrat et un contrat maximum par monstre');
		full_print_add(TYPE_INFO, '<strong>Aucun contrat spécial n\'a été affecté !</strong>');
		full_print_end('Fermer');
		go();
		return;
	}

	// vérification des dispos
	get_verif()
	wait_for_verif(function () {
		get_sec();
		wait_for_sec(function () {
			// préparer la liste des équipements + vérification de la dispo
			var i = 0;
			var liste = new Array();
			var nb = 0;
			for ( i = 0 ; i < monsters.length ; i++ ) {
				if (!affect) break;
				// le monstre est affecté à un spécial
				if (monsters[i].special_checked) {
					// cas spécial pour le paradoxe
					if (monsters[i].contrat.choix != -1) {
						if (verif_dispo(i, monsters[i].contrat.choix)) {
							if (monsters[i].equip_special.checked && contracts.table[monsters[i].contrat.choix].estimation[i].percent != 100) {
								liste.push(new Object());
								liste[nb].mid = monsters[i].id;
								liste[nb].liste = contracts.table[monsters[i].contrat.choix].estimation[i].equip;
								nb++;
							}
						}
						else affect = false;
					}
					else if (!verif_dispo(i, -1)) affect = false;
				}
			}

			// erreur bloquante
			if (!affect) {
				full_print_add(TYPE_INFO, '<strong>Aucun contrat spécial n\'a été affecté !</strong>');
				full_print_end('Fermer');
				go();
				return;
			}

			// équiper
			equip_monster_direct(liste);

			wait_for_equipement(function () {
				if (retour_fonction_equipement == KO) {
					full_print_add(TYPE_INFO, '<strong>Aucun contrat spécial n\'a été affecté !</strong>');
					full_print_end('Fermer');
					go();
					return;
				}

				// liste des affectations
				var liste = new Array();
				for ( var i = 0 ; i < monsters.length ; i++ )
					if (monsters[i].special_checked)
						// cas spécial du paradoxe
						if (monsters[i].contrat.choix != -1) liste.push(monsters[i].id);

				// affectations
				affecter_monstres(liste);

				// retour
				wait_for_affectation(function () {
					if (retour_fonction_affectation == KO) {
						full_print_add(TYPE_INFO, '<strong>Processus stoppé, les contrats restants ne sont pas affectés !</strong>');
						full_print_end('Fermer');
						go();
					}
					else {
						// lancement du paradoxe si besoin
						paradox.done = true;
						paradox.retour = OK;
						if (paradox.chosen > 0) paradox_go()
						wait_for_paradox(function () {
							if (paradox.retour == OK) {
								full_print_add(TYPE_RIEN, '');
								full_print_add(TYPE_INFO, '<strong>Tous les contrats spéciaux ont été affectés</strong>');
							}
							full_print_end('Fermer');
							go();
						});
					}
				});
			});
		});
	});
}

// effectue le paradoxe
function paradox_go() {
	paradox.done = false;
	getDOC(GM_info.script.namespace + '/paradox/start?id=' + paradox.chosen + ';' + sec_var, function (paradox_API) {
		if (fuseau(paradox_API)) {
			setTimeout(paradox_go, 1000);
			return;
		}

		getDOC(GM_info.script.namespace + '/paradox/result', function (paradox_API) {
			if (fuseau(paradox_API)) {
				full_print_add(TYPE_ERREUR, 'Fuseau sur le paradoxe, vérifiez vous-même si le paradoxe a eu lieu');
				paradox.retour = KO;
			}
			else {
				var erreur = paradox_API.getElementById('refreshWarning');
				if (erreur != null) {
					full_print_add(TYPE_ERREUR, 'Erreur sur le paradoxe, vérifiez vous-même si le paradoxe a eu lieu');
					paradox.retour = KO;
				}
				else {
					paradox.retour = OK;
					var retour = paradox_API.getElementById('validationBox');
					if (retour == null) full_print_add(TYPE_ERREUR, 'Erreur inconnue sur le paradoxe, vérifiez vous-même si le paradoxe a eu lieu');
					else full_print_add(TYPE_RIEN, retour.getElementsByTagName('div')[4].innerHTML);
				}
			}
			paradox.done = true;
		});
	});
}

// ajoute le paradoxe
function add_paradox(line) {
	var my_tr = add_text(special_body, 'tr', 'special_' + line, '', '');
	var odd = line % 2;
	if (odd) my_tr.setAttribute('class', 'false');
	else my_tr.setAttribute('class', 'true');
	// trouver le monstre le moins cher
	var min_bounty = 100000;
	for (var i = 0 ; i < monsters.length ; i++ ) {
		if (HAIDAO || !monsters[i].mbl) {
			if (!monsters[i].busy && monsters[i].fatigue == 0) {
				if (monsters[i].bounty < min_bounty) min_bounty = monsters[i].bounty;
			}
		}
	}
	add_text(my_tr, 'td', 'type_' + line, '', '<img src="/gfx/forum/time.gif" width="16px">&nbsp;Paradoxe');
	var my_td = 0;
	var bounty;
	for (var i = 0 ; i < monsters.length ; i++ ) {
		if (HAIDAO || !monsters[i].mbl) {
			if (monsters[i].bounty == min_bounty) bounty = '<strong>' + monsters[i].bounty + '</strong>';
			else bounty = '' + monsters[i].bounty + '';
			my_td = add_text(my_tr, 'td', 'special_' + i + line, '', bounty + '&nbsp;<img src="/gfx/forum/miniMoney.gif" width="12px"><br>');
			if (!monsters[i].busy) {
				if (monsters[i].fatigue == 0) {
					add_text(my_td, 'span', 'special_' + i + line, '', '<img src="/gfx/icons/ok.gif" title="Affecter" alt="Affecter" width="16px">');
					monsters[i].special[line] = add_input_checkbox(my_td, 'special_' + i + line, false);
				}
				else {
					add_text(my_td, 'span', 'special_' + i + line, '', '<img src="/gfx/forum/time.gif" title="Fatigué" alt="Fatigué" width="16px">');
					monsters[i].special[line] = new Object();
					monsters[i].special[line].checked = false;
				}
			}
			else {
				add_text(my_td, 'span', 'special_' + i + line, '', '<img src="/gfx/forum/icon_cross.gif" title="Indisponible" alt="Indisponible" width="16px">');
				monsters[i].special[line] = new Object();
				monsters[i].special[line].checked = false;
			}
		}
		else {
			monsters[i].special[line] = new Object();
			monsters[i].special[line].checked = false;
		}
	}
}

// ajoute une ligne dans le tableau des spéciaux
function add_special_line(line, cid, titre, attention) {
	var my_tr = add_text(special_body, 'tr', 'special_' + line, '', '');
	var odd = line % 2;
	if (odd) my_tr.setAttribute('class', 'false');
	else my_tr.setAttribute('class', 'true');
	var attente = timetowait(baseH, contracts.table[cid].timezone);
	add_text(my_tr, 'td', 'type_' + line, '', '' + titre +  '<br>' + print_link_contract(cid, contracts.table[cid].city + ' (+' + attente + 'h)') + attention);
	special_table.push(cid);
	estimate(cid);
	var my_td = 0;
	for (var i = 0 ; i < monsters.length ; i++ ) {
		if (!monsters[i].mbl) {
			my_td = add_text(my_tr, 'td', 'special_' + i + line, '', print_link_estimation(cid, i, print_estimation_special(cid, i)) + '<br>');
			if (!monsters[i].busy) {
				if (monsters[i].fatigue < attente) {
					add_text(my_td, 'span', 'special_' + i + line, '', '<img src="/gfx/icons/ok.gif" title="Affecter" alt="Affecter" width="16px">');
					monsters[i].special[line] = add_input_checkbox(my_td, 'special_' + i + line, false);
				}
				else {
					add_text(my_td, 'span', 'special_' + i + line, '', '<img src="/gfx/forum/time.gif" title="Fatigué ' + (monsters[i].fatigue - attente) + 'h de trop" alt="Fatigué" width="16px">');
					monsters[i].special[line] = new Object();
					monsters[i].special[line].checked = false;
				}
			}
			else {
				add_text(my_td, 'span', 'special_' + i + line, '', '<img src="/gfx/forum/icon_cross.gif" title="Indisponible" alt="Indisponible" width="16px">');
				monsters[i].special[line] = new Object();
				monsters[i].special[line].checked = false;
			}
		}
		else {
			if (HAIDAO) {
				add_text(my_tr, 'td', 'special_' + i + line, '', '<img src="/gfx/forum/icon_cross.gif" title="Indisponible" alt="Indisponible" width="16px">');
			}
			monsters[i].special[line] = new Object();
			monsters[i].special[line].checked = false;
		}
	}
}

// ajoute une explication sur le paradoxe
function add_special_empty(line, nb, titre, texte) {
	var my_tr = add_text(special_body, 'tr', 'special_' + line, '', '');
	var odd = line % 2;
	if (odd) my_tr.setAttribute('class', 'false');
	else my_tr.setAttribute('class', 'true');
	add_text(my_tr, 'td', 'type_' + line, '', titre);
	var my_td = add_text(my_tr, 'td', 'txt_' + line, '', texte);
	my_td.setAttribute('colspan', nb);
	for (var i = 0 ; i < monsters.length ; i++ ) {
		monsters[i].special[line] = new Object();
		monsters[i].special[line].checked = false;
	}
}

// ajoute la table des contrats spéciaux
function add_special_table() {
	special_OK = false;
	wait_for_contracts_and_monsters(function () {
		wait_for_prout(function () {
			wait_for_moon(function () {
				// reinit du tableau des spéciaux
				special_table.splice(0, special_table.length);
				// entête du tableau
				var t = document.createElement('table');
				t.setAttribute('id', GM_info.script.name + '_table_special');
				t.setAttribute('style', 'width:900px;');
				var c = t.createCaption();
				c.innerHTML = 'Contrats spéciaux';
				var t_head = add_text(t, 'thead', 'special', '', '');
				var my_tr = add_text(t_head, 'tr', 'special', '', '');
				add_text(my_tr, 'th', 'contract_name', '', 'Contrat');
				// init pour plein de choses
				var txt = '';
				// monstres pour les contrats
				var nb_monsters_OK = 0;
				var my_th = 0;
				for ( var i = 0 ; i < monsters.length ; i++ ) {
					monsters[i].special = new Array();
					if (HAIDAO || !monsters[i].mbl) {
						if (!monsters[i].mbl && !monsters[i].busy) {
							my_th = add_text(my_tr, 'th', 'monster_' + i, '', monsters[i].name + '<br><img src="/gfx/icons/equip.gif" title="Équiper" alt="Équiper" width="16px">');
							monsters[i].equip_special = add_input_checkbox(my_th, 'equip_special_' + i, true);
						}
						else {
							add_text(my_tr, 'th', 'monster_' + i, '', monsters[i].name);
							monsters[i].equip_special = new Object();
							monsters[i].equip_special.checked = false;
						}
						nb_monsters_OK++;
					}
				}
				// corps du tableau
				special_body = add_text(t, 'tbody', 'body_special', '', '');
				special_body.setAttribute('style', 'text-align:center;');
				specials.splice(0, specials.length);
				// paradoxe
				if (params.paradox) {
					if (contracts.paradox) {
						add_paradox(specials.length);
						specials.push('paradox');
					}
					else {
						if (contracts.paradox_date != null) {
							txt = '<strong>Prêt à ' + print_2digits(contracts.paradox_date.getHours()) + 'h' + print_2digits(contracts.paradox_date.getMinutes()) + ' le ' + print_2digits(contracts.paradox_date.getDate()) + '/' + print_2digits((contracts.paradox_date.getMonth()+1)) + '/' + print_2digits(contracts.paradox_date.getFullYear()) + '</strong>';
							add_special_empty(specials.length, nb_monsters_OK, '<img src="/gfx/forum/time.gif" width="16px">&nbsp;Paradoxe', txt);
							specials.push('paradox');
						}
					}
				}
				if (params.dangers || params.infernaux || params.prout || params.CO2 || params.divers) {
					// autres : dangers, infernaux, villes CO2 et divers (lutins, boyscouts, ...)
					var list_danger = new Array();
					var list_infernaux = new Array();
					var list_prout = new Array();
					var list_CO2 = new Array();
					var list_saisonniers = new Array();
					var city1 = /Engloutown/;
					var city2 = /Hororgoréale/;
					var city3 = /Squiiick/;
					var season1 = /Houlala/;
					var season2 = /Mioche/;
					var season3 = /sucre/;
					var season4 = /sonnette/;
					var season5 = /Même/;
					var season6 = /scouts/;
					var season7 = /lutin/;
					var danger;
					for ( var i = 0 ; i < contracts.table.length ; i++ ) {
						danger = false;
						if (params.dangers && contracts.table[i].accepted && search_monster(contracts.table[i].id) == -1) {
							danger = true;
							list_danger.push(i);
						}
						if (!contracts.table[i].accepted || danger) {
							if (params.infernaux && contracts.table[i].kind == KIND_INFERNAL) list_infernaux.push(i);
							if (params.CO2 && (city1.test(contracts.table[i].city) || city2.test(contracts.table[i].city) || city3.test(contracts.table[i].city))) list_CO2.push(i);
							if (params.divers && (season1.test(contracts.table[i].name) || season2.test(contracts.table[i].name) || season3.test(contracts.table[i].name) || season4.test(contracts.table[i].name) || season5.test(contracts.table[i].name) || season6.test(contracts.table[i].name) || season7.test(contracts.table[i].name))) list_saisonniers.push(i);
							if (params.prout && check_prout(i) != -1) list_prout.push(i);
						}
					}
					// variables pour afficher des cas particuliers
					txt = '';
					var other_infernal = -1;
					var other_prout = -1;
					var other_CO2 = -1;
					var other_saison = -1;
					var virgule;
					// Contrats acceptés sans monstre affecté
					if (params.dangers) {
						if (list_danger.length > 0) {
							for ( var i = 0 ; i < list_danger.length ; i++ ) {
								other_infernal = list_infernaux.indexOf(list_danger[i]);
								other_prout = list_prout.indexOf(list_danger[i]);
								other_CO2 = list_CO2.indexOf(list_danger[i]);
								other_saison = list_saisonniers.indexOf(list_danger[i]);
								txt = '';
								if (other_infernal != -1 || other_prout != -1 || other_CO2 != -1 || other_saison != -1) {
									virgule = false;
									txt = '<br>lire&nbsp;<img src="/gfx/forum/icon_exclaim.gif" alt="!" title="Ce contrat choisi mais sans monstre est aussi';
									if (other_infernal != -1) {
										list_infernaux.splice(other_infernal, 1);
										if (virgule) txt += ',';
										txt += ' un infernal';
										virgule = true;
									}
									if (other_prout != -1) {
										list_prout.splice(other_prout, 1);
										if (virgule) txt += ',';
										txt += ' sur une ville avec une boîte à pets installée';
										virgule = true;
									}
									if (other_CO2 != -1) {
										list_CO2.splice(other_CO2, 1);
										if (virgule) txt += ',';
										txt += ' sur une ville à CO2';
										virgule = true;
									}
									if (other_saison != -1) {
										list_saisonniers.splice(other_saison, 1);
										if (virgule) txt += ',';
										txt += ' un contrat saisonnier';
										virgule = true;
									}
									txt += '"  width="16px">';
								}
								add_special_line(specials.length, list_danger[i], '<img src="/gfx/forum/dislike.gif" width="16px">&nbsp;Danger', txt);
								specials.push(list_danger[i]);
							}
						}
						else {
							add_special_empty(specials.length, nb_monsters_OK, '<img src="/gfx/forum/dislike.gif" width="16px">&nbsp;Dangers', '<i>Aucun contrat accepté sans monstre affecté, ouf !</i>');
							specials.push('infernal');
						}
					}
					// Infernaux
					if (params.infernaux) {
						if (list_infernaux.length > 0) {
							for ( var i = 0 ; i < list_infernaux.length ; i++ ) {
								other_prout = list_prout.indexOf(list_infernaux[i]);
								other_CO2 = list_CO2.indexOf(list_infernaux[i]);
								other_saison = list_saisonniers.indexOf(list_infernaux[i]);
								txt = '';
								if (other_prout != -1 || other_CO2 != -1 || other_saison != -1) {
									virgule = false;
									txt = '<br>lire&nbsp;<img src="/gfx/forum/icon_exclaim.gif" alt="!" title="Cet infernal est aussi';
									if (other_prout != -1) {
										list_prout.splice(other_prout, 1);
										if (virgule) txt += ',';
										txt += ' sur une ville avec une boîte à pets installée';
										virgule = true;
									}
									if (other_CO2 != -1) {
										list_CO2.splice(other_CO2, 1);
										if (virgule) txt += ',';
										txt += ' sur une ville à CO2';
										virgule = true;
									}
									if (other_saison != -1) {
										list_saisonniers.splice(other_saison, 1);
										if (virgule) txt += ',';
										txt += ' un contrat saisonnier';
										virgule = true;
									}
									txt += '"  width="16px">';
								}
								add_special_line(specials.length, list_infernaux[i], '<img src="/gfx/contract5.gif" width="16px">&nbsp;Infernal', txt);
								specials.push(list_infernaux[i]);
							}
						}
						else {
							add_special_empty(specials.length, nb_monsters_OK, '<img src="/gfx/contract5.gif" width="16px">&nbsp;Infernaux', '<i>Aucun contrat infernal trouvé</i>');
							specials.push('infernal');
						}
					}
					// Villes à boîtes à pets
					if (params.prout) {
						if (list_prout.length > 0) {
							for ( var i = 0 ; i < list_prout.length ; i++ ) {
								other_CO2 = list_CO2.indexOf(list_prout[i]);
								other_saison = list_saisonniers.indexOf(list_prout[i]);
								txt = '';
								if (other_CO2 != -1 || other_saison != -1) {
									virgule = false;
									txt = '<br>lire&nbsp;<img src="/gfx/forum/icon_exclaim.gif" alt="!" title="Ce contrat sur une ville avec boîte à pets installée est aussi';
									if (other_CO2 != -1) {
										list_CO2.splice(other_CO2, 1);
										if (virgule) txt += ',';
										txt += ' sur une ville à CO2';
										virgule = true;
									}
									if (other_saison != -1) {
										list_saisonniers.splice(other_saison, 1);
										if (virgule) txt += ',';
										txt += ' un contrat saisonnier';
										virgule = true;
									}
									txt += '"  width="16px">';
								}
								var ID_prout = check_prout(list_prout[i]);
								add_special_line(specials.length, list_prout[i], 'Ville avec <img src="/gfx/icons/fartbox.gif" width="16px" onmouseout="Tip.hide();" onmouseover="Tip.show(null,\'Boîte(s) à pets de ' + cities.prout[ID_prout].agency + ' sur ' + cities.prout[ID_prout].city + '<br>PDM sur cette ville : <strong>' + cities.prout[ID_prout].PDM + '</strong>\',null,event);">', txt);
								specials.push(list_prout[i]);
							}
						}
						else {
							add_special_empty(specials.length, nb_monsters_OK, 'Villes avec <img src="/gfx/icons/fartbox.gif" width="16px">', '<i>Aucun contrat sur les villes avec boîte à pets détectée</i>');
							specials.push('prout');
						}
					}
					// Villes à CO2
					if (params.CO2) {
						if (list_CO2.length > 0) {
							for ( var i = 0 ; i < list_CO2.length ; i++ ) {
								other_saison = list_saisonniers.indexOf(list_prout[i]);
								txt = '';
								if (other_saison != -1) {
									list_saisonniers.splice(other_saison, 1);
									txt = '<br>lire&nbsp;<img src="/gfx/forum/icon_exclaim.gif" alt="!" title="Ce contrat sur ville à CO2 est aussi un contrat saisonnier" width="16px">';
								}
								add_special_line(specials.length, list_CO2[i], '<img src="/gfx/icons/smallco2.gif" width="16px">&nbsp;Ville CO<span style="font-size:10px;">2</span>', txt);
								specials.push(list_CO2[i]);
							}
						}
						else {
							add_special_empty(specials.length, nb_monsters_OK, '<img src="/gfx/icons/smallco2.gif" width="16px">&nbsp;Villes CO<span style="font-size:10px;">2</span>', '<i>Aucun contrat trouvé sur les villes à CO<sub>2</sub></i>');
							specials.push('CO2');
						}
					}
					// Contrats saisonniers
					if (params.divers) {
						if (list_saisonniers.length > 0) {
							for ( var i = 0 ; i < list_saisonniers.length ; i++ ) {
								add_special_line(specials.length, list_saisonniers[i], 'Divers', txt);
								specials.push(list_saisonniers[i]);
							}
						}
						else {
							add_special_empty(specials.length, nb_monsters_OK, 'Divers', '<i>Aucun contrat saisonnier trouvé</i>');
							specials.push('saison');
						}
					}
				}
				special_div.appendChild(t);
				valid_div_S = add_text(special_div, 'div', 'valid', '', '');
				valid_div_S.setAttribute('style', 'text-align:center;');
				add_func_button(valid_div_S, 'valid', 'Valider les équipements et les contrats SPÉCIAUX cochés !', valider_S, 'Les monstres à équiper ne seront équipés que si ils sont affectés à un contrat');
				valid_div_S.style.visibility = 'visible';
				add_text(special_div, 'div', 'space', '', '<br>');
				special_OK = true;
			});
		});
	});
}

// retourne le numéro de mois : court -> int-1
function replace_month(str) {
	if (str == 'janv' || str == 'jan') return 0;
	if (str == 'févr' || str == 'fév') return 1;
	if (str == 'mars' || str == 'mar') return 2;
	if (str == 'avril' || str == 'avr') return 3;
	if (str == 'mai') return 4;
	if (str == 'juin' || str == 'jun') return 5;
	if (str == 'juil' || str == 'jui') return 6;
	if (str == 'août' || str == 'aoû') return 7;
	if (str == 'sept' || str == 'sep') return 8;
	if (str == 'oct') return 9;
	if (str == 'nov') return 10;
	return 11;
}

// retourne le numéro de mois : long -> int
function replace_month_long(str) {
	if (str == 'janvier') return 0;
	if (str == 'février') return 1;
	if (str == 'mars') return 2;
	if (str == 'avril') return 3;
	if (str == 'mai') return 4;
	if (str == 'juin') return 5;
	if (str == 'juillet') return 6;
	if (str == 'août') return 7;
	if (str == 'septembre') return 8;
	if (str == 'octobre') return 9;
	if (str == 'novembre') return 10;
	return 11;
}

// affiche le résumé
function print_histo() {
	var i;
	var j;
	var temp;
	if (histo.defense.nb + histo.success.nb + histo.failure.nb > 0) {
		full_print_start(OK, 'Résumé de l\'historique');
		if (histo.defense.nb > 0) {
			full_print_add(TYPE_INFO, 'Nombre de <strong>défenses de tas</strong> : ' + histo.defense.nb);
			full_print_add(TYPE_INFO, 'Coût total de la défense : <span class="smallGold">' + histo.defense.money + '</span>');
			full_print_add(TYPE_INFO, 'Détails :');
			for ( i = 0 ; i < histo.defense.details.length ; i++ )
				full_print_add(TYPE_RIEN, '&nbsp;&nbsp;&nbsp;' + histo.defense.details[i]);
			full_print_add(TYPE_RIEN, '');
		}
		if (histo.missedinfernal.nb > 0) {
			full_print_add(TYPE_INFO, 'Nombre d\'<strong>infernaux pas acceptés</strong> : ' + histo.missedinfernal.nb);
			temp = '';
			for ( i = 0 ; i < histo.missedinfernal.reputation.length ; i++ ) {
				if (temp == '') temp = histo.missedinfernal.reputation[i];
				else temp += ', ' + histo.missedinfernal.reputation[i];
			}
			full_print_add(TYPE_INFO, 'Réputation perdue : ' + temp);
			full_print_add(TYPE_RIEN, '');
		}
		if (histo.failure.nb > 0) {
			full_print_add(TYPE_INFO, 'Nombre de <strong>contrats ratés</strong> : ' + histo.failure.nb);
			full_print_add(TYPE_INFO, 'Totalité des pertes : <span class="smallGold">' + histo.failure.money + '</span>');
			if (histo.failure.paradox) temp = '<span class="reputation">0</span> (paradoxe)';
			else temp = '';
			for ( i = 0 ; i < histo.failure.reputation.length ; i++ ) {
				if (temp == '') temp = histo.failure.reputation[i];
				else temp += ', ' + histo.failure.reputation[i];
			}
			full_print_add(TYPE_INFO, 'Réputation perdue : ' + temp);
			full_print_add(TYPE_RIEN, '');
		}
		if (histo.success.nb > 0) {
			full_print_add(TYPE_INFO, 'Nombre de <strong>contrats réussis</strong> : ' + histo.success.nb);
			full_print_add(TYPE_INFO, 'Totalité des gains : <span class="smallGold">' + histo.success.money + '</span>');
			if (histo.success.paradox > 0) temp = '<span class="reputation">' + histo.success.paradox + '</span> (paradoxe)';
			else temp = '';
			for ( i = 0 ; i < histo.success.reputation.length ; i++ ) {
				if (temp == '') temp = histo.success.reputation[i];
				else temp += ', ' + histo.success.reputation[i];
			}
			full_print_add(TYPE_INFO, 'Réputation gagnée : ' + temp);
			if (histo.found_items.length == 0) {
				full_print_add(TYPE_INFO, 'Aucun objet trouvé');
			}
			else {
				full_print_add(TYPE_INFO, 'Objets trouvés');
				temp = ''
				for ( i = 0 ; i < histo.found_items.length ; i++ )
					temp += histo.found_items[i].item + print_nb_img(histo.found_items[i].nb) + ' ';
				full_print_add(TYPE_RIEN, temp);
			}
			full_print_add(TYPE_RIEN, '');
		}
		if (histo.events.length > 0) {
			full_print_add(TYPE_INFO, 'Événements particuliers :');
			for ( i = 0 ; i < histo.events.length ; i++ )
				full_print_add(TYPE_RIEN, '&nbsp;&nbsp;&nbsp;' + histo.events[i]);
			full_print_add(TYPE_RIEN, '');
		}
		full_print_end('Continuer');
	}
}

// ajoute un objet dans un tableau
function add_item_histo(table, item) {
	var i = 0;
	while (i < table.length && table[i].item != item) i++;

	if (i == table.length) {
		table.push(new Object());
		table[i].item = item;
		table[i].nb = 1;
	}
	else table[i].nb++;
}

// lecture d'une page de l'historique
function read_histo_one_page() {
	getDOC(GM_info.script.namespace + '/user/history?page=' + histo.page + ';', function (histo_page) {
		if (fuseau(histo_page)) {
			setTimeout(read_histo_one_page, 1000);
			return;
		}

		var my_class;
		var moneys;
		var items;
		var image;
		var lis;
		var date_lue;
		var date_tab;
		var date;
		var temp1;
		var temp2;
		var i;
		var j;
		var my_div = histo_page.getElementById('content');
		var all = my_div.getElementsByClassName('history');
		var contractSuccess = /contractSuccess/;
		var contractFailure = /contractFailure/;
		var contractMissedInfernal = /contract/;
		var automiam = /automiam/;
		var paradoxe = /paradoxal/;
		var reputation = /class="reputation">.*<\/span>.*!/;
		var reputation_MissedInfernal = /class="reputIcon">.*<\/span>.*!/;
		var defense = /inflige.*GrosMiam/;
		var monstre_pattern = /<strong.*strong>/;
		var numbers = /[0-9]+/;
		var bravo = /Bravo/;
		var progress = /<strong.*progresser.*point.*<\/strong>/;
		var lost_item = /.*problèmes.*fuite.*perdu.*route/;
		var tired = /.*sale mioche.*fatigue supplémentaires/;
		for ( i = 0 ; i < all.length ; i++ ) {
			date_lue = all[i].getElementsByClassName('date');
			// sortie si date pas connue
			if (date_lue.length == 0) {
				histo.read_more = false;
				break;
			}
			// sortie si date trop vieille
			date_tab = date_lue[0].innerHTML.replace(/\./g, '').split(' ');
			date = new Date(parseInt(date_tab[3], 10), replace_month(date_tab[2]), parseInt(date_tab[1], 10), parseInt(date_tab[5].slice(0, date_tab[5].indexOf(':')), 10), parseInt(date_tab[5].slice(date_tab[5].indexOf(':')+1, date_tab[5].length), 10), 0, 0);
			if (date < params.histo_date) {
				histo.read_more = false;
				break;
			}
			// sinon, lecture
			my_class = all[i].getAttribute('class');
			lis = all[i].getElementsByTagName('li');
			if (automiam.test(my_class)) {
				// détails de la défense si possible
				temp1 = all[i].getElementsByClassName('smallGold');
				if (temp1.length > 0) {
					histo.defense.nb++;
					histo.defense.money += parseInt(temp1[0].innerHTML.replace('.', ''), 10);
					temp1 = lis[0].innerHTML;
					temp2 = monstre_pattern.exec(temp1);
					if (bravo.test(temp1)) temp1 = 'arrive à distraire GrosMiam';
					else temp1 = defense.exec(temp1);
					histo.defense.details.push(temp2 + ' ' + temp1);
				}
			}
			else {
				if (contractSuccess.test(my_class)) {
					histo.success.nb++;
					temp1 = lis[0].innerHTML;
					// réputation
					if (paradoxe.test(temp1)) {
						temp1 = all[i].getElementsByTagName('li')[0].getElementsByClassName('reputation')[0].innerHTML;
						histo.success.paradox += parseInt(temp1, 10);
					}
					else {
						temp2 = '<span ' + reputation.exec(temp1);
						temp2 = temp2.slice(0, temp2.length-2);
						histo.success.reputation.push(temp2);
					}
					// argent (20/20 aussi)
					moneys = all[i].getElementsByClassName('smallGold');
					for ( j = 0 ; j < moneys.length ; j++ )
						histo.success.money += parseInt(moneys[j].innerHTML.replace('.', ''), 10);
					// objets
					temp1 = all[i].getElementsByClassName('objects');
					if (temp1.length > 0) {
						items = temp1[0].getElementsByTagName('img');
						for ( j = 0 ; j < items.length ; j++ ) {
							image = '<img alt="' + items[j].getAttribute('alt') + '" src="' + items[j].getAttribute('src') + '">';
							add_item_histo(histo.found_items, image);
						}
					}
				}
				else {
					if (contractFailure.test(my_class)) {
						histo.failure.nb++;
						// réputation
						temp1 = lis[0].innerHTML;
						if (paradoxe.test(temp1)) histo.failure.paradox = true;
						else {
							temp2 = '<span ' + reputation.exec(temp1);
							temp2 = temp2.slice(0, temp2.length-2);
							// argent
							histo.failure.money += parseInt(all[i].getElementsByClassName('smallGold')[0].innerHTML.replace('.', ''), 10);
							histo.failure.reputation.push(temp2);
						}
					}
					else {
						if (contractMissedInfernal.test(my_class)) {
							// réputation
							temp1 = all[i].getElementsByClassName('txt')[0].innerHTML;
							temp2 = '<span ' + reputation_MissedInfernal.exec(temp1);
							temp2 = temp2.slice(0, temp2.length-2);
							// astuce pour ne pas voir de bug si j'ai mal détecté un infernal pas pris
							if (temp2.length > 0) {
								histo.missedinfernal.nb++;
								histo.missedinfernal.reputation.push(temp2);
							}
						}
					}
				}
				for ( j = 1 ; j < lis.length ; j++ ) {
					temp1 = lis[j].innerHTML;
					if (progress.test(temp1)) {
						histo.events.push('<strong>' + temp1.slice(0, temp1.indexOf(' vient de progresser')) + '</strong>' + temp1.slice(temp1.indexOf(' gagne'), temp1.length-1));
					}
					else {
						if (lost_item.test(temp1)){
							histo.events.push('<strong>' + temp1.slice(0, temp1.indexOf(' a eu quelques problèmes')) + '</strong> a ' + temp1.slice(temp1.indexOf('perdu'), temp1.indexOf('en route')));
						}
						else {
							if (tired.test(temp1)) {
								histo.events.push('<strong>' + temp1.slice(0, temp1.indexOf(' est tombé sur un sale mioche')) + '</strong> : ' + numbers.exec(temp1) + 'h de fatigue supplémentaires');
							}
						}
					}
				}
			}
		}

		// dernière page de l'historique : stop
		temp = my_div.getElementsByClassName('next_disable');
		if (temp.length > 0) histo.read_more = false;

		if (histo.read_more) {
			histo.page++;
			read_histo_one_page();
		}
		else {
			reinit_param_date();
			wait_for_full_print(function () { print_histo(); });
		}
	});
}

// lecture de l'historique
function read_histo() {
	// défense de tas
	histo.defense = new Object();
	histo.defense.nb = 0;
	histo.defense.money = 0;
	histo.defense.details = new Array();
	// contrats réussis
	histo.success = new Object();
	histo.success.nb = 0;
	histo.success.money = 0;
	histo.success.paradox = 0;
	histo.success.reputation = new Array();
	// contrats ratés
	histo.failure = new Object();
	histo.failure.nb = 0;
	histo.failure.money = 0;
	histo.failure.paradox = false;
	histo.failure.reputation = new Array();
	// infernaux pas acceptés
	histo.missedinfernal = new Object();
	histo.missedinfernal.nb = 0;
	histo.missedinfernal.reputation = new Array();
	// divers pour les contrats
	histo.found_items = new Array();
	histo.events = new Array();
	// gestion de la lecture
	histo.page = 1;
	histo.read_more = true;
	read_histo_one_page();
}

// réinitialisation du paramètre de la date de lecture de l'historique
function reinit_param_date() {
	params.histo_date = new Date();
	params_input.histo_date.year.value = params.histo_date.getFullYear();
	params_input.histo_date.month.value = params.histo_date.getMonth()+1;
	params_input.histo_date.day.value = params.histo_date.getDate();
	params_input.histo_date.H.value = params.histo_date.getHours();
	params_input.histo_date.M.value = params.histo_date.getMinutes();
	GM_setValue('params_' + agency.id, '(' + JSON.stringify(params) + ')');
}

// initialisation des paramètres de configuration aux valeurs par défaut
function init_params() {
	params.percent = 60;
	// types de contrats
	params.kind_0 = true;
	params.kind_1 = true;
	params.kind_2 = true;
	params.kind_3 = true;
	params.kind_4 = true;
	params.kind_5 = true;
	// éviter les villes intoxiquées
	params.intox = false;
	// spéciaux à afficher
	params.paradox = true;
	params.dangers = true;
	params.infernaux = true;
	params.prout = true;
	params.CO2 = true;
	params.divers = true;
	// équipements permanents à surveiller
	for (var i = 0 ; i < params.equip_watched.length ; i++) params.equip_watched[i] = false;
	// stock à construire
	for (var i = 0 ; i < params.stock.length ; i++) params.stock[i] = 0;
	// gestion des confirmations
	params.confirm_option_equip = true;
	params.confirm_option_mbl = true;
	params.confirm_option_crado = true;
	params.confirm_option_contracts = true;
	params.confirm_option_valid_N = true;
	params.confirm_option_valid_S = true;
	// gestion des raccourcis
	for (var i = 0 ; i < NB_LINKS ; i++) {
		params.shortcuts[i] = new Object();
		params.shortcuts[i].name = '';
		params.shortcuts[i].link = '';
	}
	// gestion des fichiers externes
	params.read_histo = false;
	params.histo_date = new Date();
	params.check_forums = false;
	params.read_mbl = false;
}
// lecture des paramètres de configuration
function get_params() {
	init_params();
	// récupération des données sauvegardées
	var temp_params = GM_getValue('params_' + agency.id, 'null');
	// lecture si possible
	if (temp_params != 'null') params = eval(temp_params);
	params.histo_date = new Date(params.histo_date);
}
// Écriture des paramètres de configuration
function clean_input(x) {
	var temp = parseInt(x.value, 10);
	if (isNaN(temp)) temp = 0;
	temp = Math.min(Math.max(0, temp), 100);
	if (temp != x.value) x.value = temp;
	return temp;
}
function clean_date(year, month, day, H, M) {
	// maintenant
	now = new Date();
	// année
	var temp = parseInt(year.value, 10);
	if (isNaN(temp)) temp = now.getYear();
	if (temp != year.value) year.value = temp;
	// mois
	temp = parseInt(month.value, 10);
	if (isNaN(temp) || temp < 1 || temp > 12) temp = now.getMonth()+1;
	if (temp != month.value) month.value = temp;
	// jour
	temp = parseInt(day.value, 10);
	if (isNaN(temp) || temp < 1 || temp > 31) temp = now.getDate();
	if (temp != day.value) day.value = temp;
	// heure
	temp = parseInt(H.value, 10);
	if (isNaN(temp) || temp < 0 || temp > 23) temp = now.getHours();
	if (temp != H.value) H.value = temp;
	// minute
	temp = parseInt(M.value, 10);
	if (isNaN(temp) || temp < 0 || temp > 59) temp = now.getMinutes();
	if (temp != M.value) M.value = temp;

	// construction
	date = new Date(year.value, month.value-1, day.value, H.value, M.value);

	return date;
}
function set_params() {
	if (!confirm('Les paramètres sauvegardés vont être écrasés. Confirmez-vous ?')) return;
	// pourcentage
	params.percent = clean_input(params_input.percent);
	// types de contrats
	params.kind_0 = params_input.kind_0.checked;
	params.kind_1 = params_input.kind_1.checked;
	params.kind_2 = params_input.kind_2.checked;
	params.kind_3 = params_input.kind_3.checked;
	params.kind_4 = params_input.kind_4.checked;
	params.kind_5 = params_input.kind_5.checked;
	// éviter les villes intoxiquées
	params.intox = params_input.intox.checked;
	// spéciaux à afficher
	params.paradox = params_input.paradox.checked;
	params.dangers = params_input.dangers.checked;
	params.infernaux = params_input.infernaux.checked;
	params.prout = params_input.prout.checked;
	params.CO2 = params_input.CO2.checked;
	params.divers = params_input.divers.checked;
	// équipements permanents à surveiller
	params.equip_watched[1] = params_input.equipS1.checked;
	params.equip_watched[2] = params_input.equipS2.checked;
	params.equip_watched[4] = params_input.equipL1.checked;
	params.equip_watched[5] = params_input.equipL2.checked;
	params.equip_watched[7] = params_input.equipF1.checked;
	params.equip_watched[8] = params_input.equipF2.checked;
	params.equip_watched[11] = params_input.equipG1.checked;
	params.equip_watched[12] = params_input.equipG2.checked;
	params.equip_watched[14] = params_input.equipC1.checked;
	params.equip_watched[15] = params_input.equipC2.checked;
	params.equip_watched[16] = params_input.equipE1.checked;
	params.equip_watched[17] = params_input.equipE2.checked;
	params.equip_watched[21] = params_input.equipB1.checked;
	params.equip_watched[22] = params_input.equipB2.checked;
	params.equip_watched[23] = params_input.equipMV.checked;
	params.equip_watched[27] = params_input.equipCZ.checked;
	params.equip_watched[28] = params_input.equipCL.checked;
	params.equip_watched[29] = params_input.equipPM.checked;
	// stock
	params.stock[0] = clean_input(params_input.stockS1T);
	params.stock[1] = clean_input(params_input.stockS1P);
	params.stock[2] = clean_input(params_input.stockS2P);
	params.stock[3] = clean_input(params_input.stockL1T);
	params.stock[4] = clean_input(params_input.stockL1P);
	params.stock[5] = clean_input(params_input.stockL2P);
	params.stock[6] = clean_input(params_input.stockF1T);
	params.stock[7] = clean_input(params_input.stockF1P);
	params.stock[8] = clean_input(params_input.stockF2P);
	params.stock[9] = clean_input(params_input.stockG1T);
	params.stock[10] = clean_input(params_input.stockG2T);
	params.stock[11] = clean_input(params_input.stockG1P);
	params.stock[12] = clean_input(params_input.stockG2P);
	params.stock[13] = clean_input(params_input.stockC1T);
	params.stock[14] = clean_input(params_input.stockC1P);
	params.stock[15] = clean_input(params_input.stockC2P);
	params.stock[16] = 0;
	params.stock[17] = 0;
	params.stock[18] = 0;
	params.stock[19] = 0;
	params.stock[20] = clean_input(params_input.stockB1T);
	params.stock[21] = clean_input(params_input.stockB1P);
	params.stock[22] = clean_input(params_input.stockB2P);
	params.stock[23] = clean_input(params_input.stockMV);
	params.stock[24] = clean_input(params_input.stockTP);
	params.stock[25] = clean_input(params_input.stockDP);
	params.stock[26] = clean_input(params_input.stockRM);
	params.stock[27] = 0;
	params.stock[28] = 0;
	params.stock[29] = clean_input(params_input.stockPM);
	params.stock[30] = 0;
	params.stock[31] = clean_input(params_input.stockMBL);
	// gestion des confirmations
	params.confirm_option_equip = params_input.confirm_option_equip.checked;
	params.confirm_option_mbl = params_input.confirm_option_mbl.checked;
	params.confirm_option_crado = params_input.confirm_option_crado.checked;
	params.confirm_option_contracts = params_input.confirm_option_contracts.checked;
	params.confirm_option_valid_N = params_input.confirm_option_valid_N.checked;
	params.confirm_option_valid_S = params_input.confirm_option_valid_S.checked;
	// gestion des raccourcis
	for (var i = 0 ; i < NB_LINKS ; i++ ) {
		params.shortcuts[i].name = params_input.shortcuts[i].name.value;
		params.shortcuts[i].link = params_input.shortcuts[i].link.value;
	}
	// gestion des fichiers externes
	params.read_histo = params_input.read_histo.checked;
	params.histo_date = clean_date(params_input.histo_date.year, params_input.histo_date.month, params_input.histo_date.day, params_input.histo_date.H, params_input.histo_date.M);
	params.check_forums = params_input.check_forums.checked;
	params.read_mbl = params_input.read_mbl.checked;

	// transformation des options en une chaîne
	GM_setValue('params_' + agency.id, '(' + JSON.stringify(params) + ')');

	// API
	API.pass = API_pass_input.value;
	set_API();
	// ré-afficher la page
	go();
}
// affichage des paramètres de configuration
function print_params() {
	// pourcentage
	params_input.percent.value = params.percent;
	// types de contrats
	params_input.kind_0.checked = params.kind_0;
	params_input.kind_1.checked = params.kind_1;
	params_input.kind_2.checked = params.kind_2;
	params_input.kind_3.checked = params.kind_3;
	params_input.kind_4.checked = params.kind_4;
	params_input.kind_5.checked = params.kind_5;
	// éviter les villes intoxiquées
	params_input.intox.checked = params.intox;
	// spéciaux à afficher
	params_input.paradox.checked = params.paradox;
	params_input.dangers.checked = params.dangers;
	params_input.infernaux.checked = params.infernaux;
	params_input.prout.checked = params.prout;
	params_input.CO2.checked = params.CO2;
	params_input.divers.checked = params.divers;
	// équipements permanents à surveiller
	params_input.equipS1.checked = params.equip_watched[1];
	params_input.equipS2.checked = params.equip_watched[2];
	params_input.equipL1.checked = params.equip_watched[4];
	params_input.equipL2.checked = params.equip_watched[5];
	params_input.equipF1.checked = params.equip_watched[7];
	params_input.equipF2.checked = params.equip_watched[8];
	params_input.equipG1.checked = params.equip_watched[11];
	params_input.equipG2.checked = params.equip_watched[12];
	params_input.equipC1.checked = params.equip_watched[14];
	params_input.equipC2.checked = params.equip_watched[15];
	params_input.equipE1.checked = params.equip_watched[16];
	params_input.equipE2.checked = params.equip_watched[17];
	params_input.equipB1.checked = params.equip_watched[21];
	params_input.equipB2.checked = params.equip_watched[22];
	params_input.equipMV.checked = params.equip_watched[23];
	params_input.equipCZ.checked = params.equip_watched[27];
	params_input.equipCL.checked = params.equip_watched[28];
	params_input.equipPM.checked = params.equip_watched[29];
	// stock
	params_input.stockS1T.value = params.stock[0];
	params_input.stockS1P.value = params.stock[1];
	params_input.stockS2P.value = params.stock[2];
	params_input.stockL1T.value = params.stock[3];
	params_input.stockL1P.value = params.stock[4];
	params_input.stockL2P.value = params.stock[5];
	params_input.stockF1T.value = params.stock[6];
	params_input.stockF1P.value = params.stock[7];
	params_input.stockF2P.value = params.stock[8];
	params_input.stockG1T.value = params.stock[9];
	params_input.stockG2T.value = params.stock[10];
	params_input.stockG1P.value = params.stock[11];
	params_input.stockG2P.value = params.stock[12];
	params_input.stockC1T.value = params.stock[13];
	params_input.stockC1P.value = params.stock[14];
	params_input.stockC2P.value = params.stock[15];
	params_input.stockB1T.value = params.stock[20];
	params_input.stockB1P.value = params.stock[21];
	params_input.stockB2P.value = params.stock[22];
	params_input.stockMV.value = params.stock[23];
	params_input.stockTP.value = params.stock[24];
	params_input.stockDP.value = params.stock[25];
	params_input.stockRM.value = params.stock[26];
	params_input.stockPM.value = params.stock[29];
	params_input.stockMBL.value = params.stock[31];
	// gestion des confirmations
	params_input.confirm_option_equip.checked = params.confirm_option_equip;
	params_input.confirm_option_mbl.checked = params.confirm_option_mbl;
	params_input.confirm_option_crado.checked = params.confirm_option_crado;
	params_input.confirm_option_contracts.checked = params.confirm_option_contracts;
	params_input.confirm_option_valid_N.checked = params.confirm_option_valid_N;
	params_input.confirm_option_valid_S.checked = params.confirm_option_valid_S;
	// gestion des raccourcis
	for (var i = 0 ; i < NB_LINKS ; i++ ) {
		params_input.shortcuts[i].name.value = params.shortcuts[i].name;
		params_input.shortcuts[i].link.value = params.shortcuts[i].link;
	}
	// gestion de la lecture des fichiers externes
	params_input.read_histo.checked = params.read_histo;
	params_input.histo_date.year.value = params.histo_date.getFullYear();
	params_input.histo_date.month.value = params.histo_date.getMonth()+1;
	params_input.histo_date.day.value = params.histo_date.getDate();
	params_input.histo_date.H.value = params.histo_date.getHours();
	params_input.histo_date.M.value = params.histo_date.getMinutes();
	params_input.check_forums.checked = params.check_forums;
	params_input.read_mbl.checked = params.read_mbl;
	// API
	API_pass_input.value = API.pass;
}
// relecture des paramètres de configuration
function cancel_params() {
	if (!confirm('Confirmez-vous l\'abandon des modifications saisies ?')) return;
	get_API();
	get_params();
	print_params();
}
function default_params() {
	if (!confirm('Confirmez-vous le retour aux valeurs par défaut ? (pensez à appliquer les changements ensuite)')) return;
	API.pass = '';
	init_params();
	print_params();
}

// ajoute les paramètres de configuration
function add_params() {
	// contrats normaux
	add_text(config_div, 'h2', 'title1', '', 'Filtrage des contrats normaux');
	var ul = add_text(config_div, 'ul', 'config', 'noMarg', '');

	// pourcentage de réussite mini
	var li = add_text(ul, 'li', 'percent', '', '<strong>Probabilité de réussite minimum</strong> : ');
	params_input.percent = add_input_text(li, 'percent', '', '2', '3');
	add_text(li, 'span', 'percent', '', '% <i>(avec les accessoires temporaires)</i>');

	// difficulté des contrats
	li = add_text(ul, 'li', 'difficulty', '', '<strong>Types de contrat à prendre en compte</strong> : ');
	params_input.kind_0 = add_input_checkbox(li, 'kind_0', false);
	add_text(li, 'span', 'kind_0', '', '<img src="/gfx/contract0.gif" width="16" alt="enfantin" title="enfantin">');
	params_input.kind_1 = add_input_checkbox(li, 'kind_1', false);
	add_text(li, 'span', 'kind_1', '', '<img src="/gfx/contract1.gif" width="16" alt="abordable" title="abordable">');
	params_input.kind_2 = add_input_checkbox(li, 'kind_2', false);
	add_text(li, 'span', 'kind_2', '', '<img src="/gfx/contract2.gif" width="16" alt="normal" title="normal">');
	params_input.kind_3 = add_input_checkbox(li, 'kind_3', false);
	add_text(li, 'span', 'kind_3', '', '<img src="/gfx/contract3.gif" width="16" alt="difficile" title="difficile">');
	params_input.kind_4 = add_input_checkbox(li, 'kind_4', false);
	add_text(li, 'span', 'kind_4', '', '<img src="/gfx/contract4.gif" width="16" alt="monstrueux" title="monstrueux">');
	params_input.kind_5 = add_input_checkbox(li, 'kind_5', false);
	add_text(li, 'span', 'kind_5', '', '<img src="/gfx/contract5.gif" width="16" alt="infernal" title="infernal">');

	// ignorer les contrats de certaines villes
	li = add_text(ul, 'li', 'intox', '', '<strong>Ignorer les villes intoxiquées (avec PDM)</strong> : ');
	params_input.intox = add_input_checkbox(li, 'intox', false);
	add_text(li, 'span', 'intox', '', 'pas de <img src="/gfx/icons/picto_prout.gif" width="16" alt="prout" title="prout">');

	// contrats spéciaux
	add_text(config_div, 'h2', 'title1', '', 'Filtrage des contrats spéciaux');
	ul = add_text(config_div, 'ul', 'config', 'noMarg', '');

	li = add_text(ul, 'li', 'specials_filter', '', '<strong>Contrats dont l\'affichage sera forcé</strong> :');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.paradox = add_input_checkbox(li, 'paradox', false);
	add_text(li, 'span', 'paradox', '', '<img src="/gfx/forum/time.gif" width="16px">&nbsp;Contrat paradoxal');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.dangers = add_input_checkbox(li, 'dangers', false);
	add_text(li, 'span', 'dangers', '', '<img src="/gfx/forum/dislike.gif" width="16px">&nbsp;Contrats acceptés mais sans monstre affecté');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.infernaux = add_input_checkbox(li, 'infernaux', false);
	add_text(li, 'span', 'infernaux', '', '<img src="/gfx/contract5.gif" width="16px">&nbsp;Contrats infernaux');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.prout = add_input_checkbox(li, 'prout', false);
	add_text(li, 'span', 'prout', '', '<img src="/gfx/icons/fartbox.gif" width="16px">&nbsp;Contrats sur les villes dont au moins un membre du syndicat a posé une boîte à pets');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.CO2 = add_input_checkbox(li, 'CO2', false);
	add_text(li, 'span', 'CO2', '', '<img src="/gfx/icons/smallco2.gif" width="16px">&nbsp;Contrats sur les villes à CO<span style="font-size:10px;">2</span>');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.divers = add_input_checkbox(li, 'divers', false);
	add_text(li, 'span', 'divers', '', 'Contrats saisonniers (scouts, lutins, etc.)');

	// gestion des objets
	add_text(config_div, 'h2', 'title1', '', 'Gestion des objets');
	ul = add_text(config_div, 'ul', 'config', 'noMarg', '');

	// Liste des équipements permanents à surveiller
	li = add_text(ul, 'li', 'equip', '', '<strong>Équipements permanents à surveiller (hors monstre MBL)</strong> :<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.equipS1 = add_input_checkbox(li, 'equipS1', false);
	add_text(li, 'span', 'equipS1', '', '<img src="/gfx/icons/sadism.gif" width="13" alt="sadisme" title="sadisme">+1');
	params_input.equipL1 = add_input_checkbox(li, 'equipL1', false);
	add_text(li, 'span', 'equipL1', '', '<img src="/gfx/icons/ugliness.gif" width="13" alt="laideur" title="laideur">+1');
	params_input.equipF1 = add_input_checkbox(li, 'equipF1', false);
	add_text(li, 'span', 'equipF1', '', '<img src="/gfx/icons/power.gif" width="13" alt="force" title="force">+1');
	params_input.equipG1 = add_input_checkbox(li, 'equipG1', false);
	add_text(li, 'span', 'equipG1', '', '<img src="/gfx/icons/greediness.gif" width="13" alt="gourmandise" title="gourmandise">+1');
	params_input.equipC1 = add_input_checkbox(li, 'equipC1', false);
	add_text(li, 'span', 'equipC1', '', '<img src="/gfx/icons/control.gif" width="13" alt="contrôle" title="contrôle">+1');
	params_input.equipB1 = add_input_checkbox(li, 'equipB1', false);
	add_text(li, 'span', 'equipB1', '', '<img src="/gfx/icons/fight.gif" width="13" alt="combat" title="combat">+1');
	params_input.equipE1 = add_input_checkbox(li, 'equipE1', false);
	params_input.equipE1.style.visibility = 'hidden';
	//~ add_text(li, 'span', 'equipE1', '', '<img src="/gfx/icons/endurance.gif" width="13" alt="endurance" title="endurance">+1');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.equipS2 = add_input_checkbox(li, 'equipS2', false);
	add_text(li, 'span', 'equipS2', '', '<img src="/gfx/icons/sadism.gif" width="13" alt="sadisme" title="sadisme">+2');
	params_input.equipL2 = add_input_checkbox(li, 'equipL2', false);
	add_text(li, 'span', 'equipL2', '', '<img src="/gfx/icons/ugliness.gif" width="13" alt="laideur" title="laideur">+2');
	params_input.equipF2 = add_input_checkbox(li, 'equipF2', false);
	add_text(li, 'span', 'equipF2', '', '<img src="/gfx/icons/power.gif" width="13" alt="force" title="force">+2');
	params_input.equipG2 = add_input_checkbox(li, 'equipG2', false);
	add_text(li, 'span', 'equipG2', '', '<img src="/gfx/icons/greediness.gif" width="13" alt="gourmandise" title="gourmandise">+2');
	params_input.equipC2 = add_input_checkbox(li, 'equipC2', false);
	add_text(li, 'span', 'equipC2', '', '<img src="/gfx/icons/control.gif" width="13" alt="contrôle" title="contrôle">+2');
	params_input.equipB2 = add_input_checkbox(li, 'equipB2', false);
	add_text(li, 'span', 'equipB2', '', '<img src="/gfx/icons/fight.gif" width="13" alt="combat" title="combat">+2');
	params_input.equipE2 = add_input_checkbox(li, 'equipE2', false);
	params_input.equipE2.style.visibility = 'hidden';
	//~ add_text(li, 'span', 'equipE2', '', '<img src="/gfx/icons/endurance.gif" width="13" alt="endurance" title="endurance">+2');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.equipMV = add_input_checkbox(li, 'equipMV', false);
	add_text(li, 'span', 'equipMV', '', '<img src="/gfx/tech/icone_malette.gif" width="30" alt="" title=""> Malette vivante ');
	params_input.equipPM = add_input_checkbox(li, 'equipPM', false);
	add_text(li, 'span', 'equipPM', '', '<img src="/gfx/tech/icone_bourse.gif" width="30" alt="" title=""> Porte monnaie Bigbluff ');
	params_input.equipCZ = add_input_checkbox(li, 'equipCZ', false);
	add_text(li, 'span', 'equipCZ', '', '<img src="/gfx/tech/icone_zinzin.gif" width="30" alt="" title=""> Chapeau ZinZin ');
	params_input.equipCL = add_input_checkbox(li, 'equipCL', false);
	add_text(li, 'span', 'equipCL', '', '<img src="/gfx/tech/icone_lutin.gif" width="30" alt="" title=""> Chapeau Lutin');

	// nombre d'équipements à avoir dans l'atelier (stock)
	li = add_text(ul, 'li', 'stock', '', '<strong>Objectifs de stock dans l\'atelier</strong> (T = temporaire / P = permanent) :<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	add_text(li, 'span', 'stockS1T', '', '&nbsp;<img src="/gfx/icons/sadism.gif" width="13" alt="sadisme" title="sadisme">+1 T&nbsp;');
	params_input.stockS1T = add_input_text(li, 'stockS1T', '', '2', '3');
	add_text(li, 'span', 'stockS1P', '', '&nbsp;<img src="/gfx/icons/sadism.gif" width="13" alt="sadisme" title="sadisme">+1 P&nbsp;');
	params_input.stockS1P = add_input_text(li, 'stockS1P', '', '2', '3');
	add_text(li, 'span', 'stockS2P', '', '&nbsp;<img src="/gfx/icons/sadism.gif" width="13" alt="sadisme" title="sadisme">+2 P&nbsp;');
	params_input.stockS2P = add_input_text(li, 'stockS2P', '', '2', '3');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	add_text(li, 'span', 'stockL1T', '', '&nbsp;<img src="/gfx/icons/ugliness.gif" width="13" alt="laideur" title="laideur">+1 T&nbsp;');
	params_input.stockL1T = add_input_text(li, 'stockL1T', '', '2', '3');
	add_text(li, 'span', 'stockL1P', '', '&nbsp;<img src="/gfx/icons/ugliness.gif" width="13" alt="laideur" title="laideur">+1 P&nbsp;');
	params_input.stockL1P = add_input_text(li, 'stockL1P', '', '2', '3');
	add_text(li, 'span', 'stockL2P', '', '&nbsp;<img src="/gfx/icons/ugliness.gif" width="13" alt="laideur" title="laideur">+2 P&nbsp;');
	params_input.stockL2P = add_input_text(li, 'stockL2P', '', '2', '3');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	add_text(li, 'span', 'stockF1T', '', '&nbsp;<img src="/gfx/icons/power.gif" width="13" alt="force" title="force">+1 T&nbsp;');
	params_input.stockF1T = add_input_text(li, 'stockF1T', '', '2', '3');
	add_text(li, 'span', 'stockF1P', '', '&nbsp;<img src="/gfx/icons/power.gif" width="13" alt="force" title="force">+1 P&nbsp;');
	params_input.stockF1P = add_input_text(li, 'stockF1P', '', '2', '3');
	add_text(li, 'span', 'stockF2P', '', '&nbsp;<img src="/gfx/icons/power.gif" width="13" alt="force" title="force">+2 P&nbsp;');
	params_input.stockF2P = add_input_text(li, 'stockF2P', '', '2', '3');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	add_text(li, 'span', 'stockG1T', '', '&nbsp;<img src="/gfx/icons/greediness.gif" width="13" alt="gourmandise" title="gourmandise">+1 T&nbsp;');
	params_input.stockG1T = add_input_text(li, 'stockG1T', '', '2', '3');
	add_text(li, 'span', 'stockG2T', '', '&nbsp;<img src="/gfx/icons/greediness.gif" width="13" alt="gourmandise" title="gourmandise">+2 T&nbsp;');
	params_input.stockG2T = add_input_text(li, 'stockG2T', '', '2', '3');
	add_text(li, 'span', 'stockG1P', '', '&nbsp;<img src="/gfx/icons/greediness.gif" width="13" alt="gourmandise" title="gourmandise">+1 P&nbsp;');
	params_input.stockG1P = add_input_text(li, 'stockG1P', '', '2', '3');
	add_text(li, 'span', 'stockG2P', '', '&nbsp;<img src="/gfx/icons/greediness.gif" width="13" alt="gourmandise" title="gourmandise">+2 P&nbsp;');
	params_input.stockG2P = add_input_text(li, 'stockG2P', '', '2', '3');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	add_text(li, 'span', 'stockC1T', '', '&nbsp;<img src="/gfx/icons/control.gif" width="13" alt="contrôle" title="contrôle">+1 T&nbsp;');
	params_input.stockC1T = add_input_text(li, 'stockC1T', '', '2', '3');
	add_text(li, 'span', 'stockC1P', '', '&nbsp;<img src="/gfx/icons/control.gif" width="13" alt="contrôle" title="contrôle">+1 P&nbsp;');
	params_input.stockC1P = add_input_text(li, 'stockC1P', '', '2', '3');
	add_text(li, 'span', 'stockC2P', '', '&nbsp;<img src="/gfx/icons/control.gif" width="13" alt="contrôle" title="contrôle">+2 P&nbsp;');
	params_input.stockC2P = add_input_text(li, 'stockC2P', '', '2', '3');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	add_text(li, 'span', 'stockB1T', '', '&nbsp;<img src="/gfx/icons/fight.gif" width="13" alt="combat" title="combat">+1 T&nbsp;');
	params_input.stockB1T = add_input_text(li, 'stockB1T', '', '2', '3');
	add_text(li, 'span', 'stockB1P', '', '&nbsp;<img src="/gfx/icons/fight.gif" width="13" alt="combat" title="combat">+1 P&nbsp;');
	params_input.stockB1P = add_input_text(li, 'stockB1P', '', '2', '3');
	add_text(li, 'span', 'stockB2P', '', '&nbsp;<img src="/gfx/icons/fight.gif" width="13" alt="combat" title="combat">+2 P&nbsp;');
	params_input.stockB2P = add_input_text(li, 'stockB2P', '', '2', '3');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	add_text(li, 'span', 'stockMV', '', '&nbsp;<img src="/gfx/tech/icone_malette.gif" width="30"> Mallette vivante&nbsp;');
	params_input.stockMV = add_input_text(li, 'stockMV', '', '2', '3');
	add_text(li, 'span', 'stockPM', '', '&nbsp;<img src="/gfx/tech/icone_bourse.gif" width="30"> Porte monnaie Bigbluff&nbsp;');
	params_input.stockPM = add_input_text(li, 'stockPM', '', '2', '3');
	add_text(li, 'span', 'stockMBL', '', '&nbsp;<img src="/gfx/tech/icone_mbldop.gif" width="30"> Dop-Dopage&nbsp;');
	params_input.stockMBL = add_input_text(li, 'stockMBL', '', '2', '3');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	add_text(li, 'span', 'stockTP', '', '&nbsp;<img src="/gfx/tech/icone_sabotage.gif" width="30"> Tripatouilleur&nbsp;');
	params_input.stockTP = add_input_text(li, 'stockTP', '', '2', '3');
	add_text(li, 'span', 'stockDP', '', '&nbsp;<img src="/gfx/tech/icone_distru.gif" width="30"> Disrupteur&nbsp;');
	params_input.stockDP = add_input_text(li, 'stockDP', '', '2', '3');
	add_text(li, 'span', 'stockRM', '', '&nbsp;<img src="/gfx/tech/icone_detour.gif" width="30"> Régulateur de marché&nbsp;');
	params_input.stockRM = add_input_text(li, 'stockRM', '', '2', '3');

	// options du script
	add_text(config_div, 'h2', 'title1', '', 'Gestion du script');
	ul = add_text(config_div, 'ul', 'config', 'noMarg', '');

	// gestion des confirmations
	li = add_text(ul, 'li', 'confirm_config', '', '<strong>Confirmations souhaitées lors des actions suivantes</strong> :');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.confirm_option_equip = add_input_checkbox(li, 'confirm_option_equip', false);
	add_text(li, 'span', 'confirm_option_equip', '', 'Équiper un monstre avec des objets permanents');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.confirm_option_mbl = add_input_checkbox(li, 'confirm_option_mbl', false);
	add_text(li, 'span', 'confirm_option_mbl', '', 'Doper un monstre pour la MBL');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.confirm_option_crado = add_input_checkbox(li, 'confirm_option_crado', false);
	add_text(li, 'span', 'confirm_option_crado', '', 'Utiliser une cradoline');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.confirm_option_contracts = add_input_checkbox(li, 'confirm_option_contracts', false);
	add_text(li, 'span', 'confirm_option_contracts', '', 'Générer les contrats');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.confirm_option_valid_N = add_input_checkbox(li, 'confirm_option_valid_N', false);
	add_text(li, 'span', 'confirm_option_valid_N', '', 'Valider les équipements temporaires et les affectations à des contrats normaux');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.confirm_option_valid_S = add_input_checkbox(li, 'confirm_option_valid_S', false);
	add_text(li, 'span', 'confirm_option_valid_S', '', 'Valider les équipements temporaires et les affectations à des contrats spéciaux');

	// gestion des lectures de fichiers
	li = add_text(ul, 'li', 'extern_files', '', '<strong>Gestion des lectures de fichiers externes</strong> :');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.read_mbl = add_input_checkbox(li, 'read_mbl', false);
	add_text(li, 'span', 'read_histo', '', 'Lister les matchs MBL en cours ou à venir');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.check_forums = add_input_checkbox(li, 'check_forums', false);
	add_text(li, 'span', 'read_forums', '', 'Afficher le nombre de messages non lus sur les boutons des forums (sauf raccourcis personnels)');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	params_input.read_histo = add_input_checkbox(li, 'read_histo', false);
	add_text(li, 'span', 'read_histo', '', 'Résumer l\'historique au démarrage du script');
	add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
	add_text(li, 'span', 'histo_date_label', '', 'Date limite de lecture de l\'historique : ');
	params_input.histo_date = new Object();
	params_input.histo_date.day = add_input_text(li, 'histo_date_day', '', '2', '2');
	add_text(li, 'span', '', '', '/');
	params_input.histo_date.month = add_input_text(li, 'histo_date_month', '', '2', '2');
	add_text(li, 'span', '', '', '/');
	params_input.histo_date.year = add_input_text(li, 'histo_date_year', '', '2', '4');
	add_text(li, 'span', '', '', ' à ');
	params_input.histo_date.H = add_input_text(li, 'histo_date_H', '', '2', '2');
	add_text(li, 'span', '', '', 'h');
	params_input.histo_date.M = add_input_text(li, 'histo_date_M', '', '2', '2');
	add_text(li, 'span', 'histo_date_info', '', '<br><i>Cette date sera mise à jour automatiquement, mais vous pouvez forcer une date pour la prochaine lecture</i>');

	// gestion des raccourcis
	li = add_text(ul, 'li', 'shortcuts', '', '<strong>Raccourcis personnalisés</strong> (laissez le nom vide pour ne pas utiliser) :');
	for (var i = 0 ; i < NB_LINKS ; i++ ) {
		params_input.shortcuts[i] = new Object();
		add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
		add_text(li, 'span', 'shortcut_name_' + i, '', '* Nom du raccourci ' + (i+1) + ' :&nbsp;');
		params_input.shortcuts[i].name = add_input_text(li, 'shortcut_name_' + i, '', 15, 30);
		add_text(li, 'span', 'return', '', '<br>&nbsp;&nbsp;&nbsp;&nbsp;');
		add_text(li, 'span', 'shortcut_link_' + i, '', '> Lien associé : ' + GM_info.script.namespace + '/');
		params_input.shortcuts[i].link = add_input_text(li, 'shortcut_link_' + i, '', 40, 100);
	}

	// mot de passe API
	li = add_text(ul, 'li', 'pass', '', '<strong>Mot de passe API</strong> : ');
	API_pass_input = add_input_pass(li, 'pass', '', '15');

	// boutons
	var div = add_text(config_div, 'div', 'params_buttons', '', '<br>&nbsp;');
	div.setAttribute('style', 'text-align:center;display:block;');
	add_func_button(div, 'apply', 'Enregistrer les valeurs', set_params, 'Sauvegarde la configuration');
	add_func_cancel_button(div, 'cancel', 'Annuler les modifications', cancel_params, 'Annuler la saisie actuelle, reprendre les paramètres précédemment enregistrés');
	add_func_cancel_button(div, 'cancel', 'Réinitialiser aux valeurs par défaut', default_params, 'Annuler la saisie actuelle, reprendre les paramètres d\'origine du script');

	print_params();
}

// texte de l'aide
function add_help(here) {
	var my_ul
	var my_li;
	var my_span;
	var help_txt = add_text(here, 'div', 'help_content', '', '');
	help_txt.setAttribute('style', 'text-align:justify;padding:0px 30px 0px 30px;');

	add_text(help_txt, 'h2', '', '', 'Objectifs de ce script');
	my_ul = add_text(help_txt, 'ul', '', '', '');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Avoir une vision rapide et globale de Croque Monster</strong>');
	add_text(my_li, 'div', '', '', 'Ce script peut rassembler : une lecture de l\'historique de l\'agence, les matchs MBL en cours et à venir, des raccourcis utiles pré-définis, des raccourcis personnalisés par l\'utilisateur (oui, toi !), une interface de gestion de l\'équipement des monstres, de l\'équipement MBL idéal, de l\'atelier et des contrats.');
	add_text(my_li, 'span', '', '', '<br>');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Affecter rapidement et au mieux les monstres disponibles</strong>');
	add_text(my_li, 'div', '', '', 'Grâce à l\'interface de gestion des contrats, il est possible avec ce script d\'utiliser une <img src="/gfx/icons/cradoline.gif" alt="cradoline"> en 1 clic, de définir des filtres en quelques clics et de demander une liste d\'affectations de vos monstres sur les contrats ramenant le maximum de <img src="/gfx/icons/miniMoney.gif" alt="monstercrédits"> ou augmentant au mieux le pourcentage des <img src="/gfx/contract5.gif" alt="infernaux">. Une fois la liste calculée (ça peut prendre plusieurs dizaines de secondes selon les cas &mdash; au pire, le script devra tester environ 350000 combinaisons !), vous pouvez en 1 clic attribuer les objets utiles et affecter les monstres aux contrats.');
	add_text(my_li, 'div', '', '', 'Un second tableau regroupe les contrats spéciaux pour lesquels vous pouvez forcer l\'affichage : le paradoxe, les contrats acceptés mais sans monstre, les infernaux, les contrats sur les villes avec une boîte à pets, les contrats sur les villes spéciales à CO<span style="font-size:10px;">2</span> ou encore les contrats saisonniers tels que les scouts ou les lutins.');

	add_text(help_txt, 'h2', '', '', 'Comment configurer ce script ?');
	my_ul = add_text(help_txt, 'ul', '', '', '');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Filtrage des contrats normaux</strong>');
	add_text(my_li, 'div', '', '', 'Les contrats dits "normaux" seront retenus lors de l\'optimisation. Vous ne pourrez pas choisir directement ces contrats, il vous faut donc utiliser des filtres pour guider l\'optimisation. Vous avez la main sur le pourcentage de réussite minimum (avec objets, d\'ailleurs, si des objets sont nécessaires, cela vous sera indiqué) pour pouvoir affecter un monstre à un contrat. Vous pouvez également choisir le type de contrat que vous souhaitez conserver. Pour finir, vous avez la possibilité d\'ignorer les villes ayant déjà des PDM (Points De Marché) marqués par votre syndicat.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Filtrage des contrats spéciaux</strong>');
	add_text(my_li, 'div', '', '', 'Les contrats dits "spéciaux" font l\'objet d\'un affichage forcé dans un tableau dédié. Dans ce tableau, vous aurez tout loisir d\'affecter le monstre que vous voulez au contrat. Les contrats dont l\'affichage peut être forcé sont : le contrat paradoxal, les contrats acceptés mais sans monstre affecté, les contrats infernaux, les contrats sur les villes dont au moins un membre du syndicat a posé une boîte à pets, les contrats sur les villes à CO<span style="font-size:10px;">2</span> et les contrats saisonniers (scouts, lutins, etc.).<br><strong>Attention</strong>, selon votre configuration un contrat spécial peut faire partie de l\'optimisation, ils n\'en sont pas exclus.<br>Exemple : un contrat monstrueux sur une ville à CO<span style="font-size:10px;">2</span>, si il remplit les conditions du filtrage des contrats normaux, pourra être choisi dans l\'optimisation. Il en va de même pour un infernal si vous acceptez les infernaux dans vos contrats normaux.<br>Il est donc conseillé de jeter un œil sur vos contrats spéciaux avant de lancer une optimisation. Erreur typique : croire qu\'un infernal fera forcément partie de l\'optimisation selon l\'avancement du pourcentage d\'infernal alors que le pourcentage d\'infernal de ce contrat sera à 0%.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Gestion des objets</strong>');
	add_text(my_li, 'div', '', '', 'La liste des équipements permanents à surveiller sert à équiper rapidement (en 1 clic) un monstre de tous les objets désirés. En effet, il est toujours désagréable de perdre un objet pendant un contrat, et il est possible de ne pas le remarquer immédiatement. Ce script le fait à votre place et vous indiquera quels équipements sont à remettre pour chaque monstre (sauf le monstre MBL qui a généralement un équipement spécifique).');
	add_text(my_li, 'div', '', '', 'La gestion du stock de l\'atelier vous permet de facilement construire tous les objets dont vous avez besoin. Une fois les stocks définis dans les options, en 1 seul clic vous pouvez lancer le maximum d\'objets possible dans votre atelier. Puis en laissant votre page ouverte avec le script en fonctionnement, il continuera automatiquement de produire vos objets jusqu\'à atteindre les valeurs que vous avez définies.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Gestion du script</strong>');
	add_text(my_li, 'div', '', '', 'Pour avoir le "Menu Singe" qui vous ressemble (vous descendez du singe, non ?), le script est configurable. Vous pouvez spécifier quelles actions seront conditionnées par une confirmation ou non afin d\'éviter les clics malheureux qui pourraient coûter une cradoline par exemple. Afin de trouver le bon équilibre entre vitesse de chargement du script et ce que vous avez besoin, il est possible d\'activer ou non certains chargements de pages en arrière plan : lire l\'historique de l\'agence (la date est également configurable), lire le nombre de messages non lus dans les forums favoris et le forum syndical et enfin lire les matchs MBL. La dernière option utile pour personnaliser le script est de définir des raccourcis vers des pages que vous utilisez souvent. Pour cela, il suffit de leur donner un nom puis de coller à la suite de "http://www.croquemonster.com/" la fin de l\'adresse de la page que vous souhaitez pointer.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Sauvegarde de la configuration</strong>');
	add_text(my_li, 'div', '', '', 'Il est nécessaire de sauvegarder les valeurs pour qu\'elles soient prises en compte par le script.');

	add_text(help_txt, 'h2', '', '', 'Interface de gestion');
	add_text(help_txt, 'div', '', '', 'Voici, de haut en bas une explication concernant l\'interface de ce script');
	my_ul = add_text(help_txt, 'ul', '', '', '');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Onglet "Menu Singe "</strong>');
	add_text(my_li, 'div', '', '', 'Même une fois lancé, vous pouvez cliquer sur l\'onglet pour recharger l\'affichage du script. Par exemple si vous avez passé un fuseau et que vos contrats sont joués, alors cela mettra à jour le tableau des monstres. Les messages non lus, la liste des matchs MBL, etc. tout cela sera mis à jour.<br>À chaque appui sur l\'onglet, si vous avez activé l\'option, le script vous fournira un résumé concernant l\'historique des monstres de votre agence si des éléments non lus sont présents.<br>À noter, les informations du monster pod seront également mises à jour : heure, <img src="/gfx/icons/lvl.gif" alt="niveau">, <img src="/gfx/icons/reputation.gif" alt="réputation">, <img src="/gfx/icons/miniMoney.gif" alt="monstercrédits"> et les <img src="/gfx/icons/nomail.gif" alt="messages">.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>MBL</strong>');
	add_text(my_li, 'div', '', '', 'Si vous avez coché de recevoir des informations à propos de la MBL, alors vous aurez une liste des matchs en cours ou à venir si il en existe ; sinon, vous aurez un lien vers vos matchs terminés.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Raccourcis</strong>');
	add_text(my_li, 'div', '', '', 'Le script fournit un lien vers le commerce, les enchères et le tas du syndicat si vous faîtes partie d\'un syndicat. Deux raccourcis spéciaux sont également accessibles : les favoris du forum et le forum syndical (si vous faites partie d\'un syndicat). Pour ces deux derniers raccourcis spéciaux, <img src="/gfx/icons/mailblink.gif" alt="messages non lus"> indique qu\'il existe des messages non lus tandis que <img src="/gfx/icons/nomail.gif" alt="messages tous lus"> vous indique qu\'aucun nouveau message n\'est repéré.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Stock</strong>');
	add_text(my_li, 'div', '', '', 'Si votre stock est conforme aux objectifs fixés, alors vous en serez informés. Sinon, vous aurez un bouton pour lancer la construction de vos objets. En passant la souris sur ce bouton, vous aurez le détails des constructions qui seront lancées.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Cradoline</strong>');
	add_text(my_li, 'div', '', '', 'Le stock de vos cradolines est indiqué par <img src="/gfx/icons/cradoline.gif" alt="cradoline">. À côté, vous trouverez un bouton vous permettant d\'en utiliser une.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Contrats</strong>');
	add_text(my_li, 'div', '', '', 'Il est indiqué le nombre de contrats générés pour le moment sur le nombre de villes possédées. Il est également indiqué l\'heure du dernier contrat généré. Ici, vous pourrez aussi générer les contrats sans charger la page des contrats. En réalité, ce bouton le fait pour vous.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Filtre horaire</strong>');
	add_text(my_li, 'div', '', '', 'Ici vous pourrez filtrer les horaires de début et de fin des contrats que vous autorisez pour l\'optimisation. Pour mieux comprendre le sens de "+1h" ou "+5h", prenons un exemple : il est 17h13, vous demandez les contrats entre "maintenant" et "+3h", alors cela indique que vous voulez conserver tous les contrats entre 17h13 et 20h13, soit pendant 3 fuseaux.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Simulation</strong>');
	add_text(my_li, 'div', '', '', 'La simulation est utile lorsque vos montres sont fatigués et que vous souhaitez savoir si une cradoline sera utile et rentable. En cochant cette case, l\'optimisation se lancera sans que vous puissiez valider le choix. Vous aurez tout de même connaissance des revenus espérés, du nombre de monstres avec un contrat, etc. Si vous estimez que cela est intéressant, alors vous pouvez utiliser une cradoline puis demander cette fois-ci une réelle optimisation que vous validerez.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Sélection</strong>');
	add_text(my_li, 'div', '', '', 'Deux boutons sont donnés ici. Vous pourrez demander une optimisation en privilégiant l\'avancement des pourcentages d\'infernaux (avec en second critère la rentrée d\'argent tout de même) ou en privilégiant seulement les revenus possibles. Il est important de noter que les primes des monstres seront prises en compte dans ce calcul ainsi que les prix de construction des équipements nécessaires pour obtenir le pourcentage demandé par le filtrage. Le coût des objets humains nécessaires à la construction des objets temporaires ne sont pas pris en compte car avec des mallettes vivantes sur tous les monstres, finalement on ne manque jamais d\'objets humains.<br>Au pire des cas, le script devra tester environ 350000 combinaisons, il est donc possible que cela prenne plusieurs dizaines de secondes. Mais ce cas est marginal, vous aurez certainement le plus souvent une optimisation rapide. En réalité, si tous vos monstres sont identiques, alors l\'optimisation est plus difficile car ils se disputent les mêmes contrats et le nombre de possibilités explose.<br>Une fois le filtrage terminé, le nombre de contrats restants est donné à droite des boutons.');


	add_text(help_txt, 'h2', '', '', 'Tableau des monstres et des contrats normaux');
	add_text(help_txt, 'div', '', '', 'Le premier tableau dresse sur chaque ligne un ensemble d\'informations ou d\'actions disponibles pour un monstre. Voici comment cela est organisé.');
	my_ul = add_text(help_txt, 'ul', '', '', '');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "Monstre"</strong>');
	add_text(my_li, 'div', '', '', 'Le nom du monstre est donné. En passant sur le nom, vous aurez des informations concernant votre monstre (caractéristiques, revente). Enfin, vous pouvez cliquer sur le nom pour aller sur la page du monstre.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "<img src="/gfx/forum/icon_chart.gif" alt="Équipement">"</strong>');
	add_text(my_li, 'div', '', '', 'Cette colonne vous permet d\'équiper rapidement un monstre de tous les permanents choisis dans les paramètres. Si il existe au moins un objet à équiper, le bouton <img src="/gfx/icons/equip.gif" alt="Équiper"> apparaîtra. Si la case est vide alors le monstre a son équipement permanent au complet.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "MBL"</strong>');
	add_text(my_li, 'div', '', '', 'Passez votre souris sur <img src="/gfx/icons/fight.gif" alt="combat" width="12px"> pour connaître le niveau maximum de <img src="/gfx/icons/fight.gif" alt="combat"> que vous pouvez obtenir après équipement et dopage si nécessaire. Cliquez sur l\'image pour équiper le monstre.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "Occupation"</strong>');
	add_text(my_li, 'div', '', '', 'Occupation sur laquelle un monstre est affecté (contrat, match, racket, ...). Selon l\'occupation du monstre des informations s\'affichent au survol de la souris ou un lien est cliquable.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "<img src="/gfx/icons/reveil.gif" alt="Astreinte">"</strong>');
	add_text(my_li, 'div', '', '', 'Vide si le monstre n\'est pas d\'astreinte, sinon le nombre de <img src="/gfx/icons/spa_traumal.gif" alt="spa" width="18px"> prévu pour l\'astreinte est affiché.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "<img src="/gfx/forum/time.gif" alt="Fatigue">"</strong>');
	add_text(my_li, 'div', '', '', 'Fatigue restante du monstre.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "<img src="/gfx/forum/icon_cross.gif" alt="Ignorer">"</strong>');
	add_text(my_li, 'div', '', '', 'Cette case à cocher permet d\'ignorer un monstre lors d\'une optimisation. En effet, si vous souhaitez conserver un monstre pour autre chose ou plus tard, il est préféfrable d\'en avertir l\'optimisation afin qu\'elle fournisse les meilleurs gains possibles.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "#"</strong>');
	add_text(my_li, 'div', '', '', 'Nombre de contrats filtrés pour un monstre (dépend du pourcentage de réussite) après l\'optimisation.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "% inf."</strong>');
	add_text(my_li, 'div', '', '', 'Pourcentage d\'avancement d\'infernal du contrat choisi par l\'optimisation.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "<img src="/gfx/icons/miniMoney.gif" alt="Récompense">"</strong>');
	add_text(my_li, 'div', '', '', 'Prix du contrat choisi par l\'optimisation.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "Réussite et infos"</strong>');
	add_text(my_li, 'div', '', '', 'Réussite du contrat choisi par l\'optimisation. Si un équipement est possible pour améliorer les chances de réussir le contrat, alors les deux pourcentages sont donnés. En survolant avec la souris cette zone, vous obtenez des informations concernant le contrat et le monstre. Si il y a des risques de dévorer le mioche alors qu\'il ne devait pas l\'être, alors un "!" apparaît.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "<img src="/gfx/icons/equip.gif" alt="Équiper">"</strong>');
	add_text(my_li, 'div', '', '', 'Si le contrat choisi par l\'optimisation peut être mieux réussi avec un équipement, il faut cocher cette case pour équiper le monstre.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Colonne "<img src="/gfx/icons/ok.gif" alt="Affecter">"</strong>');
	add_text(my_li, 'div', '', '', 'Il faut cocher cette case pour que le monstre soit affecté au contrat qui a été choisi pour lui.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Gains</strong>');
	add_text(my_li, 'div', '', '', 'Le résumé des gains est donné dans un tableau. Le bouton de mise à jour sert à vous donner les gains en fonction des cases d\'équipement et d\'affectation que vous aurez cochés ou décochées. Les gains réels sont les revenus que vous aurez après avoir payé les monstres et payé les constructions des objets.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Validation</strong>');
	add_text(my_li, 'div', '', '', 'Si l\'optimisation n\'est pas l\'objet d\'une simulation, alors un bouton de validation apparaît. En cliquant sur ce bouton, vous allez équiper tous les monstres pour lesquels la case d\'équipement est coché puis tous les monstres dont la case d\'affectation est cochée sont affectés aux contrats choisis. Dès qu\'une erreur est détectée par le script, il vous prévient et stoppe le processus.');

	add_text(help_txt, 'h2', '', '', 'Tableau des contrats spéciaux');
	add_text(help_txt, 'div', '', '', 'Ce tableau force l\'affichage de certains contrats. Ici les monstres sont dans les colonnes et les contrats sont listés sur les lignes. Le monstre MBL ne fait pas partie de cet affichage puisque le seul but de ce tableau est de choisir un contrat.');
	my_ul = add_text(help_txt, 'ul', '', '', '');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Principe de l\'affichage</strong>');
	add_text(my_li, 'div', '', '', 'Chaque ligne concerne un contrat différent. Chaque case donne le pourcentage de chances de réussir un contrat. Si un équipement peut être mis sur le monstre pour améliorer les chances de réussite, alors ce pourcentage est également affiché. Si il existe un risque de dévorer l\'enfant alors que ce n\'était pas prévu dans le contrat, cela est indique par "!". Si vous souhaitez faire un contrat il suffit de cocher la case puis d\'appuyer sur le bouton de validation sous le tableau. Pour finir, il est possible de survoler un contrat pour avoir plus de détails.<br>Si le monstre est fatigué, alors <img src="/gfx/forum/time.gif" alt="Fatigue"> est affiché. Si le monstre n\'est pas disponible, <img src="/gfx/forum/icon_cross.gif" alt="occupé"> est affiché.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Équiper un monstre</strong>');
	add_text(my_li, 'div', '', '', 'Pour équiper un monstre, il faut cocher la case <img src="/gfx/icons/equip.gif" alt="Équiper"> près de son nom.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Détails des contrats spéciaux</strong>');
	add_text(my_li, 'div', '', '', 'Pour certains spéciaux, il existe des informations lors du survol de son nom dans la première colonne. Exemple : le propriétaire d\'une boite à pêts et les PDM du syndicat sur cette ville.');
	my_li = add_text(my_ul, 'li', '', '', '<strong>Cas des contrats spéciaux très spéciaux</strong>');
	add_text(my_li, 'div', '', '', 'Il est possible qu\'un contrat infernal soit sur une ville à CO<span style="font-size:10px;">2</span> sur laquelle une boîte à pets a été installée. Bref, les contrats spéciaux peuvent se recouper. Si tel est le cas, alors le contrat ne sera affiché qu\'une seule fois, avec en priorité cet ordre : <img src="/gfx/forum/dislike.gif" alt="Dangers"> (contrats acceptés mais sans monstre), <img src="/gfx/contract5.gif" alt="infernaux">, <img src="/gfx/icons/fartbox.gif" alt="Boîte à pets">, <img src="/gfx/icons/smallco2.gif" alt="CO2"> et les contrats "Divers" (saisonniers tels que les lutins ou les scouts), . Afin de savoir que vous avez sous les yeux un contrat très spécial, la mention "lire&nbsp;<img src="/gfx/forum/icon_exclaim.gif" alt="!"> " avec des explications lors du survol sera affichée.');
}

// création du "Menu Singe"
function create_content() {
	contenu_perso.setAttribute('id', 'content');

	// récupération du bonus de pleine lune
	get_moon();

	// récupération de la liste des monstres
	get_monsters();

	// récupération des paramètres
	get_params();

	// mise à jour du pod
	update_pod();

	if (params.read_histo) read_histo();

	// MBL
	mbl_div = add_text(contenu_perso, 'div', 'mbl', '', '');
	add_match();

	// Raccourcis
	raccourcis_div = add_text(contenu_perso, 'h1', 'shortcuts', 'noMarg', '<img src="/gfx/forum/like.gif" width="18px"> Raccourcis ');
	add_link_button(raccourcis_div, 'exchange', 'Commerce', '/exchange', 'Acheter des objets humains');
	add_link_button(raccourcis_div, 'auctions', 'Enchères', '/exchange/auction', 'Enchères pour de la cradoline');
	add_links_only_syndicate(raccourcis_div);
	add_reads();
	add_reads_only_syndicate();
	var temp = add_text(contenu_perso, 'div', 'shortcuts', '', '');
	add_links_player(temp);

	// Monstres, contrats et atelier
	monsters_div = add_text(contenu_perso, 'div', 'monsters_contracts_labo', '', '');
	add_text(monsters_div, 'h1', 'monsters', 'noMarg', '<img src="/gfx/icons/stat_monster.gif" width="18px"> Monstres, contrats et atelier');
	monsters_ul = add_text(monsters_div, 'ul', 'monsters', 'noMarg', '');
	// ajoute la ligne concernant le stock
	add_line_stock();
	// ajoute la ligne concernant la cradoline
	add_line_crado();
	// ajoute la ligne du nombre de contrats (donc avec lecture des contrats)
	add_line_contracts();
	// ajoute la ligne de sélection
	add_line_filter();
	// ajoute la ligne de simulation
	add_line_simulation();

	// sélection
	var li = add_text(monsters_ul, 'li', 'tri', '', 'Sélection : ');
	add_func_button(li, 'inferno', '<img src="/gfx/contract5.gif" width="17px"> Optimiser l\'avancement des infernaux', function () {optimize(INFERNO);}, 'Choisir les contrats en privilégiant le pourcentage d\'infernal');
	add_func_button(li, 'money', '<img src="/gfx/forum/miniMoney.gif" width="17px"> Optimiser les Monstercrédits', function () {optimize(MONEY);}, 'Choisir les contrats en privilégiant la rentrée d\'argent');

	// tableaux des monstres avec les contrats
	contracts_div = add_text(contenu_perso, 'div', 'contrats', '', '');
	add_monster_table();

	// ajout du tableau des spéciaux
	special_div = add_text(contenu_perso, 'div', 'special', '', '');
	if (params.paradox || params.dangers || params.infernaux || params.prout || params.CO2 || params.divers) add_special_table();

	// Configuration
	config_div = add_text(contenu_perso, 'div', 'config_on_off', '', '');
	config_div.setAttribute('style', 'text-align:center;display:block;');
	config_on = add_func_button(config_div, 'config_on', 'Afficher les options de configuration', show_config, '');
	config_off = add_cancel_button(config_div, 'config_off', 'Cacher les options de configuration', '');
	config_off.addEventListener('click', hide_config, false);
	config_div = add_text(contenu_perso, 'div', 'config', '', '<br>');
	hide_config();
	add_text(config_div, 'h1', 'monsters', 'noMarg', '<img src="/gfx/forum/icon_idea.gif" width="18px"> Configuration');
	add_params();

	// aide
	help_div = add_text(contenu_perso, 'div', 'help', '', '<br>');
	help_div.setAttribute('style', 'text-align:center;display:block;');
	help_on = add_func_button(help_div, 'help_on', 'Afficher l\'aide de ' + GM_info.script.name + ' v' + GM_info.script.version, show_help, '');
	help_off = add_cancel_button(help_div, 'help_off', 'Cacher l\'aide', '');
	help_off.addEventListener('click', hide_help, false);
	help_div = add_text(help_div, 'div', 'help', '', '<br><h1 class="noMarg">Aide</h1>');
	help_div.setAttribute('style', 'text-align:left;');
	hide_help();
	add_help(help_div);

	var bottom = document.createElement('div');
	bottom.setAttribute('id', 'contentBottom');
	contenu_perso.appendChild(bottom);

	// récupération des villes intoxiquées pour gagner du temps lors du filtrage
	wait_for_syndicate(function () {
		if (agency.syndicateId != 0) {
			if (params.prout || params.intox) get_PDM(); else PDM.OK = true;
			if (params.prout) wait_for_PDM(function () { get_prout(); }); else prout_OK = true;
		}
		else {
			PDM.OK = true;
			prout_OK = true;
		}
	});
	// récupération des infernaux
	get_cities();
}

// fonction qui remplace le contenu par celui du "Menu Singe" (et recharge les données si nécessaire)
function go() {
	contenu_perso.innerHTML = '';
	if (API.pass == '') {
		get_API();
		if (API.pass == '') init_API();
	}
	if (API.pass != '') {
		get_agency();
		wait_for_API(function () {
			if (API.name == '') {
				alert('Mauvais mot de passe API ou accès API désactivé.\nVérifiez les paramètres de votre compte et réessayez.');
			}
			else {
				set_API();
				wait_for_syndicate(create_content);
				if (first_time) {
					first_time = false;
					var x = document.getElementById('content');
					var parent = x.parentNode;
					var onglets = liste_onglets.getElementsByTagName('a');
					for (i = 0 ; i < onglets.length-1 ; i++) onglets[i].setAttribute('class', '');
					tab_singe.setAttribute('class', 'active');
					parent.replaceChild(contenu_perso, x);
					document.title = 'CroqueMonster - Menu Singe';
					var pod = document.getElementById('podMsg');
					pod.innerHTML = 'Bienvenue dans l\'univers de squalbee, un joueur qui vous veut du bien <img src="/gfx/forum/icon_smile.gif" width="13px">.<br><br>Vive le <a href="/syndicate/5454">RCC</a> !<br><br>Pensez à lire l\'aide.<br>(voir en bas)';
				}
			}
		});
	}
}

// affichage de l'onglet du "Menu Singe"
function create_tab() {
	liste_onglets = document.getElementById('tabulation');
	if (!liste_onglets) {
		var x = document.getElementById('contentTop');
		var parent = x.parentNode;
		liste_onglets = document.createElement('div');
		liste_onglets.setAttribute('id', 'tabulation');
		liste_onglets.innerHTML = '<ul class="tab"><li><a class="active" href="' + location.href + '">Page normale</a></li></ul>'
		parent.replaceChild(liste_onglets, x);
	}
	var tab_here = liste_onglets.getElementsByTagName('ul');
	if (!tab_here) return;
	var li = document.createElement('li');
	tab_singe = document.createElement('a');
	tab_singe.setAttribute('class', '');
	tab_singe.setAttribute('style', 'cursor:pointer');
	tab_singe.innerHTML = 'Menu Singe <img src="/gfx/forum/icon_exclaim.gif" alt="!" width="13px">';
	tab_singe.addEventListener('click', go, false);
	li.appendChild(tab_singe);
	tab_here[0].appendChild(li);
}

if (agency.id != 0 && check_page()) create_tab();

// ces fonctions sont ici car elles font planter la reconnaissance des fonctions dans mon éditeur (à cause des ' de partout)
// affichage d'une barre de pourcentage
function print_tip_percent(label, classe, percent, percent_equip) {
	var barre = '';
	if (classe != '') classe = ' class=\\\'' + classe + '\\\'';
	if (label == 'Total') label = '<strong>Total</strong>';
	if (percent_equip > percent || (label == 'Miam' && percent_equip < percent)) {
		barre = '<li' + classe + '><label>' + label + '</label><div class=\\\'xpMiniBar\\\'><div class=\\\'xpBorder\\\'><div class=\\\'xpBar\\\' style=\\\'width:' + percent_equip + '%;\\\'> </div><div class=\\\'xpNb\\\'>' + percent_equip + '%</div></div></div><div class=\\\'xpMiniBar\\\'>&nbsp;&gt;&nbsp;</div><div class=\\\'xpMiniBar\\\'><div class=\\\'xpBorder\\\'><div class=\\\'xpBar\\\' style=\\\'width:' + percent + '%;\\\'></div><div class=\\\'xpNb\\\'>' + percent + '%</div></div></div></li>';
	}
	else {
		if (!(label == 'Miam' && percent < 100)) {
			barre = '<li' + classe + '><label>' + label + '</label><div class=\\\'xpMiniBar\\\'><div class=\\\'xpBorder\\\'><div class=\\\'xpBar\\\' style=\\\'width:' + percent + '%;\\\'></div><div class=\\\'xpNb\\\'>' + percent + '%</div></div></div></li>';
		}
	}
	return barre;
}

// affichage d'un contrat : lien + boîte + estimation
function print_link_estimation(cid, mid, txt) {
	var infos1 = '';
	var infos2 = '';
	var sadism = '';
	var ugliness = '';
	var power = '';
	var greediness = '';
	var total = '';
	var prix = '';
	var prime = '';
	var equip = '';
	var gain = '';
	var danger = '';
	var danger_bis = '';
	if (cid == -1) infos2 = txt;
	else {
		infos1 = '<a style="color:#1C5059;font-weight:normal;text-decoration:none;" href="' + GM_info.script.namespace + '/contract/?cid=' + contracts.table[cid].id + '" onmouseout="Tip.hide();" onmouseover="Tip.show(\'<img src=\\\'/gfx/contract' + contracts.table[cid].kind + '.gif\\\' size=\\\'16px\\\'>&nbsp;' + contracts.table[cid].name.replace(/'/g,"\\\'") + ', ' + contracts.table[cid].age + ' ans\',\'' + contracts.table[cid].city.replace(/'/g,"\\\'") + ' - ' + contracts.table[cid].country.replace(/'/g,"\\\'") + ' à ' + timezone2time(contracts.table[cid].timezone) + 'h<br><ul class=\\\'caracs\\\'>';
		if (contracts.table[cid].sadism > 0)
			sadism = print_tip_percent('Sadisme', 'sadism', contracts.table[cid].estimation[mid].percentS, contracts.table[cid].estimation[mid].percent_equipS);
		else sadism = '';
		if (contracts.table[cid].ugliness > 0)
			ugliness = print_tip_percent('Laideur', 'ugliness', contracts.table[cid].estimation[mid].percentU, contracts.table[cid].estimation[mid].percent_equipU);
		else ugliness = '';
		if (contracts.table[cid].power > 0)
			power = print_tip_percent('Force', 'power', contracts.table[cid].estimation[mid].percentP, contracts.table[cid].estimation[mid].percent_equipP);
		else power = '';
		if (contracts.table[cid].greediness > 0)
			greediness = print_tip_percent('Gourmand', 'greediness', contracts.table[cid].estimation[mid].percentG, contracts.table[cid].estimation[mid].percent_equipG);
		else {
			greediness = print_tip_percent('Miam', 'burp', 100 - contracts.table[cid].estimation[mid].percentG, 100 - contracts.table[cid].estimation[mid].percent_equipG);
			if (contracts.table[cid].estimation[mid].percentG < 100) {
				danger = '<br><strong>Risques de dévorer !</strong>';
				danger_bis = '!';
			}
		}
		total = '<br>' + print_tip_percent('Total', '', contracts.table[cid].estimation[mid].percent, contracts.table[cid].estimation[mid].percent_equip);
		prix = '<li class=\\\'bounty\\\'><label>Prix</label>' + contracts.table[cid].prize + '</li>';
		prime = '<li class=\\\'bounty\\\'><label>Prime</label>' + monsters[mid].bounty + '</li>';
		if (contracts.table[cid].estimation[mid].equip_prix > 0) equip = '<li class=\\\'bounty\\\'><label>Objets</label>' + contracts.table[cid].estimation[mid].equip_prix + '</li>';
		gain = '<li class=\\\'bounty\\\'><label>Gain</label>' + (contracts.table[cid].prize - monsters[mid].bounty - contracts.table[cid].estimation[mid].equip_prix) + '</li>';
		infos2 = '</ul>\',null,event);">' + txt + danger_bis + '</a>';
	}

	return infos1 + sadism + ugliness + power + greediness + total + prix + prime + equip + gain + danger + infos2;
}

// affichage d'un contrat : lien + boîte + recalcule
function print_link_accepted(cid, mid, txt) {
	var infos1 = '';
	var infos2 = '';
	var sadism = '';
	var ugliness = '';
	var power = '';
	var greediness = '';
	var total = '';
	var prix = '';
	var prime = '';
	var gain = '';
	var danger = '';
	var danger_bis = '';
	var percent_sadism = 0;
	var percent_ugliness = 0;
	var percent_power = 0;
	var percent_greediness = 0;
	var percent_total = 0;
	if (cid == -1) infos2 = txt;
	else {
		var C_time = timezone2time(contracts.table[cid].timezone);
		infos1 = '<a href="' + GM_info.script.namespace + '/contract/?cid=' + contracts.table[cid].id + '" onmouseout="Tip.hide();" onmouseover="Tip.show(\'<img src=\\\'/gfx/contract' + contracts.table[cid].kind + '.gif\\\' size=\\\'16px\\\'>&nbsp;' + contracts.table[cid].name.replace(/'/g,"\\\'") + ', ' + contracts.table[cid].age + ' ans\',\'' + contracts.table[cid].city.replace(/'/g,"\\\'") + ' - ' + contracts.table[cid].country.replace(/'/g,"\\\'") + ' à ' + timezone2time(contracts.table[cid].timezone) + 'h<br><ul class=\\\'caracs\\\'>';
		percent_sadism = estimate_stat(monsters[mid].sadism, contracts.table[cid].sadism, contracts.table[cid].difficulty, C_time);
		if (contracts.table[cid].sadism > 0) sadism = print_tip_percent('Sadisme', 'sadism', percent_sadism, 0);
		percent_ugliness = estimate_stat(monsters[mid].ugliness, contracts.table[cid].ugliness, contracts.table[cid].difficulty, C_time);
		if (contracts.table[cid].ugliness > 0) ugliness = print_tip_percent('Laideur', 'ugliness', percent_ugliness, 0);
		percent_power = estimate_stat(monsters[mid].power, contracts.table[cid].power, contracts.table[cid].difficulty, C_time);
		if (contracts.table[cid].power > 0) power = print_tip_percent('Force', 'power', percent_power, 0);
		percent_greediness = estimate_statG(monsters[mid].greediness, contracts.table[cid].greediness, contracts.table[cid].difficulty, C_time, monsters[mid].control, contracts.table[cid].kind);
		if (contracts.table[cid].greediness > 0)
			greediness = print_tip_percent('Gourmand', 'greediness', percent_greediness, 0);
		else {
			greediness = print_tip_percent('Miam', 'burp', 100 - percent_greediness, 0);
			if (percent_greediness < 100) {
				danger = '<br><strong>Risques de dévorer !</strong>';
				danger_bis = '!';
			}
		}
		percent_total = Math.floor((percent_sadism * percent_ugliness * percent_power * percent_greediness) / 1000000);
		total = '<br>' + print_tip_percent('Total', '', percent_total, 0);
		prix = '<li class=\\\'bounty\\\'><label>Prix</label>' + contracts.table[cid].prize + '</li>';
		prime = '<li class=\\\'bounty\\\'><label>Prime</label>' + monsters[mid].bounty + '</li>';
		gain = '<li class=\\\'bounty\\\'><label>Gain</label>' + (contracts.table[cid].prize - monsters[mid].bounty) + '</li>';
		infos2 = '</ul>\',null,event);">' + txt + danger_bis + '</a>';
	}

	return infos1 + sadism + ugliness + power + greediness + total + prix + prime + gain + danger + infos2;
}

// affichage d'un contrat : lien + boîte
function print_link_contract(cid, txt) {
	var lien = '';
	if (cid == -1) lien = txt;
	else {
		lien = '<a href="' + GM_info.script.namespace + '/contract/?cid=' + contracts.table[cid].id + '" onmouseout="Tip.hide();" onmouseover="Tip.show(\'<img src=\\\'/gfx/contract' + contracts.table[cid].kind + '.gif\\\' size=\\\'16px\\\'>&nbsp;' + contracts.table[cid].name.replace(/'/g,"\\\'") + ', ' + contracts.table[cid].age + ' ans\',\'' + contracts.table[cid].city.replace(/'/g,"\\\'") + ' - ' + contracts.table[cid].country.replace(/'/g,"\\\'") + ' à ' + timezone2time(contracts.table[cid].timezone) + 'h<br><ul class=\\\'caracs\\\'><li class=\\\'sadism\\\'><label>Sadisme</label>' + contracts.table[cid].sadism + '</li><li class=\\\'ugliness\\\'><label>Laideur</label>' + contracts.table[cid].ugliness + '</li><li class=\\\'power\\\'><label>Force</label>' + contracts.table[cid].power + '</li><li class=\\\'greediness\\\'><label>Gourmand</label>&nbsp;' + contracts.table[cid].greediness + '</li><li class=\\\'bounty\\\'><label>Prix</label>' + contracts.table[cid].prize + '</li>\',null,event);">' + txt + '</a>';
	}
	return lien;
}
