# Croque Monster Scripts

Scripts écrits fin 2012, sans savoir faire du dev web, sans connaître JavaScript 😆.

## Objectif

Ces scripts servent à améliorer l'expérience utilisateur pour le jeu http://www.croquemonster.com (c'est pas tout jeune 😅).

## Utilisation

* Avoir un compte sur le jeu http://www.croquemonster.com
* Installer https://www.tampermonkey.net
* Aller sur http://squal.conrad.free.fr/ et installer les scripts souhaités

## Scripts "faits maison"

### cmMenuSinge

Simplifie la gestion de l'agence : atelier, contrats, affectations.

### cmMBLRCC

Simplifie le suivi d'un match de MBL et facilite la prise de décision.
